<?php
namespace pfmAPI\Apps\Devices\Controllers;

use pfmAPI\Controllers\RESTController,
    pfmAPI\Exceptions\HTTPException,

    pfmAPI\Apps\User\Models\User,
    pfmAPI\Apps\Devices\Models\Devices,
    pfmAPI\Apps\Devices\Models\Tags,
    pfmAPI\Apps\Devices\Models\Groups,
    pfmAPI\Apps\Devices\Models\UserHasDevice,
    pfmAPI\Apps\Devices\Models\Values;

class DevicesController extends RESTController {

    public function checkOwner($device_id) {
        if (!UserHasDevice::findFirst(array(
            "conditions" => "device_id = ?1 AND user_id = ?2",
            "bind"       => array(1 => $device_id, 2 => $this->getDI()->get('oauth2_resource')->getOwnerId())
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));
    }

    public function allowCrossplatform() {
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Allow-Methods: POST, PATCH, GET, PUT, DELETE, OPTIONS");
        exit(header("Access-Control-Allow-Origin: *"));
    }
    public function getDevices() {
        //$devices = Devices::find();
        $devices = Devices::getDevicesByUser($this->getDI()->get('oauth2_resource')->getOwnerId());
        return array("items" => $devices->toArray());
    }

    public function addDevice() {
        $device = new Devices();
        $device->name = $this->request->get('name');
        $device->OPC_path = $this->request->get('opc_path');
        if (!$device->save())
            throw new HTTPException(
                "The device could not be saved in the database right now.",
                500,
                array(
                    'dev' => implode(',', $device->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));

        $device->setUserRole($this->getDI()->get('oauth2_resource')->getOwnerId(), 1);

        return array("ok" => true);
    }

    public function getDevice($device_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if (!Tags::findFirst(["device_id = ?1", "bind" => [ 1 => $device_id ]]))
            $tagsData = array();
        else {
            $tags = $device->tags;
            $groupsData = Array();
            foreach ($tags as $tag) {
                if ($tag->group_id) {
                    $groupsData[$tag->group->id]['id'] = $tag->group->id;
                    $groupsData[$tag->group->id]['name'] = $tag->group->name;
                    $groupsData[$tag->group->id]['tags'][] = $tag->toArray() + array("current_value" => $tag->currentValue()->toArray());
                } else {
                    $groupsData[0]['id'] = false;
                    $groupsData[0]['name'] = "(Sin grupo)";
                    $groupsData[0]['tags'][] = $tag->toArray() + array("current_value" => $tag->currentValue()->toArray());
                }
            }

            foreach ($groupsData as $groupData) {
                $tagsData[] = array("group_id" => $groupData['id'], "name" => $groupData['name'], "tags" => $groupData['tags']);
            }
        }

        return $device->toArray() + array("groups" => $tagsData);
    }

    public function removeDevice($device_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if (!$device->delete())
            throw new HTTPException(
                "The device could not be deleted from the database right now.",
                500,
                array(
                    'dev' => implode(',', $device->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));

        return array("ok" => true);
    }

    public function editDevice($device_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if ($this->request->hasPut('name'))
            $device->name = $this->request->getPut('name');

        if ($this->request->hasPut('opc_path'))
            $device->OPC_path = $this->request->getPut('opc_path');

        if (!$device->save())
            throw new HTTPException(
                "The device could not be saved in the database right now.",
                500,
                array(
                    'dev' => implode(',', $device->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));

        $this->getDevice($device_id);
    }

    public function getUsers($device_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        return array("items" => $device->getUsers()->toArray());
    }

    // alias
    public function addUser($device_id) {
        $this->checkOwner($device_id);

        $user_id = false;
        if ($this->request->has("user")) {
            if (!$user = User::findFirst(array(
                "conditions" => "nick = ?1 or email = ?1",
                "bind"       => array(1 => $this->request->get("user"))
            )))
                throw new HTTPException(
                    "User not found",
                    400,
                    array(
                        'internalCode' => 'PM001',
                    ));
            else
                $user_id = $user->id;

        } elseif ($this->request->has("user_id"))
            $user_id = $this->request->get("user_id");

        if (!$user_id)
            throw new HTTPException(
                "User not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        $this->editUser($device_id, $user_id);

        return array("ok" => true);
    }

    public function editUser($device_id, $user_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if ($this->request->has('role_id'))
            $role_id = $this->request->get('role_id');
        elseif ($this->request->hasPut('role_id'))
            $role_id = $this->request->getPut('role_id');
        else
            throw new HTTPException(
                "Role id not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        $device->setUserRole($user_id, $role_id);

        return array("ok" => true);
    }

    public function removeUser($device_id, $user_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if ($user_device = UserHasDevice::findFirst([
            "user_id = ?1 AND device_id = ?2",
            "bind" => [
                1 => $user_id,
                2 => $device_id,
            ],
        ])) {
            if (!$user_device->delete())
                throw new HTTPException(
                    "The device user could not be deleted from the database right now.",
                    500,
                    array(
                        'dev' => implode(',', $device->getMessages()),
                        'internalCode' => 'PrivateMessage001',
                    ));
        }

        return array("ok" => true);
    }

    public function getTag($device_id, $tag_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));


        if (!$tag = Tags::findFirst(array(
            "conditions" => "id = ?1 and device_id = ?2",
            "bind"       => array(1 => $tag_id, 2 => $device_id)
        )))
            throw new HTTPException(
                "Tag not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        return $tag->toArray() + array("group" => ($tag->group_id ? $tag->group->toArray() : false)) + array("values" => $tag->getLatestValues(100)->toArray());
    }

    public function addTag($device_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        $tag = new Tags();
        $tag->device_id = $device_id;
        $tag->name = $this->request->get('name');
        if (!$tag->save())
            throw new HTTPException(
                "The tag could not be saved in the database right now.",
                500,
                array(
                    'dev' => implode(',', $tag->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));

        if ($this->request->has('group') && $this->request->get('group'))
            $tag->setGroup($this->request->get('group'));

        $this->getTag($device_id, $tag->id);
        //return array("ok" => true);
    }


    public function editTag($device_id, $tag_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if ($tag = Tags::findFirst([
            "id = ?1 AND device_id = ?2",
            "bind" => [
                1 => $tag_id,
                2 => $device_id,
            ],
        ])) {
            if ($this->request->hasPut('name'))
                $tag->name = $this->request->getPut('name');

            if ($this->request->hasPut('group'))
                $tag->setGroup($this->request->getPut('group'));
        }


        if (!$tag->save())
            throw new HTTPException(
                "The tag could not be saved in the database right now.",
                500,
                array(
                    'dev' => implode(',', $device->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));


        return array("ok" => true);
    }



    public function removeTag($device_id, $tag_id) {
        $this->checkOwner($device_id);

        if (!$device = Devices::findFirst(array(
            "conditions" => "id = ?1",
            "bind"       => array(1 => $device_id)
        )))
            throw new HTTPException(
                "Device not found",
                400,
                array(
                    'internalCode' => 'PM001',
                ));

        if ($tag = Tags::findFirst([
            "id = ?1 AND device_id = ?2",
            "bind" => [
                1 => $tag_id,
                2 => $device_id,
            ],
        ])) {
            if (!$tag->delete())
                throw new HTTPException(
                    "The tag could not be deleted from the database right now.",
                    500,
                    array(
                        'dev' => implode(',', $device->getMessages()),
                        'internalCode' => 'PrivateMessage001',
                    ));
        }

        return array("ok" => true);
    }




    public function getDeviceOld($device_id)
    {
        $this->checkOwner($device_id);

        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => Devices::getDevicesById(explode(",", $device_id)),
            "limit"=> $this->request->get('limit',null,20),
            "page" => $this->request->get('page',null,1)
        ));
        $page = $paginator->getPaginate();
        $page_info = array(
            'current' => $page->current,
            'total_pages' => $page->total_pages,
            'total_items' => $page->total_items,
        );
        //$items = $page->items->toArray();
        $items = array();
        foreach ($page->items as $item)
        {
			$arr_objects = array();
            $objects = Groups::find(array("device_id = ".$item->id.''));
			foreach ($objects as $object)
			{
                $arr_values = array();
                $values = Values::find(array("tags_id = ".$object->id.''));
                foreach ($values as $value)
                {
                    $arr_values[] = $value->toArray();
                }
				$arr_objects[] = $object->toArray();
			}
            $items[] = (array) $item + array("tags" => $arr_objects);
        }
        return array("device" => $items[0], "page" => $page_info);
    }


    public function tagHistory($tag_id) {
        $tag = Tags::findFirst(array("tags_id" => " . $tag_id"));
        return $tag->toArray() + array("latestValues" => $values);
        return array("test");
    }
}