<?php
namespace pfmAPI\Apps\Devices\Models;


use Phalcon\Mvc\Model\Relation;

use Phalcon\Di;
use pfmAPI\Apps\Devices\Models\UserHasDevice,
    pfmAPI\Apps\User\Models\User,
    pfmAPI\Models\ModelRolesInterface,
    pfmAPI\Exceptions\HTTPException;

class Devices extends \Phalcon\Mvc\Model implements ModelRolesInterface
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $name;
     
    /**
     *
     * @var string
     */
    public $OPC_path;
     
    /**
     *
     * @var string
     */
    public $created_at;
	 
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'pfmAPI\Apps\Devices\Models\Groups', 'device_id', array(
            'alias' => 'groups',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
            )
        ));

        $this->hasMany('id', 'pfmAPI\Apps\Devices\Models\Tags', 'device_id', array(
            'alias' => 'tags',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
            )
        ));

        $this->hasMany('id', 'pfmAPI\Apps\Devices\Models\UserHasDevice', 'device_id', array(
            'alias' => 'users_roles',
            'foreignKey' => array(
                'action' => Relation::ACTION_CASCADE,
            )
        ));


    }

    static public function getDevicesByUser($user_id)
    {
        $di = \Phalcon\DI::getDefault();
        return $di->get('modelsManager')->createBuilder()
            ->from('pfmAPI\Apps\Devices\Models\Devices')
            ->innerJoin('\pfmAPI\Apps\Devices\Models\UserHasDevice', 'device.device_id = pfmAPI\Apps\Devices\Models\Devices.id', 'device')
            ->where('device.user_id = :id:', ['id' => $user_id])
            ->getQuery()->execute();
    }

    public function beforeValidationOnCreate()
    {
        // Timestamp the confirmaton
        $this->created_at = date("Y-m-d H:i:s");
    }

    /**
     * Return the related "players"
     */
    public function getGroups($parameters=null)
    {
        return $this->getRelated('groups', $parameters);
    }
    /**
     * Return the related "players"
     */
    public function getTags($parameters=null)
    {
        return $this->getRelated('tags', $parameters);
    }
	
    static public function getDevicesById($devices_id)
    {
        $di = \Phalcon\DI::getDefault();
        return $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Apps\Devices\Models\Devices')
            ->inwhere('[\pfmAPI\Apps\Devices\Models\Devices].id', $devices_id)
            ->columns(array(
                '[\pfmAPI\Apps\Devices\Models\Devices].id',
                '[\pfmAPI\Apps\Devices\Models\Devices].name',
                '[\pfmAPI\Apps\Devices\Models\Devices].created_at as created_at',
            ))
            ->orderBy('[\pfmAPI\Apps\Devices\Models\Devices].id ASC');
    }
	
    static public function getDevicesBuilder()
    {
        $di = \Phalcon\DI::getDefault();
        return $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Apps\Devices\Models\Devices')
            ->columns(array(
                '[\pfmAPI\Apps\Devices\Models\Devices].id',
                '[\pfmAPI\Apps\Devices\Models\Devices].name',
                '[\pfmAPI\Apps\Devices\Models\Devices].created_at as created_at',
            ))
            ->orderBy('[\pfmAPI\Apps\Devices\Models\Devices].id ASC');
    }

    public function setUserRole($user_id, $role_id) {
        if (!$user_device = UserHasDevice::findFirst([
            "user_id = ?1 AND device_id = ?2",
            "bind" => [
                1 => $user_id,
                2 => $this->id,
            ],
        ])) {
            $user_device = new UserHasDevice();
            $user_device->user_id = $user_id;
            $user_device->device_id = $this->id;
        }

        $user_device->device_role_id = $role_id;

        if (!$user_device->save())
            throw new HTTPException(
                "The user role could not be saved in the database right now.",
                500,
                array(
                    'dev' => implode(',', $user_device->getMessages()),
                    'internalCode' => 'PrivateMessage001',
                ));
    }

    public function getUsers() {
        $di = \Phalcon\DI::getDefault();
        return $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Apps\Devices\Models\UserHasDevice')
            ->where('[\pfmAPI\Apps\Devices\Models\UserHasDevice].device_id = :id:', ['id' => $this->id])
            ->join('[\pfmAPI\Apps\Devices\Models\DevicesRoles]', 'role.id = [\pfmAPI\Apps\Devices\Models\UserHasDevice].device_role_id', 'role', 'LEFT')
            ->join('[\pfmAPI\Apps\User\Models\User]', 'user.id = [\pfmAPI\Apps\Devices\Models\UserHasDevice].user_id', 'user', 'LEFT')
            ->columns(array(
                'user.id',
                'user.nick',
                'role.name as role',
                'role.id as role_id',
            ))
            ->orderBy('user.nick ASC')->getQuery()->execute();
    }
	
/*
	 *
	 *
        return $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Apps\Devices\Models\UserHasDevice')
            ->where('[\pfmAPI\Apps\Devices\Models\UserHasDevice].device_id = :id:', ['id' => $this->id])
            ->join('\pfmAPI\Apps\User\Models\User', 'user.id = \pfmAPI\Apps\Models\UserHasDevice.user_id', 'user', 'LEFT')
            ->join('\pfmAPI\Apps\Devices\Models\DevicesRoles', 'role.id = \pfmAPI\Apps\Devices\UserHasDevice].device_role_id', 'role', 'LEFT')
            ->orderBy('[\pfmAPI\Apps\User\Models\User].nick ASC')->getQuery()->execute();

    static public function getaDevicesById($devices_id)
    {
        $di = \Phalcon\DI::getDefault();
        return $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Apps\Team\Models\TeamsHasUsers')
            ->andWhere('[\pfmAPI\Apps\Team\Models\TeamsHasUsers].accepted = 1')
            ->join('\pfmAPI\Apps\Team\Models\Team', 'team.id = [\pfmAPI\Apps\Team\Models\TeamsHasUsers].teams_id', 'team', 'LEFT')
            ->andWhere('team.is_individual_team = 0')
            //->andWhere('team.active = 1')
            ->columns(array(
                'team.id',
                'team.games_id',
                'team.name',
                'team.tag',
                'team.creation as created_at',
                '[\pfmAPI\Apps\Team\Models\TeamsHasUsers].users_id',
                '[\pfmAPI\Apps\Team\Models\TeamsHasUsers].accepted_by_users_id',
                '[\pfmAPI\Apps\Team\Models\TeamsHasUsers].joined as joined_at',
            ))
            ->orderBy('[\pfmAPI\Apps\Team\Models\TeamsHasUsers].users_id ASC, team.joined_at DESC');
    }
*/

public static function getUserPrivilege($objectId, $userId) {
    return true;
}

    public static function getIsUserActive($objectId, $userId) { return true; }

    public static function getPrivilegesIds() {  return true; }


    public static function getUserRole($deviceId, $userId) {
        $getUserRoleSql = 'SELECT tp.name
                                FROM `user_has_device` tu
                                JOIN `devices_roles` tp ON tu.device_role_id = tp.id
                                WHERE tu.device_id = :device_id AND tu.user_id = :users_id';
        $result = Di::getDefault()->get('db')->query($getUserRoleSql,
            array(
                'device_id'              => $deviceId,
                'users_id'              => $userId
            )
        );
        /** @var \Phalcon\Db\ResultInterface  $result */
        return $result->fetch()[0];
    }
}
