<?php
namespace pfmAPI\Apps\Devices\Models;

class DevicesRoles extends \Phalcon\Mvc\Model
{

    const ROLE_GOD = 1;
    const ROLE_ADMIN = 2;
    const ROLE_EDITOR = 3;
    const ROLE_VIEWER = 4;

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var integer
     */
    public $name;

    public function getSource()
    {
        return "devices_roles";
    }

}
