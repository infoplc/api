<?php
namespace pfmAPI\Apps\Devices\Models;

class Groups extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $name;
     
    /**
     *
     * @var integer
     */
    public $device_id;
     
	 
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'pfmAPI\Apps\Devices\Models\TagsHasGroup', 'group_id', array(
            'alias' => 'tagsList',
            'foreignKey' => array(
                'message' => 'Cannot be deleted because it has activity in the system'
            )
        ));
    }
	
    /**
     * Return the related "players"
     */
    public function getTags($parameters=null)
    {
        return $this->getRelated('tagsList', $parameters)->tags;
    }
}
