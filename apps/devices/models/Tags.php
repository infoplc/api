<?php
namespace pfmAPI\Apps\Devices\Models;

use pfmAPI\Apps\Devices\Models\UserHasDevice,
    pfmAPI\Apps\User\Models\User,
    pfmAPI\Exceptions\HTTPException;

class Tags extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $name;
     
    /**
     *
     * @var integer
     */
    public $editable;

    /**
     *
     * @var integer
     */
    public $type;

    /**
     *
     * @var integer
     */
    public $device_id;
    /**
     *
     * @var integer
     */
    public $group_id;
     
	 
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'pfmAPI\Apps\Devices\Models\Values', 'tag_id', array(
            'alias' => 'values',
            'foreignKey' => array(
                'message' => 'Cannot be deleted because it has activity in the system'
            )
        ));

        $this->hasOne('group_id', 'pfmAPI\Apps\Devices\Models\Groups', 'id', array(
            'alias' => 'group',
            'foreignKey' => array(
                'message' => 'Cannot be deleted because it has activity in the system'
            )
        ));
    }
	
	
    /**
     * Return the related "players"
     */
    public function getValues($parameters=null)
    {
        return $this->getRelated('values', $parameters);
    }

    public function currentValue()
    {
        return $this->getRelated('values',  [
            "limit" => 1,
            "order" => "id DESC"
        ]);
    }

    public function getLatestValues($limit = 100)
    {
        return $this->getRelated('values',  [
            "limit" => $limit,
            "order" => "id DESC"
        ]);
    }



    public function setGroup($group_name) {

        if (!$group_name) {
            $this->group_id = null;
        } else {

            if (!$group = Groups::findFirst(array(
                "conditions" => "name = ?1 AND device_id = ?2",
                "bind" => array(1 => $group_name, 2 => $this->device_id)
            ))
            ) {
                $group = new Groups();
                $group->name = $group_name;
                $group->device_id = $this->device_id;
                if (!$group->save())
                    throw new HTTPException(
                        "The group could not be saved in the database right now.",
                        500,
                        array(
                            'dev' => implode(',', $group->getMessages()),
                            'internalCode' => 'PrivateMessage001',
                        ));
            }

            $this->group_id = $group->id;
        }


            if (!$this->save())
                throw new HTTPException(
                    "The group field could not be saved in the database right now.",
                    500,
                    array(
                        'dev' => implode(',', $this->getMessages()),
                        'internalCode' => 'PrivateMessage001',
                    ));
    }
}
