<?php
namespace pfmAPI\Apps\Devices\Models;

use pfmAPI\Exceptions\HTTPException;

class UserHasDevice extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var integer
     */
    public $device_id;

    /**
     *
     * @var string
     */
    public $device_role_id;

    /**
     *
     * @var string
     */
    public $created_at;


    public function getSource()
    {
        return "user_has_device";
    }

    public function beforeValidationOnCreate()
    {
        // Timestamp the confirmaton
        $this->created_at = date("Y-m-d H:i:s");
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('device_id', 'pfmAPI\Apps\Devices\Models\Devices', 'id', array(
            'alias' => 'device',
            'foreignKey' => array(
                'message' => '01Cannot be deleted because it has activity in the system'
            )
        ));
        $this->belongsTo('user_id', 'pfmAPI\Apps\User\Models\User', 'id', array(
            'alias' => 'user',
            'foreignKey' => array(
                'message' => '02Cannot be deleted because it has activity in the system'
            )
        ));
        $this->belongsTo('device_role_id', 'pfmAPI\Apps\Devices\Models\DevicesRoles', 'id', array(
            'alias' => 'role',
            'foreignKey' => array(
                'message' => '03Cannot be deleted because it has activity in the system'
            )
        ));
    }

    /**
     * Return the related "players"
     */
    public function getTag($parameters=null)
    {
        return $this->getRelated('tag', $parameters);
    }
}
