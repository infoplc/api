<?php
namespace pfmAPI\Apps\Devices\Models;

class Values extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;
     
    /**
     *
     * @var string
     */
    public $value;
     
    /**
     *
     * @var string
     */
    public $created_at;
     
    /**
     *
     * @var integer
     */
    public $tag_id;
     
}
