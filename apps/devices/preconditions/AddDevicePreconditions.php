<?php
namespace pfmAPI\Apps\Devices\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class AddDevicePreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "name" => array(
                "optional" => false,
            ),
            "opc_path" => array(
                "optional" => false,
            ),
        );

        $this->setToken(
            array(
                "owner" => 'user',
                "scopes" => false
            )
        );
    }
}