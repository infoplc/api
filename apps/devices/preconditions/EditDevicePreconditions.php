<?php
namespace pfmAPI\Apps\Devices\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class EditDevicePreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "name" => array(
                "optional" => false,
            ),
            "opc_path" => array(
                "optional" => false,
            ),
        );

        $this->setToken(
            array(
                "owner" => 'user',
                "scopes" => false
            )
        );

        $this->setPermissions(
            array(
                'class'     => 'pfmAPI\Apps\Devices\Models\Devices',
                'object_id' => $this->getParam('device_id'),
                'resource'  => 'Device',
                'access'    => 'edit',
            )
        );
    }
}