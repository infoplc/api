<?php
namespace pfmAPI\Apps\Devices\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class EditUserPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->setToken(
            array(
                "owner" => 'user',
                "scopes" => false
            )
        );

        $this->params = array(
            "role_id" => array(
                "optional" => false,
            ),
        );;

        $this->setPermissions(
            array(
                'class'     => 'pfmAPI\Apps\Devices\Models\Devices',
                'object_id' => $this->getParam('device_id'),
                'resource'  => 'User',
                'access'    => 'edit',
            )
        );
    }
}