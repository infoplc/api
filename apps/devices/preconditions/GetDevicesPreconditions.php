<?php
namespace pfmAPI\Apps\Devices\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class GetDevicesPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->setToken(
            array(
                "owner" => 'user',
                "scopes" => false
            )
        );
    }
}