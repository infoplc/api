<?php

return call_user_func(function(){

    $userCollection = new \Phalcon\Mvc\Micro\Collection();

    $userCollection
        ->setPrefix('/v1/devices')
        ->setHandler('\pfmAPI\Apps\Devices\Controllers\DevicesController')
        ->setLazy(true);

    $userCollection->get('/', 'getDevices');
    $userCollection->post('/', 'addDevice');
    $userCollection->options('/', 'allowCrossplatform');
    

    $userCollection->get('/{device_id}', 'getDevice');
    $userCollection->delete('/{device_id}', 'removeDevice');
    $userCollection->put('/{device_id}', 'editDevice');
    $userCollection->options('/{device_id}', 'allowCrossplatform');

    $userCollection->get('/{device_id}/users', 'getUsers');
    $userCollection->post('/{device_id}/users', 'addUser');
    $userCollection->options('/{device_id}/users', 'allowCrossplatform');

    $userCollection->put('/{device_id}/users/{user_id}', 'editUser');
    $userCollection->delete('/{device_id}/users/{user_id}', 'removeUser');
    $userCollection->options('/{device_id}/users/{user_id}', 'allowCrossplatform');

    $userCollection->get('/{device_id}/tags/{tag_id}', 'getTag');
    $userCollection->post('/{device_id}/tags', 'addTag');
    $userCollection->options('/{device_id}/tags', 'allowCrossplatform');

    $userCollection->put('/{device_id}/tags/{tag_id}', 'editTag');
    $userCollection->delete('/{device_id}/tags/{tag_id}', 'removeTag');
    $userCollection->options('/{device_id}/tags/{tag_id}', 'allowCrossplatform');

    return $userCollection;
});