<?php
namespace pfmAPI\Apps\Misc\Controllers;

use pfmAPI\Controllers\RESTController,
    pfmAPI\Models\ACLLoader,
    pfmAPI\Apps\User\Models\User,
    Phalcon\Acl\Adapter\Memory,
    pfmAPI\Exceptions\HTTPException;

class MiscController extends RESTController {

    public function getTime() {
        return array("time" => time());
    }

    /*
     * Devuelve informacion del token
     */
    public function tokenInfo()
    {
        return array(
            "OwnerType" => $this->getDI()->get('oauth2_resource')->getOwnerType(),
            "OwnerId" => $this->getDI()->get('oauth2_resource')->getOwnerId(),
            "AccessToken" => $this->getDI()->get('oauth2_resource')->getAccessToken(),
            "ClientId" => $this->getDI()->get('oauth2_resource')->getClientId(),
            "Scopes" => $this->getDI()->get('oauth2_resource')->getScopes(),
        );
    }

    /*
     * Devuelve informacion del usuario
     */
    public function whoAmI()
    {
        if (!$user = User::findFirst($this->getDI()->get('oauth2_resource')->getOwnerId()))
            throw new HTTPException(
                "User not found",
                404,
                array(
                    'dev' => "The provided token belongs to a unexisting user",
                    'internalCode' => 'Oauth005',
                ));

        return array(
            'id' => $this->getDI()->get('oauth2_resource')->getOwnerId(),
            'nickname' => $user->nick,
            'email' => $user->email,
            'imageUrl' => 'example.jpg'
        );
    }

    public function getAclsTable()
    {
        $acl = new Memory();
        $aclLoader = new ACLLoader($acl);
        return $aclLoader->getAclsTable();
    }
}