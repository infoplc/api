<?php
namespace pfmAPI\Apps\Misc\Models;

use pfmAPI\Exceptions\HTTPException,
    \Phalcon\DI;

class Misc extends \Phalcon\Mvc\Model {

    public static function createCacheDirIfNotExists() {
        $cacheDir = BASE_DIR . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
        if(!is_dir($cacheDir)) {
            mkdir($cacheDir);
        }
    }

    public static function createLogDirIfNotExists() {
        $logDir = BASE_DIR . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR;
        if(!is_dir($logDir)) {
            mkdir($logDir);
        }
        return $logDir;
    }

    public static function createMaintenanceFile() {
        $publicDir = BASE_DIR . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;
        $resource = fopen($publicDir.'maintenance.flag','w');
        fclose($resource);
    }

    public static function removeMaintenanceFile() {
        $publicDir = BASE_DIR . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR;
        unlink($publicDir.'maintenance.flag');
    }

    public static function logException(\Exception $exception) {
        $logDir = self::createLogDirIfNotExists();
        $resource = fopen($logDir . 'exception.log','a');
        $traceLines = $exception->getTrace();

        fwrite($resource,date('Y-m-d H:i:s').' '.$exception->getMessage().' '.$exception->getCode()."\n");
        foreach($traceLines as $line) {

            fwrite($resource,(isset($line['file'])? $line['file'] . '(' . $line['line'] . '): ' : '') . (isset($line['class']) ? $line['class'] : '').'::'.$line['function'].'()'."\n");
        }

        $previousException = $exception->getPrevious();
        if($previousException) {
            fwrite($resource,"\nPREVIOUS EXCEPTION:\n");
            self::logException($exception);
        }
        fwrite($resource,"\n\n");
        fclose($resource);
    }

    public static function log($string, $filename = null) {
        $logDir = self::createLogDirIfNotExists();
        if(!$filename) {
            $filename = 'system.log';
        }
        $resource = fopen($logDir . $filename,'a');
        fwrite($resource,date('Y-m-d H:i:s').': '.$string."\n");
        fclose($resource);
    }

    /**
     * Given an access name ("edit","insert", etc.), a resource ("Competition", "Team", etc.) and a class that
     * has related privileges (like "pfmAPI\Apps\Competition\Models\Competition"), you will get an array of type
     * array("privilege ID" => "Privilege Name") that match the access and the resource.
     *
     * @param array $accessName
     * @param string $resource
     * @param string $searchPrivilegesFromClass
     * @throws HTTPException
     * @return array
     */
    public static function searchPrivilegesThatCanAccess($accessName, $resource, $searchPrivilegesFromClass) {
        $aclsTable = DI::getDefault()->getShared('aclLoader')->getAclsTable();
        $matchedPrivileges = array();

        $privilegesArray = array();
        if($searchPrivilegesFromClass && is_subclass_of($searchPrivilegesFromClass, 'pfmAPI\Models\ModelPrivilegesInterface') && is_callable($searchPrivilegesFromClass.'::getPrivilegesIds')) {
            /** @var ModelPrivilegesInterface $searchPrivilegesFromClass */
            $privilegesArray = $searchPrivilegesFromClass::getPrivilegesIds();
        }

        if(!count($privilegesArray))
        {
            throw new HTTPException(
                "Could not find any associated privilege levels to class: ".(string)$searchPrivilegesFromClass,
                400,
                array(
                    'internalCode' => 'Misc001',
                ));
        }

        foreach($aclsTable as $aclRow) {
            if(!empty($aclRow['privilege_name'])) {
                if(!empty($aclRow['resource_name']) && $aclRow['resource_name'] == $resource) {
                    if(!empty($aclRow['accesses_names'])) {
                        $accessesNames = explode(',',$aclRow['accesses_names']);
                        $accessesNames = array_map(function($elem) { return trim($elem); }, $accessesNames);
                        foreach($accessesNames as $currentAccessName) {
                            if(($currentAccessName === $accessName) || $currentAccessName == '*') {
                                if(isset($privilegesArray[$aclRow['privilege_name']])) {
                                    $privilegeId = $privilegesArray[$aclRow['privilege_name']];
                                    $matchedPrivileges[$privilegeId] = $aclRow['privilege_name'];
                                } else {
                                    throw new HTTPException(
                                        "There is no any privilege in '".(string)$searchPrivilegesFromClass."' that could '".$accessName."' a '".$resource."' resource.",
                                        400,
                                        array(
                                            'internalCode' => 'Misc002',
                                        ));
                                }
                            }
                        }
                    }
                }
            }
        }
        return $matchedPrivileges;
    }
}