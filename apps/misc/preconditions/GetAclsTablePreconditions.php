<?php
namespace pfmAPI\Apps\Misc\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class GetAclsTablePreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->setToken(
            array(
                "owner" => 'client',
                "scopes" => false
            )
        );
    }
}