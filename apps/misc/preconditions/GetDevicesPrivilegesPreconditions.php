<?php
namespace pfmAPI\Apps\Misc\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class GetDevicesPrivilegesPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->setToken(
            array(
                "owner" => 'client',
                "scopes" => false
            )
        );
    }
}