<?php
namespace pfmAPI\Apps\Misc\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class TokenInfoPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->token = array(
            "owner" => 'client',
            "scopes" => false,
        );
    }
}