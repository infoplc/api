<?php
namespace pfmAPI\Apps\Misc\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class WhoAmIPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->token = array(
            "owner" => 'user',
            "scopes" => false,
        );
    }
}