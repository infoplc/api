<?php

return call_user_func(function(){

    $miscCollection = new \Phalcon\Mvc\Micro\Collection();

    $miscCollection
        ->setPrefix('/v1/misc')
        ->setHandler('\pfmAPI\Apps\Misc\Controllers\MiscController')
        ->setLazy(true);

    $miscCollection->get('/time', 'getTime');
    $miscCollection->get('/token_info', 'tokenInfo');
    $miscCollection->get('/whoami', 'whoAmI');
    $miscCollection->get('/getAclsTable', 'getAclsTable');
    $miscCollection->get('/getDevicesPrivileges', 'getDevicesPrivileges');


    return $miscCollection;
});
