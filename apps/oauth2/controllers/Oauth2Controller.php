<?php
namespace pfmAPI\Apps\Oauth2\Controllers;

use pfmAPI\Controllers\RESTController,
    pfmAPI\Exceptions\HTTPException,
    pfmAPI\Apps\Oauth2\Forms\ForgotPasswordForm,
    pfmAPI\Apps\Oauth2\Forms\AuthorizeForm,
    pfmAPI\Apps\Oauth2\Forms\LoginForm,
    pfmAPI\Apps\Oauth2\Forms\LogoutForm,
    pfmAPI\Apps\Oauth2\Forms\SignUpForm,
    pfmAPI\Apps\Oauth2\Forms\ResetPasswordForm,
    pfmAPI\Apps\Oauth2\Forms\ChangeUserPasswordForm,
    pfmAPI\Apps\Oauth2\Forms\ChangeUserEmailForm,
    pfmAPI\Apps\Oauth2\Forms\LinkAccount_PVPnetForm,
    pfmAPI\Apps\Oauth2\Forms\LinkAccount_SteamForm,
    pfmAPI\Apps\Oauth2\Forms\EditImageForm,
    pfmAPI\Apps\User\Models\User,
    pfmAPI\Models\Image,
    pfmAPI\Models\GamePlatform,
    pfmAPI\Models\GamePlatformsHasUsers,
    League\OAuth2\Server,
    LightOpenID,
    pfmAPI\Apps\Misc\Models\PVPNet,
    pfmAPI\Apps\Misc\Models\Steam;

class Oauth2Controller extends RESTController {
    private function checkLogin()
    {
        // Get the current identity
        $identity = $this->auth->getIdentity();
        return (!is_array($identity)) ? false : $identity;
    }

    /*
     * Genera tokens para clientes, o intercambia códigos por tokens de usuario
     * grant_type=authorization_code - Usuario (requiere "code")
     * grant_type=client_credentials - Cliente
     */
    public function accessToken()
    {
        $server = $this->getDI()->getShared('oauth2_authorization');
        $this->parameters->rest->envelope = false;
        try {
            $response = $server->issueAccessToken($this->request->get());
            return $response;
        } catch (\Exception $e) {

            throw new HTTPException(
                "Invalid parameters",
                404,
                array(
                    'dev' => $e->getMessage(),
                    'internalCode' => 'Oauth001',
                ));
        }
    }

    /*
     * Cuando un cliente externo quiere permisos de un usuario nuestro, pasa primero por esta función;
     * Si esta logueado, lo mandaremos al formulario donde el usuario tendrá que validar los permisos que le solicita el cliente.
     * Si no lo esta, pasará al formulario de login.
     */
    public function auth()
    {
        try {
            $authParams = $this->getDI()->getShared('oauth2_authorization')->getGrantType('authorization_code')->checkAuthoriseParams();
        } catch (\League\OAuth2\Server\Exception\ClientException $e) {
            throw new HTTPException(
                "Invalid parameters 14",
                404,
                array(
                    'dev' => $e->getMessage(),
                    'internalCode' => 'Oauth002',
                ));
        }
        $this->session->set('oauth-client', $authParams);
        exit(header('Location: /oauth2/signin'));
    }

    /*
     * Funcion que presenta los permisos que le solicita un cliente;
     * Tanto si acepta como si no, lo devolveremos a la ruta indicada por el cliente. En caso afirmativo lo mandaremos con un código canejable por un token de acceso.
     */
    public function authorize()
    {
        if (!$user = $this->checkLogin()) {
            header("Location: /oauth2/signin");
            exit();
        } else {
            if (!$this->session->get('oauth-client'))
                exit(header("Location: /oauth2/profile/"));
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);

        $form = new AuthorizeForm();

        //$authParams = $this->getDI()->getShared('oauth2_authorization')->getGrantType('authorization_code')->checkAuthorizeParams();
        $authParams = $this->session->get('oauth-client');
        $auto_approve = isset($authParams['client_details']['auto_approve']) ? $authParams['client_details']['auto_approve'] : false;

        $view->setVar('auto_approve', $auto_approve);

        $force_approve = false;
        if (($authParams['client_details']['id'] == "staddium") && ($auto_approve))
            $force_approve = true;

        if (!$auto_approve)
        {
            // Si no tiene autoapprove, se podria comprobar si el usuario ha dado permiso con anterioridad para no molestarlo de nuevo
            // TODO
        }

        //if ($this->request->isPost())
        //{
            if ($force_approve OR $form->isValid($this->request->getQuery()) != false)
            {
                // If the user authorizes the request then redirect the user back with an authorization code
                $this->session->remove('oauth-client');
                if ($auto_approve OR ($this->request->get('authorization') === 'approve')) {
                    $code = $this->getDI()->getShared('oauth2_authorization')->getGrantType('authorization_code')->newAuthoriseRequest('user', $user['id'], $authParams);

                    $redirectUri = new \League\OAuth2\Server\Util\RedirectUri;

                    $url = $redirectUri->make(
                        $authParams['redirect_uri'],
                        [
                            'state' => $authParams['state'],
                            'code'  => $code
                        ]
                    );

                    exit(header("Location: " . $url));
                }

                // The user denied the request so redirect back with a message
                else {

                    $redirectUri = new \League\OAuth2\Server\Util\RedirectUri;

                    $url = $redirectUri->make(
                        $authParams['redirect_uri'],
                        [
                            'error' =>  'access_denied',
                            'message'   => 'Access denied by user'
                        ]
                    );

                    exit(header("Location: " . $url));
                }
            }
        //}

        /* * * * */

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("authorize", array("authParams" => $authParams, "form" => $form));
    }

    /*
     * ----------------------------------------------------------------------------------------------------------------
     * De aquí para abajo, Funciones dedicadas a la gestión de usuarios (registro / perfil / recuperar clave / validación de email / login)
     * ----------------------------------------------------------------------------------------------------------------
     */


    public function forgotUserPassword()
    {
        if ($this->checkLogin())
        {
            if ($authParams = $this->session->get('oauth-client'))
                exit(header("Location: /oauth2/authorize"));
            else
                exit(header("Location: /oauth2/profile/"));
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $form = new ForgotPasswordForm();
        $view->setVar("form", $form);
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost()) == true) {
                if ($userd = User::findFirstByEmail($this->request->getPost('email')))
                {
                    if (!$userd->getLastUnusedResetPasswordSentInTheLastFiveMinutesAgo())
                    {
                        $userd->sendResetPassword();
                        $view->setVar('success', true);
                    }
                    else
                    {
                        $view->setVar("errorMsg", $this->getTranslation()->_('WAIT_MORE_FOR_RESET_PASSWORD'));
                    }
                }
                else
                {
                    $view->setVar("errorMsg", $this->getTranslation()->_('NO_EMAIL_ACCOUNT_ASSOCIATED'));
                }
            }
        }


        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("forgotUserPassword");
    }

    public function changeUserPassword()
    {
        if (!$user = $this->checkLogin()) {
            header("Location: /oauth2/signin");
            exit();
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);
        $view->setVar("user", $this->auth->getUser());

        $view->setVar("form", new ChangeUserPasswordForm());

        if ($this->request->isPost())
        {
            if ($view->form->isValid($this->request->getPost()) == true)
            {
                if (!$this->auth->samesAsCurrentPassword($this->request->get('currentPassword')))
                    $view->setVar("errorMsg", $this->getTranslation()->_('INVALID_CURRENT_PASSWORD'));
                else
                {
                    if (!$this->auth->changePassword($this->request->get('password')))
                        $view->setVar("errorMsg", $this->getTranslation()->_('ERROR_SAVING_CHANGES'));
                    else
                    {
                        $this->auth->getUser()->addPasswordChangeLog($this->request->getClientAddress($this->config->proxy->enabled), $this->request->getUserAgent());
                        $this->session->set("profile_message", 'PASSWORD_SUCCESSFULLY_CHANGED');
                        exit(header("Location: /oauth2/profile"));
                    }

                }
            }
        }

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("changeUserPassword");
    }


    public function changeUserEmail()
    {
        if (!$user = $this->checkLogin()) {
            header("Location: /oauth2/signin");
            exit();
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);
        $view->setVar("user", $this->auth->getUser());

        $view->setVar("form", new ChangeUserEmailForm());
        if ($this->request->isPost())
        {
            if ($view->form->isValid($this->request->getPost()) == true)
            {
                if (!$this->auth->samesAsCurrentPassword($this->request->get('currentPassword')))
                    $view->setVar("errorMsg", $this->getTranslation()->_('INVALID_CURRENT_PASSWORD'));
                else
                {
                    if ($this->auth->getUser()->email == $this->request->get('email'))
                        $view->setVar("errorMsg", $this->getTranslation()->_('NEW_EMAIL_EQUAL_CURRENT_EMAIL'));
                    else {
                        if ($this->auth->getUser()->getLastChangeEmailVerificationCodeInTheLastFiveMinutesAgo())
                            $view->setVar("errorMsg", $this->getTranslation()->_('WAIT_MORE_FOR_CHANGE_EMAIL'));
                        else
                        {
                            $this->auth->getUser()->disableAllActiveEmailValidations();
                            $this->auth->getUser()->sendNewEmailConfirmation($this->request->get('email'));
                            $view->setVar("successMsg", $this->getTranslation()->_('SENT_CONFIRMATION_EMAIL_SUCCESSFULLY'));
                        }
                    }
                }
            }
        }

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("changeUserEmail");
    }

    public function resetUserPassword($code, $email)
    {
        if ($this->checkLogin()) {
            if ($authParams = $this->session->get('oauth-client'))
                exit(header("Location: /oauth2/authorize"));
            else
                exit(header("Location: /oauth2/profile/"));
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $success = false;
        $valid_code = false;
        $already_changed = false;
        if ($reset_password = User::getByResetPasswordData($code, $email))
        {
            $valid_code = true;
            $already_changed = ($reset_password['reset'] == 1);
            if (!$already_changed)
            {
                $form = new ResetPasswordForm();
                $view->setVar("form", $form);
                if ($this->request->isPost()) {
                    if ($form->isValid($this->request->getPost()) == true) {
                        $userd = User::findFirstById($reset_password['users_id']);
                        $userd->setPassword($this->request->getPost('password'));
                        $userd->must_change_password = '0';
                        $userd->save();

                        $userd->addPasswordChangeLog($this->request->getClientAddress($this->config->proxy->enabled), $this->request->getUserAgent());
                        User::confirmResetPasswordById($reset_password['id']);

                        $success = true;
                    }
                }
            }
        }

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("resetUserPassword", array("success" => $success, "valid_code" => $valid_code, "already_changed" => $already_changed));
    }

    public function signUp()
    {
        if ($this->checkLogin())
        {
            if ($authParams = $this->session->get('oauth-client'))
                exit(header("Location: /oauth2/authorize"));
            else
                exit(header("Location: /oauth2/profile/"));
        }

        $form = new SignUpForm();
        $view = new \Phalcon\Mvc\View\Simple();


        $view->setVar("t", $this->getTranslation());
        $view->setVar("form", $form);

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        if ($this->request->isPost())
        {
            if ($form->isValid($this->request->getPost()) != false)
            {
                $reserved_nicks = array("staddium", "stadium", "staddiumorg", "staddium_org", "staff", "admin", "admin_", "_admin_", "_admin", "administrador", "administracion", "arbitro", "referee", "x3no");
                if (in_array($this->request->getPost('nick', 'striptags'), $reserved_nicks))
                    $view->setVar("errorMsg", "Reserved nickname, sorry");
                else
                {
                    $userd = new User();
                    $userd->assign(array(
                        'nick' => $this->request->getPost('nick', 'striptags'),
                        'email' => $this->request->getPost('email'),
                        'password' => $this->request->getPost('password')
                    ));

                    if ($userd->save()) {
                        $view->success = true;
                    }
                    foreach ($userd->getMessages() as $errorMsg)
                    {
                        $view->setVar("errorMsg", $errorMsg);
                        break 1;
                    }
                }
                //$this->flash->error($user->getMessages());
            }
        }

        // Render a view
        echo $view->render("signup");
    }

    public function editUserPreferences() {

        if (!$user = $this->checkLogin()) {
            header("Location: /oauth2/signin");
            exit();
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);
        $view->setVar("user", $this->auth->getUser());

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("editUserPreferences", array("logoutForm" => $logoutForm));
    }


    public function editUserProfile() {

        if (!$user = $this->checkLogin()) {
            header("Location: /oauth2/signin");
            exit();
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());
        if ($this->session->has('profile_message'))
        {
            $view->setVar('successMsg', $this->getTranslation()->_($this->session->get('profile_message')));
            $this->session->remove('profile_message');
        }

        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);
        $view->setVar("user", $this->auth->getUser());

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("editUserProfile", array("logoutForm" => $logoutForm));
    }

    public function signIn() {

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        if ($this->checkLogin())
        {
            if ($authParams = $this->session->get('oauth-client'))
                exit(header("Location: /oauth2/authorize"));
            else
                exit(header("Location: /oauth2/profile/"));
        } else {
            $form = new LoginForm();
            $view->setVar('form', $form);
            try {
                if (!$this->request->isPost())
                {
                    if ($this->auth->hasRememberMe()) {
                        return $this->auth->loginWithRememberMe();
                    }
                } else {
                    if ($form->isValid($this->request->getPost()))
                    {
                        $this->auth->check(array(
                            'email' => $this->request->getPost('email'),
                            'password' => $this->request->getPost('password'),
                            'remember' => $this->request->getPost('remember')
                        ));

                        header("Location: /oauth2/authorize");
                        exit();
                    } else (die($form->niceErrors()));
                }
            } catch (\pfmAPI\Library\Auth\Exception $e) {
                $view->setVar("errorMsg", $this->getTranslation()->_('INVALID_USERNAME_OR_PASSWORD'));
            }

            /* * * * */
            //A trailing directory separator is required
            $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

            // Render a view
            echo $view->render("signin");
        }

    }

    public function logOut()
    {
        if (!$this->checkLogin()) {
            exit(header("Location: /oauth2/signin/"));
        }

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());
        $logoutForm = new LogoutForm();
        $view->setVar("logoutForm", $logoutForm);

        if ($logoutForm->isValid($this->request->getQuery()) == false) {
            $view->setVar("errorMsg", $this->getTranslation()->_('INVALID_CSRF'));
        } else {
            $this->auth->remove();
            exit(header("Location: /oauth2/signin"));
        }

        /* * * * */
        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("logout");
    }


    public function confirmUserEmail($code, $email)
    {
        $valid_code = false;
        $already_confirmed = false;

        $view = new \Phalcon\Mvc\View\Simple();
        $view->setVar("t", $this->getTranslation());

        if ($confirmation = User::getByConfirmationEmailData($code, $email))
        {
            $valid_code = true;
            $already_confirmed = ($confirmation['confirmed'] == 1);

            if ($confirmation['expired'])
                $view->setVar("errorMsg", $this->getTranslation()->_('EMAIL_VERIFICATION_EXPIRED'));
            else
            {
                if (!$already_confirmed)
                {
                    $userd = User::findFirstById($confirmation['users_id']);
                    if ($userd->email != $confirmation['email'])
                    {
                        $checkUser = User::find(
                            array(
                                "conditions" => "email = ?0",
                                "bind"       => array(0 => $confirmation['email'])
                            ));

                        if (count($checkUser) == 0)
                        {
                            $userd->addEmailChangeLog($this->request->getClientAddress($this->config->proxy->enabled), $this->request->getUserAgent(), $userd->email);
                            User::doEmailValidationById($confirmation['id']);
                            $userd->email = $confirmation['email'];
                            $userd->save();
                        }
                        else
                        {
                            $view->setVar("errorMsg", $this->getTranslation()->_('EMAIL_ALREADY_TAKEN', array("nickname" => $checkUser[0]->nick)));
                        }
                    }
                    else
                    {
                        User::doEmailValidationById($confirmation['id']);
                        $userd->active = 1;
                        $userd->save();
                    }

                }
            }
        }

        //A trailing directory separator is required
        $view->setViewsDir( BASE_DIR . $this->parameters->core->viewsDir);

        // Render a view
        echo $view->render("confirmUserEmail", array("valid_code" => $valid_code, "already_confirmed" => $already_confirmed));
    }
}
