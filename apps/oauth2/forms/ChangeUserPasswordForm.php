<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class ChangeUserPasswordForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "currentPassword" => array(
                'type' => "password",
                'minLength' => 8,
            ),
            "password" => array(
                'type' => "password",
                'validator' => "identical",
                'with' => 'confirmPassword',
                'minLength' => 8,
            ),
            "confirmPassword" => array(
                'type' => "password",
            ),
        );

        $this->addFields();
    }
}