<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class EditImageForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "delete" => array(
                'validator' => 'identical',
                'type'  => "check",
                'value' => '1',
                'optional' => true,
            ),
        );

        $this->addFields();
    }
}