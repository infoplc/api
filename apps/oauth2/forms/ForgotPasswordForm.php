<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class ForgotPasswordForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "email" => array(
                'validator' => "email",
            ),
        );

        $this->addFields();
    }
}