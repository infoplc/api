<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class LoginForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "email" => array(
                'validator' => "email",
            ),
            "password" => array(
                'type' => "password",
                'minLength' => 8,
            ),
            "remember" => array(
                'optional' => true,
                'validator' => 'identical',
                'type'  => "check",
                'value' => 'yes'
            ),
        );

        $this->addFields();
    }
}