<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class ResetPasswordForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "password" => array(
                'type' => "password",
                'validator' => "identical",
                'with' => 'confirmPassword',
                'minLength' => 8,
            ),
            "confirmPassword" => array(
                'type' => "password",
            ),
        );

        $this->addFields();
    }
}