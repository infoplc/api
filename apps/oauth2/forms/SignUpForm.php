<?php
namespace pfmAPI\Apps\Oauth2\Forms;

use pfmAPI\Forms\BaseForm;

class SignUpForm extends BaseForm
{
    public $items;

    public function __construct(){
        $this->items = array(
            "csrf" => array(
                'validator' => "identical",
                'value' => $this->getCSRF(),
                'type' => "hidden",
            ),
            "nick" => array(
                'validator' => 'alphanumeric_extended',
                'minLength' => 3,
                'maxLength' => 14,
            ),
            "email" => array(
                'validator' => "email",
            ),
            "password" => array(
                'type' => "password",
                'validator' => "identical",
                'with' => 'confirmPassword',
                'minLength' => 8,
            ),
            "confirmPassword" => array(
                'type' => "password",
            ),
            "terms" => array(
                'validator' => 'identical',
                'type'  => "check",
                'value' => 'yes'
            ),
        );

        $this->addFields();
    }
}