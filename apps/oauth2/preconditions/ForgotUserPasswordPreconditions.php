<?php
namespace pfmAPI\Apps\Oauth2\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class ForgotUserPasswordPreconditions extends BasePreconditions
{
    public function doCheck()
    {
        // nothing to do here...
    }
}