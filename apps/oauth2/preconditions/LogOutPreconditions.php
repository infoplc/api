<?php
namespace pfmAPI\Apps\Oauth2\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class LogOutPreconditions extends BasePreconditions
{
    public function doCheck()
    {
        // nothing to do here...
    }
}