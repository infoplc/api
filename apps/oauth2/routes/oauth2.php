<?php

return call_user_func(function(){

    $userCollection = new \Phalcon\Mvc\Micro\Collection();

    $userCollection
        ->setPrefix('/oauth2')
        ->setHandler('\pfmAPI\Apps\Oauth2\Controllers\Oauth2Controller')
        ->setLazy(true);

    /*
     * /!\ Las acciones del oauth2, la mayoría deberian ser POST, pero estan los GET
     *     para facilitar el debug; Para la versión de producción deberían desaparecer
     */

    $userCollection->get('/logout', 'logOut');
    $userCollection->post('/logout', 'logOut');
    $userCollection->get('/token', 'accessToken');
    $userCollection->post('/token', 'accessToken');
    $userCollection->get('/auth', 'auth');

    $userCollection->get('/authorize', 'authorize');
    $userCollection->post('/authorize', 'authorize');

    $userCollection->get('/signin', 'signIn');
    $userCollection->post('/signin', 'signIn');

    $userCollection->get('/profile', 'editUserProfile');
    $userCollection->post('/profile', 'editUserProfile');

    $userCollection->get('/profile/preferences', 'editUserPreferences');
    $userCollection->post('/profile/preferences', 'editUserPreferences');
    $userCollection->get('/profile/password', 'changeUserPassword');
    $userCollection->post('/profile/password', 'changeUserPassword');
    $userCollection->get('/profile/email', 'changeUserEmail');
    $userCollection->post('/profile/email', 'changeUserEmail');


    $userCollection->get('/signup', 'signUp');
    $userCollection->post('/signup', 'signUp');

    $userCollection->get('/confirm/{code}/{email}', 'confirmUserEmail');
    $userCollection->get('/reset-password/{code}/{email}', 'resetUserPassword');
    $userCollection->post('/reset-password/{code}/{email}', 'resetUserPassword');

    $userCollection->get('/forgot-password', 'forgotUserPassword');
    $userCollection->post('/forgot-password', 'forgotUserPassword');

    return $userCollection;
});
