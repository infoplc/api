<?php
namespace pfmAPI\Apps\User\Controllers;

use pfmAPI\Controllers\RESTController,
    pfmAPI\Exceptions\HTTPException,
    pfmAPI\Apps\User\Models\User,
    pfmAPI\Apps\Misc\Models\Steam,
    pfmAPI\Apps\Misc\Models\PVPNet;

class UserController extends RESTController {

    protected $allowedFields = array(
        'search' => array(
            'id',
            'nick',
            'tag',
            'games_id',
        ),
        'partials' => array(
            'id',
            'nick',
            'tag',
            'games_id',
            'active',
            'logo',
            'creation'
        )
    );

    /**
     * Returns nick, email, creation, last_activity and banned of the specified user
     *
     * @return array
     * @throws HTTPException
     */
    public function getProfile() {
        $records = array();
        $collection = User::findIn(explode(",", $this->request->get('users_id')));
        foreach ($collection as $item) {

            $avatar = $item->getRelatedAvatar();
            $background = $item->getRelatedBackground();
            $gamePlatforms = $item->getGamePlatforms()->toArray();
            foreach($gamePlatforms as &$gamePlatform) {
                if($gamePlatform['game_platforms_id'] == 1) {
                    $gamePlatform['steam_info'] = Steam::getUserInfo(array($gamePlatform['user_platform_id']));
                } elseif($gamePlatform['game_platforms_id'] == 3) {
                    $gamePlatform['pvpnet_info'] = PVPNet::getSummonersInfo(array($gamePlatform['user_platform_id']));
                }
            }
            $records[] = array(
                'id'            => $item->id,
                'nick'          => $item->nick,
                'creation'      => $item->creation,
                'last_activity' => $item->last_activity,
                'banned'        => $item->banned,
                'game_platforms' => $gamePlatforms,
                'avatar'        => $avatar ? array("id" => $avatar->id, "path" => $avatar->path, "width" => $avatar->width, "height" => $avatar->height) : false,
                'background'    => $background ? array("id" => $background->id, "path" => $background->path, "width" => $background->width, "height" => $background->height) : false,
            );
        }

        return $records;
    }

    public function search() {
        return User::search($this->searchFields, $this->partialFields);
    }

    public function updateProfile($id) {
        /** @var User $user */
        if($user = User::findFirst($id)) {
            if ($this->request->get('nick'))
                $user->setNick($this->request->get('nick'));

            if ($this->request->get('email'))
                $user->setEmail($this->request->get('email'));

            if ($this->request->get('password'))
                $user->setPassword($this->request->get('password'));

            if ($user->update() == false) {
                $messages = implode(',', $user->getMessages());
                throw new HTTPException(
                    "The user could not be updated right now.",
                    500,
                    array(
                        'dev' => $messages,
                        'internalCode' => 'UserC005',
                    ));
            }

            return array();
        } else {
            throw new HTTPException(
                "The user does not exist.",
                404,
                array(
                    'dev' => 'The user with id #'.htmlentities($id, ENT_QUOTES, 'UTF-8').' does not exist in the database',
                    'internalCode' => 'UserC003',
                ));
        }
    }

    /**
     * Used to suspend a user account.
     * It always has to be called by the user that wants to suspend its account.
     *
     * @return array
     * @throws HTTPException
     */
    public function suspendUser() {
        //TODO: Implementado de la forma antigua aún
        /** @var User $user */
        if ($user = User::findFirst($accessTokenInfo['owner_id']))
        {
            if($user->checkPassword($this->request->get('password'))) {
                $user->setSuspended(true);
                if($user->save() == false) {
                    throw new HTTPException(
                        "The user could not be suspended right now.",
                        500,
                        array(
                            'internalCode' => 'UserC009',
                        ));
                }
            }
            else
            {
                throw new HTTPException(
                    "The user password is not valid.",
                    404,
                    array(
                        'dev' => 'The user password is not valid',
                        'internalCode' => 'UserC003',
                    ));
            }
        }
        else
        {
            throw new HTTPException(
                "The user does not exist.",
                404,
                array(
                    'dev' => 'The user with id #'.$accessTokenInfo['owner_id'].' does not exist in the database',
                    'internalCode' => 'UserC003',
                ));
        }

        return array();
    }
}