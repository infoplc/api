<?php
namespace pfmAPI\Apps\User\Models;

/**
 * RememberTokens
 * Stores the remember me tokens
 */
class RememberTokens extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $users_id;

    /**
     *
     * @var string
     */
    public $token;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var integer
     */
    public $created_at;


    public function getSource()
    {
        return "user_remember_tokens";
    }

    public function beforeValidationOnCreate()
    {
        // Timestamp the confirmaton
        $this->created_at = date("Y-m-d H:i:s");
    }

    public function initialize()
    {
        $this->belongsTo('users_id', 'pfmAPI\Apps\User\Models\User', 'id', array(
            'alias' => 'user'
        ));
    }

    static public function getRT($users_id, $token, $user_agent)
    {

        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = '  SELECT `user_remember_tokens`.*
                                FROM `user_remember_tokens`
                                WHERE
                                    `users_id` = :users_id AND
                                    `token` = :token AND
                                    `user_agent` = :user_agent';

        $result = $connection->query($query, array(
            'users_id'               => $users_id,
            'token'                => $token,
            'user_agent'                => $user_agent,
        ));

        return $result->fetch();
    }
}
