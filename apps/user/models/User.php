<?php
namespace pfmAPI\Apps\User\Models;

use Phalcon\Validation;
use pfmAPI\Apps\Misc\Models\Misc,
    \pfmAPI\Exceptions\HTTPException,
    \Phalcon\DI,
    \Phalcon\Validation\Validator\Uniqueness,

    pfmAPI\Models\ModelPrivilegesInterface;

class User extends \Phalcon\Mvc\Model implements ModelPrivilegesInterface {
    public $id;

    public $nick;

    public $password;

    public $created_at;

    public $email;

    public $active;

    public function getSource()
    {
        return "users";
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
    }

    public static function getUserPrivilege($objectId,$userId) {
        /** @var UsersHasPrivileges $userPrivilege */
        $userPrivilege =  UsersHasPrivileges::findFirst(array(
            "conditions" => '[users_id] = ?0',
            "bind"       => array(
                0 => $userId,
            )
        ));
        if($userPrivilege)
            return $userPrivilege->getPrivilege()->getName();
        return null;
    }

    public static function getIsUserActive($objectId,$userId) {
        return (bool)User::findFirst(array(
            "conditions" => '[id] = ?0 AND [active] = 1',
            "bind"       => array(
                0 => $userId,
            )
        ));
    }

    public static function getPrivilegesIds()
    {
        return array('Super Administrator');
    }

    /**
     * @param $userId
     * @return User
     * @throws HTTPException
     */
    public static function getUser($userId) {
        /** @var User $user */
        $user = User::findFirst($userId);
        if(!$user) {
            throw new HTTPException(
                "The user with ID $userId does not exist",
                401,
                array(
                    'internalCode' => 'User002',
                ));
        }
        return $user;
    }

    /**
     * @param string $nick
     */
    public function setNick($nick) {
        $this->nick = $nick;
    }

    /**
     * @return string
     */
    public function getNick() {
        return $this->nick;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param bool $suspended
     */
    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;
    }

    /**
     * @return bool
     */
    public function getSuspended()
    {
        return $this->suspended;
    }

    /**
     * @param bool $must_change_password
     */
    public function setMustChangePassword($must_change_password)
    {
        $this->must_change_password = $must_change_password;
    }

    /**
     * @return bool
     */
    public function getMustChangePassword()
    {
        return $this->must_change_password;
    }

    /**
     * @param string $created_at
     */
    public function setCreation($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getCreation()
    {
        return $this->created_at;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        //TODO: Check valid email
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $password Plain password
     */
    public function setPassword($password)
    {
        $this->password = $this->encryptPassword($password);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function registerUserThrottling($user_id) {
        //TODO: Añadir un intento de login; X intentos = Intento de ataque por fuerza bruta
    }

    public function encryptPassword($password) {
        //TODO: Escoger método de encriptación de password de usuario
        return $this->getDI()
            ->getSecurity()
            ->hash($password);
    }

    public function checkPassword($plainPassword) {
        if (!$this->getDI()->getSecurity()->checkHash($plainPassword, $this->getPassword())) {
            $this->registerUserThrottling($this->getId());
            return false;
        }

        return true;
    }

    public function getIPs() {
        $getIpsSql = 'SELECT * FROM `user_ips` WHERE `users_id` = :user_id ORDER BY id DESC';
        $statement = $this->getReadConnection()->query($getIpsSql, array('user_id' => $this->getId()));
        return $statement->fetchAll();
    }

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        if (empty($this->password)) {
            // Generate a plain temporary password
            $tempPassword = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(12)));

            // Use this password as default
            $this->password = $this->getDI()
                ->getSecurity()
                ->hash($tempPassword);
        }


        // The account must be confirmed via e-mail
        $this->active = false;

        // The account is not suspended by default
        $this->suspended = false;

        // Timestamp of the creation
        $this->created_at = date("Y-m-d H:i:s");
    }

    /*
     * Best function name of the year. What year? Every year.
     */
    public function getLastUnusedResetPasswordSentInTheLastFiveMinutesAgo()
    {
        $connection = $this->getDI()->get('db');
        $query = 'SELECT * FROM `user_reset_passwords` where `users_id` = :users_id and `created_at` >= :time and reset = 0';
        $result = $connection->query($query, array(
            'users_id'          => $this->id,
            'time'              => date("Y-m-d H:i:s", time() - 60 * 5),
        ));

        return $result->fetch();
    }

    public function sendResetPassword()
    {
        $code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));


        $this->getDI()
            ->getMail()
            ->send(array(
                $this->email => $this->nick
            ), "Reiniciar clave", 'reset', array(
                'resetUrl' => '/oauth2/reset-password/' . $code . '/' . urlencode($this->email)
            ));

        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_reset_passwords` (`users_id`, `code`, `created_at`) VALUES (:users_id, :code, :date)';
        $connection->query($query, array(
            'users_id'  => $this->id,
            'code'      => $code,
            'date'      => date('Y/m/d H:i:s'),
        ));

        return true;
    }

    public function getLastChangeEmailVerificationCodeInTheLastFiveMinutesAgo()
    {
        $connection = $this->getDI()->get('db');
        $query = 'SELECT * FROM `user_email_confirmations` where `users_id` = :users_id and `created_at` >= :time and confirmed = 0 and expired = 0';
        $result = $connection->query($query, array(
            'users_id'          => $this->id,
            'time'              => date("Y-m-d H:i:s", time() - 60 * 5),
        ));

        return $result->fetch();
    }

    public function sendNewEmailConfirmation($email)
    {
        $code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

        $this->getDI()
            ->getMail()
            ->send(array(
                $email => $this->nick
            ), "Confirmar email", 'confirmation', array(
                'confirmUrl' => '/oauth2/confirm/' . $code . '/' . urlencode($email)
            ));

        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_email_confirmations` (users_id, email, code, created_at) VALUES (:users_id, :email, :code, :date)';
        $connection->query($query, array(
            'email'     => $email,
            'users_id'  => $this->id,
            'code'      => $code,
            'date'      => date('Y/m/d H:i:s'),
        ));

        return true;
    }

    public function sendEmailConfirmation()
    {
        $code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

        $this->getDI()
            ->getMail()
            ->send(array(
                $this->email => $this->nick
            ), "Confirmar email", 'confirmation', array(
                'confirmUrl' => '/oauth2/confirm/' . $code . '/' . urlencode($this->email)
            ));

        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_email_confirmations` (users_id, email, code, created_at) VALUES (:users_id, :email, :code, :date)';
        $connection->query($query, array(
            'email'     => $this->email,
            'users_id'  => $this->id,
            'code'      => $code,
            'date'      => date('Y/m/d H:i:s'),
        ));

        return true;
    }

    /**
     * Send a confirmation e-mail to the user if the account is not active
     */
    public function afterSave()
    {
        // TODO: Implementar el tema de enviar el correo. Ahora mismo no se cumple la siguiente condición
        if ($this->active == false) {
            if ($this->sendEmailConfirmation()) {
              /*  $this->getDI()
                    ->getFlash()
                    ->notice('A confirmation mail has been sent to ' . $this->email);*/
            }
        }

    }

    /**
     * Validate that emails are unique across users
     */
    public function validation()
    {
/*
        $validator = new Validation();

        $validator->add(
            'nick',
            \Phalcon\Validation\Validator\Uniqueness(array(
                "field" => "nick",
                "message" => "The nick is already registered"
            ))
        );
        $validator->add(
            'email',
            \Phalcon\Validation\Validator\Uniqueness(array(
                "field" => "email",
                "message" => "The email is already registered"
            ))
        );


        return ($this->validate($validator) != true);
*/
        return true;
    }

    static public function getUsers($ids)
    {
        return self::query()
            ->columns(array('id', 'nick', 'email', 'created_at'))
            ->inWhere('id', $ids)
            ->execute();
    }

    public static function findIn(array $identifiers)
    {
        return self::query()
            ->inWhere('id', $identifiers)
            ->execute();
    }

    static public function getUsersByNick($nicks)
    {
        return self::query()
            ->columns(array('id', 'nick', 'email', 'created_at'))
            ->inWhere('nick', $nicks)
            ->execute();
    }


    public function sendNotificationTo($to, $code, $rel_type = null, $rel_id = null)
    {
        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_notifications` (`users_id`, `author_id`, `code`, `rel_type`, `rel_id`, `created_at`) VALUES (:to, :users_id, :code, :rel_type, :rel_id, :data)';
        $connection->query($query, array(
            'users_id'      => $this->id,
            'code'          => $code,
            'rel_type'      => $rel_type,
            'rel_id'        => $rel_id,
            'to'            => $to,
            'data'          => date('Y/m/d H:i:s'),
        ));
    }

    static public function getByConfirmationEmailData($code, $email)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = '  SELECT `user_email_confirmations`.*
                                FROM `user_email_confirmations`
                                WHERE
                                    `code` = :code AND
                                    `email` = :email';
        $result = $connection->query($query, array(
            'email'               => $email,
            'code'                => $code,
        ));

        return $result->fetch();
    }

    static public function getByResetPasswordData($code, $email)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = '  SELECT `user_reset_passwords`.*
                                FROM `user_reset_passwords`
                                INNER JOIN `users` ON
                                    `users`.email = :email
                                WHERE
                                    `code` = :code';
        $result = $connection->query($query, array(
            'email'               => $email,
            'code'                => $code,
        ));

        return $result->fetch();
    }

    static public function doEmailValidationById($user_email_confirmations_id)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = 'UPDATE `user_email_confirmations` SET `confirmed_at` = :date , `confirmed` = 1 WHERE `id` = :id';
        $connection->query($query, array(
            'id'             => $user_email_confirmations_id,
            'date'           => date('Y/m/d H:i:s'),
        ));
    }

    public function disableAllActiveEmailValidations()
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = 'UPDATE `user_email_confirmations` SET `confirmed_at` = :date, `expired` = 1 WHERE confirmed = 0 and `users_id` = :users_id';
        $connection->query($query, array(
            'users_id'             => $this->id,
            'date'           => date('Y/m/d H:i:s'),
        ));
    }

    static public function confirmResetPasswordById($users_reset_password_id)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = 'UPDATE `user_reset_passwords` SET `modified_at` = :date , `reset` = 1 WHERE `id` = :id';
        $connection->query($query, array(
            'id'             => $users_reset_password_id,
            'date'           => date('Y/m/d H:i:s'),
        ));
    }


    public function newSession($api_id)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(16, $cstrong));
        $token_life = 3600*24*30*6;
        $token_expiration = time() + $token_life;

        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_sessions` (users_id, api_key_id, token, token_expiration) VALUES (:users_id, :api_key_id, :token, :token_expiration)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'api_key_id'        => $api_id,
            'token'             => $token,
            'token_expiration'  => date('Y/m/d H:i:s', $token_expiration),
        ));

        return (object) array("value" => $token, "expiration" => $token_expiration, "life" => $token_life);
    }

    public function addPasswordChangeLog($ip, $user_agent)
    {
        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_password_changes` (users_id, ip, user_agent, date) VALUES (:users_id, :ip, :user_agent, :date)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'ip'                => $ip,
            'user_agent'        => $user_agent,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }

    public function addEmailChangeLog($ip, $user_agent, $email)
    {
        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_email_changes` (users_id, email, ip, user_agent, date) VALUES (:users_id, :email, :ip, :user_agent, :date)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'email'             => $email,
            'ip'                => $ip,
            'user_agent'        => $user_agent,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }

    public function addGamePlatformChangeLog($ip, $user_agent, $game_platform_id, $user_platform_id)
    {
        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_game_platforms_changes` (users_id, game_platforms_id, user_platform_id, ip, user_agent, date) VALUES (:users_id, :game_platforms_id, :user_platform_id, :ip, :user_agent, :date)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'game_platforms_id' => $game_platform_id,
            'user_platform_id'  => $user_platform_id,
            'ip'                => $ip,
            'user_agent'        => $user_agent,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }

    static public function addFailedLogin($user_id, $ip)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = 'INSERT INTO `user_failed_logins` (users_id, ip, date) VALUES (:users_id, :ip, :date)';
        $connection->query($query, array(
            'users_id'          => $user_id,
            'ip'                => $ip,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }

    public function setGamePlatform($game_platform, $username)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');

        $query = 'INSERT INTO `game_platforms_has_users` (`game_platforms_id`, `users_id`, `user_platform_id`) VALUES (:platform, :users_id, :username)';
        $connection->query($query, array(
            'username'          => $username,
            'users_id'          => $this->id,
            'platform'          => $game_platform,
        ));
    }

    static public function getFailedAttemptsByIP($ip)
    {
        $di = \Phalcon\DI::getDefault();
        $connection = $di->get('db');
        $query = 'SELECT COUNT(*) FROM `user_failed_logins` where `ip` = :ip and `date` >= :time';
        $result = $connection->query($query, array(
            'ip'                => $ip,
            'time'              => time() - 3600 * 6,
        ));

        return $result->fetch()[0];
    }

    public function addSuccessLogin($ip, $user_agent)
    {
        $connection = $this->getDI()->get('db');
        $query = 'INSERT INTO `user_success_logins` (users_id, ip, user_agent, date) VALUES (:users_id, :ip, :user_agent, :date)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'ip'                => $ip,
            'user_agent'        => $user_agent,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }


    /* * * * * * * * * [external] * * * * * * * * */

    public function addExternalRememberToken($token, $user_agent)
    {
        $connection = $this->getDI()->get('db_webclient');
        $query = 'INSERT INTO `user_remember_tokens` (users_id, token, user_agent, created_at) VALUES (:users_id, :token, :user_agent, :created_at)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'token'             => $token,
            'user_agent'        => $user_agent,
            'created_at'        => date("Y-m-d H:i:s"),
        ));
    }

    public function externalFindFirstById($id)
    {
        $connection = $this->getDI()->get('db_webclient');
        $query = 'SELECT * FROM `users` where `id` = :id';
        $result = $connection->query($query, array(
            'id'          => $id,
        ));

        return $result->fetch();
    }

    public function addExternalUser($id, $nick)
    {
        $connection = $this->getDI()->get('db_webclient');
        $query = 'INSERT INTO `users` (id, nick) VALUES (:id, :nick)';
        $connection->query($query, array(
            'id'          => $id,
            'nick'        => $nick,
        ));
    }

    public function updateExternalUser($id, $nick)
    {
        $connection = $this->getDI()->get('db_webclient');
        $query = 'UPDATE `users` SET `nick` = :nick WHERE `id` = :id';
        $connection->query($query, array(
            'id'          => $id,
            'nick'        => $nick,
        ));
    }

    public function addExternalSuccessLogin($ip, $user_agent)
    {
        $connection = $this->getDI()->get('db_webclient');
        $query = 'INSERT INTO `user_success_logins` (users_id, ip, user_agent, date) VALUES (:users_id, :ip, :user_agent, :date)';
        $connection->query($query, array(
            'users_id'          => $this->id,
            'ip'                => $ip,
            'user_agent'        => $user_agent,
            'date'              => date("Y-m-d H:i:s"),
        ));
    }


}