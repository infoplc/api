<?php

namespace pfmAPI\Apps\User\Models;

class UserPrivileges extends \Phalcon\Mvc\Model
{
    /** @var integer */
    protected $id;

    /** @var string */
    protected $name;

    public function getSource()
    {
        return "user_privileges";
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
