<?php
namespace pfmAPI\Apps\User\Models;

use \pfmAPI\Exceptions\HTTPException,
    \Phalcon\DI;

/**
 * Class UsersHasPrivileges
 * @package pfmAPI\Apps\User\Models
 *
 * @method UserPrivileges getPrivilege()
 */
class UsersHasPrivileges extends \Phalcon\Mvc\Model
{
    protected $users_id;

    protected $privileges_id;

    public function getSource()
    {
        return "users_has_privileges";
    }

    public function initialize()
    {
        $this->hasMany('users_id', 'pfmAPI\Apps\User\Models\User', 'id', array(
            'alias' => 'users'
        ));

        $this->hasOne('privileges_id', 'pfmAPI\Apps\User\Models\UserPrivileges', 'id', array(
            'alias' => 'privilege'
        ));
    }

    /**
     * @return mixed
     */
    public function getUsersId()
    {
        return $this->users_id;
    }

    /**
     * @param mixed $users_id
     */
    public function setUsersId($users_id)
    {
        $this->users_id = $users_id;
    }

    /**
     * @return mixed
     */
    public function getPrivilegesId()
    {
        return $this->privileges_id;
    }

    /**
     * @param mixed $privileges_id
     */
    public function setPrivilegesId($privileges_id)
    {
        $this->privileges_id = $privileges_id;
    }
}