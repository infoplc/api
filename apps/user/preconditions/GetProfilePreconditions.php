<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class GetProfilePreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "users_id" => array(
                "validator" => 'multiple_integer'
            ),
        );

        $this->token = array(
            "owner" => 'client',
            "scopes" => false
        );
    }
}