<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class LoginPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "id" => array(
                "validator" => 'integer'
            ),
            "nick" => array(
                "minLength" => 4,
                "maxLength" => 32
            ),
            "email" => array(
                "validator" => 'email',
            ),
            "password" => array(
                "minLength" => 4
            )
        );

        $this->params_at_least = array(
            array("id", "nick", "email")
        );

        $this->token = array(
            "owner" => false,
            "scopes" => false
        );
    }
}