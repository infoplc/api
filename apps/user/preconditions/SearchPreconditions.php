<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class SearchPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->setParameters(
            array(
                "q" => array(
                    "optional"  => false,
                ),
                "fields" => array(
                    "optional"  => true,
                ),
            )
        );

        $this->setToken(
            array(
                "owner" => 'client',
                "scopes" => false
            )
        );
    }
}