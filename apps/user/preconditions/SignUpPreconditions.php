<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class SignUpPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "nick" => array(
                "minLength" => 4,
                "maxLength" => 32
            ),
            "email" => array(
                "validator" => 'email',
            ),
            "password" => array(
                "minLength" => 4
            ),
        );

        $this->token = array(
            "owner" => false,
            "scopes" => false
        );
    }
}