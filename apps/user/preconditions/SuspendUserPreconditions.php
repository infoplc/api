<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class SuspendUserPreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            'password' => array(
                "minLength" => 4,
            )
        );

        $this->token = array(
            "owner" => false,
            "scopes" => false
        );
    }
}