<?php
namespace pfmAPI\Apps\User\Preconditions;

use pfmAPI\Preconditions\BasePreconditions;

class UpdateUserProfilePreconditions extends BasePreconditions
{
    public function setPreconditions()
    {
        $this->params = array(
            "nick" => array(
                "optional" => true,
                "minLength" => 4,
                "maxLength" => 32,
                "empty" => true
            ),
            "email" => array(
                "optional" => true,
                "validator" => 'email',
                "empty" => true
            ),
            "password" => array(
                "optional" => true,
                "minLength" => 4,
                "empty" => true
            ),
        );

        $this->params_at_least = array(
            array("nick", "email", "password")
        );

        $this->token = array(
            "owner" => false,
            "scopes" => false
        );
    }
}