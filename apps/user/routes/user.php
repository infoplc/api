<?php

return call_user_func(function(){

    $userCollection = new \Phalcon\Mvc\Micro\Collection();

    $userCollection
        ->setPrefix('/v1/user')
        ->setHandler('\pfmAPI\Apps\User\Controllers\UserController')
        ->setLazy(true);

    $userCollection->get('/profile', 'getProfile');
    $userCollection->get('/search', 'search');

    return $userCollection;
});