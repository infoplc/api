-- api calls
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/authentication/token/new', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/authentication/token/validate_with_login', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/signup', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/update', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/login', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/delete', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/profile', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/user/get_ips', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/team/new', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/team', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/team/user_apply', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/team/accept_apply', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/team/change_user_privileges', NULL);
INSERT INTO `api_calls` (`name`, `complexity`) VALUES ('v1/competition/new', NULL);

-- test api_key
INSERT INTO `api_key` (`username`, `api_key`, `password`) VALUES ('frontend', '1234', 'abcd');

-- team privileges
INSERT INTO `team_privileges` (`name`) VALUES ('Leader');
INSERT INTO `team_privileges` (`name`) VALUES ('Captain');
INSERT INTO `team_privileges` (`name`) VALUES ('Player');

