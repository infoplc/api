INSERT INTO `user_privileges` (`name`) VALUES ('Super Administrator');
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Super Administrator', '*', '*', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Organizer', 'Map', 'insert', 1);

