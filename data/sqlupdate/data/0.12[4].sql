INSERT INTO `users` (`id`, `nick`, `password`, `creation`, `last_activity`, `email`, `banned`, `active`, `suspended`, `must_change_password`) VALUES (1, 'dantek', '$2a$08$nsf0ouWJVE4PW6qUS9mkZOyg/FimJsJ3oCCgWPUsqV1RnGmZa9AJi', '0000-00-00 00:00:00', NULL, 'dantek.xtr@gmail.com', 0, 1, 0, 0);
INSERT INTO `users` (`id`, `nick`, `password`, `creation`, `last_activity`, `email`, `banned`, `active`, `suspended`, `must_change_password`) VALUES (2, 'cuspide', '$2a$08$nsf0ouWJVE4PW6qUS9mkZOyg/FimJsJ3oCCgWPUsqV1RnGmZa9AJi', '0000-00-00 00:00:00', NULL, 'jordi.andujar@gmail.com ', 0, 1, 0, 0);

INSERT INTO `game_platforms` (`id`, `name`, `platform_id_name`) VALUES (1, 'Steam', 'steam');
INSERT INTO `game_platforms` (`id`, `name`, `platform_id_name`) VALUES (2, 'Battle.net', 'battlenet');
INSERT INTO `game_platforms` (`id`, `name`, `platform_id_name`) VALUES (3, 'PVP.net', 'pvpnet');

INSERT INTO `games` (`id`, `game_platforms_id`, `name`) VALUES (1, 1, 'Team Fortress 2');
INSERT INTO `games` (`id`, `game_platforms_id`, `name`) VALUES (2, 1, 'DOTA 2');
INSERT INTO `games` (`id`, `game_platforms_id`, `name`) VALUES (3, 1, 'Counter-Strike: Source');
INSERT INTO `games` (`id`, `game_platforms_id`, `name`) VALUES (4, 2, 'StarCraft II');
INSERT INTO `games` (`id`, `game_platforms_id`, `name`) VALUES (5, 3, 'League of Legends');

INSERT INTO `teams` (`id`, `games_id`, `name`, `tag`, `active`, `creation`) VALUES ('1', '1', 'xeno squad', 'xs', '1', '2015-01-18 23:25:00');