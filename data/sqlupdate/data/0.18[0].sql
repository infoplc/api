set foreign_key_checks = 0;
TRUNCATE TABLE competition_privileges;
INSERT INTO `competition_privileges` (`name`) VALUES ('Competition Organizer');
INSERT INTO `competition_privileges` (`name`) VALUES ('Referee');
set foreign_key_checks = 1;

TRUNCATE TABLE acls;
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Organizer', 'Team', 'insert, penalty, accept, reject', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Referee', 'Team', 'penalty', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Organizer', 'Competition', 'edit', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Super Administrator', '*', '*', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Organizer', 'Map', 'insert', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Leader', 'User', 'accept, change_privileges, view_waiting', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Leader', 'Team', 'signup', 1);

