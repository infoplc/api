INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Leader', 'Match', 'edit', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Organizer', 'Match', 'edit', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Competition Referee', 'Match', 'edit', 1);
