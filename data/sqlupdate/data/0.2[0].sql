INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `auto_approve`) VALUES ('what', 'ever', 'client test', 0);
INSERT INTO `oauth_client_endpoints` (`client_id`, `redirect_uri`) VALUES ('what', 'http://www.google.es');
INSERT INTO `oauth_scopes` (`scope`, `name`, `description`) VALUES ('profile', 'Profile', 'Access to basic profile data');
INSERT INTO `oauth_scopes` (`scope`, `name`, `description`) VALUES ('messaging', 'Messaging', 'Access to personal messaging');