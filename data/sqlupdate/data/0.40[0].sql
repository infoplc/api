INSERT INTO `organization_privileges` (`name`) VALUES ('Organization Administrator');
INSERT INTO `organization_privileges` (`name`) VALUES ('Organization Manager');
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Organization Administrator', 'Organization', 'edit', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Organization Manager', 'Organization', 'edit', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Organization Administrator', 'User', 'add_staff, change_privileges', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Organization Administrator', 'Competition', 'assign, unassign', 1);
INSERT INTO `acls` (`privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES ('Organization Manager', 'Competition', 'assign, unassign', 1);