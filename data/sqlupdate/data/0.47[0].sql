UPDATE `user_notifications`
SET `code` = replace(code, 'TEAM_USER_REJECTED', 'TEAM_USER_REJECTED_APPLY');

UPDATE `acls` SET `accesses_names`='accept, change_privileges, view_waiting, reject, kick, invite' WHERE `id`=6 AND `privilege_name`='Leader' AND `resource_name`='User' AND `accesses_names`='accept, change_privileges, view_waiting, reject, kick';
