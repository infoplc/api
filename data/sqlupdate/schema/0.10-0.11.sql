ALTER TABLE `match_games`
  CHANGE COLUMN `duration` `duration` TIME NULL DEFAULT NULL AFTER `score`,
  ADD COLUMN `is_won` TINYINT(1) NULL DEFAULT NULL AFTER `duration`;

CREATE TABLE IF NOT EXISTS `maps` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `games_id` INT NOT NULL,
  `name` VARCHAR(64) NOT NULL,
  `is_predefined` TINYINT(1) NULL,
  PRIMARY KEY (`id`, `games_id`),
  INDEX `fk_maps_games1_idx` (`games_id` ASC),
  CONSTRAINT `fk_maps_games1`
  FOREIGN KEY (`games_id`)
  REFERENCES `games` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `competitions_has_maps` (
  `competitions_id` INT NOT NULL,
  `maps_id` INT NOT NULL,
  PRIMARY KEY (`competitions_id`, `maps_id`),
  INDEX `fk_competitions_has_maps_maps1_idx` (`maps_id` ASC),
  INDEX `fk_competitions_has_maps_competitions1_idx` (`competitions_id` ASC),
  CONSTRAINT `fk_competitions_has_maps_competitions1`
  FOREIGN KEY (`competitions_id`)
  REFERENCES `competitions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_competitions_has_maps_maps1`
  FOREIGN KEY (`maps_id`)
  REFERENCES `maps` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `match_games_has_maps` (
  `match_games_id` INT NOT NULL,
  `maps_id` INT NOT NULL,
  PRIMARY KEY (`match_games_id`, `maps_id`),
  INDEX `fk_match_games_has_maps_maps1_idx` (`maps_id` ASC),
  INDEX `fk_match_games_has_maps_match_games1_idx` (`match_games_id` ASC),
  CONSTRAINT `fk_match_games_has_maps_match_games1`
  FOREIGN KEY (`match_games_id`)
  REFERENCES `match_games` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_games_has_maps_maps1`
  FOREIGN KEY (`maps_id`)
  REFERENCES `maps` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

ALTER TABLE `competitions`
ADD COLUMN `games_per_match` INT(5) NOT NULL DEFAULT '1' AFTER `game_mode_id`;

ALTER TABLE `match_games`
CHANGE COLUMN `teams_id` `teams_id` INT(11) NULL DEFAULT NULL AFTER `matches_id`,
ADD COLUMN `internal_match_game_id` INT(11) NOT NULL AFTER `teams_id`;
