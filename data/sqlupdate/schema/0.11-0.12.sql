ALTER TABLE `maps`
  ADD COLUMN `users_id` INT NOT NULL AFTER `games_id`,
  ADD INDEX `fk_maps_users1_idx` (`users_id`),
  ADD CONSTRAINT `fk_maps_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `user_privileges`
  DROP COLUMN `item`,
  DROP COLUMN `action`,
  DROP COLUMN `value`;