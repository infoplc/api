ALTER TABLE `competitions_has_teams`
  ADD COLUMN `waiting` TINYINT(1) NOT NULL AFTER `accepted`,
  ADD COLUMN `rejected` TINYINT(1) NOT NULL AFTER `waiting`;