ALTER TABLE `competitions_has_teams`
  ADD COLUMN `rejected_by_users_id` INT(11) NULL DEFAULT NULL AFTER `accepted_by_users_id`,
  ADD INDEX `rejected_by_users_id` (`rejected_by_users_id`),
  ADD CONSTRAINT `fk_competitions_has_teams1_rejected_by_users_id2` FOREIGN KEY (`rejected_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
