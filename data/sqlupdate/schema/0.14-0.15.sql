ALTER TABLE `competitions`
  ADD COLUMN `has_started` TINYINT(1) NULL DEFAULT NULL AFTER `featured_end_time`;
ALTER TABLE `competitions`
  ADD COLUMN `has_waiting_list` TINYINT(1) NULL DEFAULT NULL AFTER `has_started`;
ALTER TABLE `competitions_has_teams`
  ADD COLUMN `has_assigned_matches` TINYINT(1) NULL DEFAULT NULL AFTER `weight`;