RENAME TABLE `users_email_confirmations`  TO `user_email_confirmations`; 
RENAME TABLE `users_success_logins`       TO `user_success_logins`; 
RENAME TABLE `users_reset_passwords`      TO `user_reset_passwords`; 
RENAME TABLE `users_remember_tokens`      TO `user_remember_tokens`; 
RENAME TABLE `users_password_changes`     TO `user_password_changes`; 
RENAME TABLE `users_failed_logins`        TO `user_failed_logins`; 

CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `code` varchar(32) NOT NULL,
  `rel_type` varchar(14) DEFAULT NULL,
  `rel_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `removed_at` datetime DEFAULT NULL,
  `readed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_notifications_users1` (`users_id`),
  KEY `fk_user_notifications_users2` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `user_notifications`
  ADD CONSTRAINT `fk_user_notifications_users2` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_notifications_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
