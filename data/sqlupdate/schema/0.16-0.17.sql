set foreign_key_checks = 0;


ALTER TABLE `competitions`
CHANGE COLUMN `games_per_match` `games_per_match` INT(5) NULL DEFAULT '1' AFTER `game_mode_id`;
ALTER TABLE `tournaments`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `competitions`
ALTER `games_id` DROP DEFAULT;
ALTER TABLE `competitions`
CHANGE COLUMN `games_id` `games_id` INT(11) NOT NULL AFTER `id`;

set foreign_key_checks = 1;