ALTER TABLE `teams_has_users`
ADD COLUMN `apply_date` DATETIME NULL AFTER `accepted_by_users_id`;

ALTER TABLE `teams_has_users`
ALTER `joined` DROP DEFAULT;
ALTER TABLE `teams_has_users`
CHANGE COLUMN `joined` `joined` DATETIME NULL AFTER `team_privileges_id`;