ALTER TABLE `competitions_has_teams`
ADD COLUMN `state` ENUM('accepted','waiting','rejected','dropped') NOT NULL DEFAULT 'waiting' AFTER `rejected`;
ALTER TABLE `competitions_has_teams`
DROP COLUMN `accepted`,
DROP COLUMN `waiting`,
DROP COLUMN `rejected`;