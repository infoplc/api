ALTER TABLE `teams_has_matches`
  ADD COLUMN `own_score` DECIMAL(10,2) NULL AFTER `play_date_proposal`,
  ADD COLUMN `opponent_score` DECIMAL(10,2) NULL AFTER `own_score`,
  ADD COLUMN `state` ENUM('win','draw','loss') NULL AFTER `opponent_score`;

ALTER TABLE `match_games`
  CHANGE COLUMN `score` `own_score` DECIMAL(10,2) NULL DEFAULT NULL AFTER `internal_match_game_id`,
  ADD COLUMN `opponent_score` DECIMAL(10,2) NULL DEFAULT NULL AFTER `own_score`,
  ADD COLUMN `state` ENUM('win','draw','loss') NULL DEFAULT NULL AFTER `opponent_score`,
  DROP COLUMN `is_won`;

ALTER TABLE `competitions`
  ADD COLUMN `auto_accept_scores` TINYINT(1) NULL AFTER `has_waiting_list`;

ALTER TABLE `matches`
  CHANGE COLUMN `state` `state` ENUM('opened','dated','playing','played','scored','closed') NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `end_limit_time`;
