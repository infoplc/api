ALTER TABLE `users_email_confirmations` ADD `confirmed_at` DATETIME NULL DEFAULT NULL AFTER `date`;
ALTER TABLE `users_email_confirmations` CHANGE `date` `created_at` DATETIME NOT NULL;

CREATE TABLE IF NOT EXISTS `users_reset_passwords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `code` varchar(48) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NULL DEFAULT NULL,
  `reset` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `users_password_changes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(10) unsigned NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(48) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
