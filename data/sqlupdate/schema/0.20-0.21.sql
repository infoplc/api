ALTER TABLE `game_platforms_has_users` CHANGE `user_platform_id` `user_platform_id` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

CREATE TABLE IF NOT EXISTS `user_game_platforms_changes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `game_platforms_id` INT NOT NULL,
  `user_platform_id` VARCHAR(256) NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(48) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `user_game_platforms_changes`
  ADD CONSTRAINT `fk_platforms_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_platforms_game_platforms` FOREIGN KEY (`game_platforms_id`) REFERENCES `game_platforms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE TABLE IF NOT EXISTS `user_email_changes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `email` VARCHAR(256) NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(48) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `user_email_changes`
  ADD CONSTRAINT `fk_email_user` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
