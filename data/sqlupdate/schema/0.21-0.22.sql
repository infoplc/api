ALTER TABLE `user_email_confirmations` ADD `email` VARCHAR(256) NOT NULL AFTER `users_id`;
ALTER TABLE `user_email_confirmations` ADD `expired` TINYINT(1) NOT NULL DEFAULT '0' AFTER `confirmed`;