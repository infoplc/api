ALTER TABLE `competitions`
ADD COLUMN `state` ENUM('preparation','signup','playing','finished') NULL DEFAULT 'preparation' AFTER `auto_accept_scores`,
ADD COLUMN `max_teams` INT(11) NULL DEFAULT NULL AFTER `state`;
ALTER TABLE `competitions`
DROP COLUMN `has_started`;