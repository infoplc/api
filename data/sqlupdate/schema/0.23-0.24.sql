ALTER TABLE `teams_has_users`
ADD COLUMN `kicked_by_users_id` INT NULL AFTER `apply_date`,
ADD CONSTRAINT `FK_teams_has_users_users3` FOREIGN KEY (`kicked_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `teams_has_users`
ADD COLUMN `unkicked_by_users_id` INT NULL AFTER `kicked_by_users_id`,
ADD CONSTRAINT `FK_teams_has_users_users4` FOREIGN KEY (`unkicked_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;