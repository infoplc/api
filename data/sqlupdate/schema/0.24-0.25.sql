ALTER TABLE `teams`
ADD COLUMN `description` TEXT NULL AFTER `creation`;

CREATE TABLE `team_names_history` (
  `teams_id` INT NOT NULL,
  `date` DATETIME NOT NULL,
  `name` VARCHAR(45) NULL,
  `tag` VARCHAR(16) NULL,
  PRIMARY KEY (`teams_id`, `date`),
  CONSTRAINT `FK__teams` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `team_names_history`
ADD COLUMN `users_id` INT NOT NULL AFTER `date`,
ADD COLUMN `ip` VARCHAR(32) NOT NULL AFTER `users_id`,
ADD CONSTRAINT `FK_team_names_history_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

