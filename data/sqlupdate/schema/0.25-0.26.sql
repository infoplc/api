CREATE TABLE `team_users_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `teams_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `by_users_id` INT NULL,
  `action` ENUM('apply','join','leave','kick','unkick') NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_teams` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_users_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_users_2` FOREIGN KEY (`by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `images` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `path` VARCHAR(64) NOT NULL,
  `width` INT NULL,
  `height` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK__users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `competitions`
ADD COLUMN `banner` INT(11) NULL DEFAULT NULL AFTER `max_teams`,
ADD CONSTRAINT `fk_images_banner` FOREIGN KEY (`banner`) REFERENCES `images` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;