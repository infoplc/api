CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE `comments`
ADD CONSTRAINT `fk_comments_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizers_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `state` enum('open','closed') NOT NULL,
  `related_type` enum('competition','team','user') NOT NULL,
  `related_id` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE `tickets`
ADD CONSTRAINT `fk_tickets_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS `ticket_has_comments` (
  `tickets_id` int(11) NOT NULL,
  `comments_id` int(11) NOT NULL,
  UNIQUE KEY `tickets_id` (`tickets_id`,`comments_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `ticket_has_comments`
ADD CONSTRAINT `fk_ticket_has_comments_tickets_id` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
ADD CONSTRAINT `fk_ticket_has_comments_comments_id` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS `ticket_has_recipients` (
  `tickets_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `last_message_seen_id` int(11) NOT NULL,
  `unreaded_messages` int(11) NOT NULL,
  UNIQUE KEY `tickets_id` (`tickets_id`,`users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `ticket_has_recipients`
ADD CONSTRAINT `fk_ticket_has_recipients_tickets_id` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
ADD CONSTRAINT `fk_ticket_has_recipients_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE IF NOT EXISTS `ticket_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tickets_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE `ticket_messages`
ADD CONSTRAINT `fk_ticket_messages_tickets_id` FOREIGN KEY (`tickets_id`) REFERENCES `tickets` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
ADD CONSTRAINT `fk_ticket_messages_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE `private_messages`
CHANGE `creation` `created_at` DATETIME NULL DEFAULT NULL,
ADD `modified_at` DATETIME NULL AFTER `created_at`,
ADD `revision` INT NULL AFTER `modified_at`,
ADD `deleted_at` DATETIME NULL AFTER `revision`;
ALTER TABLE `private_messages_has_recipients` ADD `new_replies` INT NOT NULL AFTER `open_time`;

CREATE TABLE IF NOT EXISTS `ticket_message_revisions` (
  `ticket_messages_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `ticket_messages_id` (`ticket_messages_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `ticket_message_revisions`
ADD CONSTRAINT `fk_ticket_message_revisions_ticket_messages_id` FOREIGN KEY (`ticket_messages_id`) REFERENCES `ticket_messages` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


CREATE TABLE IF NOT EXISTS `private_message_revisions` (
  `private_messages_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `private_message_revisions`
ADD CONSTRAINT `fk_private_message_revisions_private_messages_id` FOREIGN KEY (`private_messages_id`) REFERENCES `private_messages` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

DROP TABLE private_messages_has_recipients;

CREATE TABLE IF NOT EXISTS `private_message_has_recipients` (
  `private_messages_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `type` ENUM('main','cc','cco') NULL,
  `last_message_seen_id` int(11) NOT NULL,
  `unreaded_messages` int(11) NOT NULL,
  PRIMARY KEY (`private_messages_id`, `users_id`),
  INDEX `fk_private_messages_has_users_users1_idx` (`users_id` ASC),
  INDEX `fk_private_messages_has_users_private_messages1_idx` (`private_messages_id` ASC),
  CONSTRAINT `fk_private_messages_has_users_private_messages1`
    FOREIGN KEY (`private_messages_id`)
    REFERENCES `private_messages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_private_messages_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
