CREATE TABLE `match_comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `matches_id` INT NOT NULL,
  `teams_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `creation` DATETIME NOT NULL,
  `comment` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_match_comments_has_matches` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_match_comments_has_teams` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_match_comments_has_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;