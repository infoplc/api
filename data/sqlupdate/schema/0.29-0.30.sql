ALTER TABLE `match_comments`
ALTER `teams_id` DROP DEFAULT;

ALTER TABLE `match_comments`
CHANGE COLUMN `teams_id` `teams_id` INT(11) NULL AFTER `matches_id`,
ADD COLUMN `as_admin` TINYINT(1) NULL AFTER `users_id`;

ALTER TABLE `match_comments`
ADD COLUMN `match_state` ENUM('opened','dated','playing','played','scored','closed') NULL DEFAULT NULL AFTER `as_admin`;

ALTER TABLE `match_comments`
ADD COLUMN `only_for_admins` TINYINT(1) NULL AFTER `as_admin`;