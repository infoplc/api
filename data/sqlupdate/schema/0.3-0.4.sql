ALTER TABLE `competitions_has_teams`
ADD COLUMN `join_date` DATETIME NULL AFTER `accepted`;

ALTER TABLE `competitions_has_teams`
ADD COLUMN `accepted_by_users_id` INT NOT NULL AFTER `join_date`,
ADD INDEX `accepted_by_users_id` (`accepted_by_users_id`),
ADD CONSTRAINT `fk_competitions_has_teams1_accepted_by_users_id2` FOREIGN KEY (`accepted_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `competitions_has_teams`
ADD COLUMN `apply_date` DATETIME NOT NULL AFTER `accepted_by_users_id`;

CREATE TABLE `acls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `privilege_name` VARCHAR(32) NOT NULL,
  `resource_name` VARCHAR(32) NOT NULL,
  `accesses_names` VARCHAR(64) NOT NULL,
  `allowed` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`, `privilege_name`, `resource_name`, `accesses_names`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


/* Inicio cambio columna ID de competition_privileges */
ALTER TABLE `users_has_competitions`
DROP FOREIGN KEY `fk_users_has_competitions_competition_privileges1`;
ALTER TABLE `competition_privileges`
CHANGE COLUMN `idcompetition_privileges` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE `users_has_competitions`
ADD CONSTRAINT `fk_users_has_competitions_competition_privileges` FOREIGN KEY (`competition_privileges_id`) REFERENCES `competition_privileges` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
/* Fin cambio columna ID de competition_privileges */

/* Inicio eliminación estructura de control de APIs*/
SET foreign_key_checks = 0;
DROP TABLE `api_calls`;
DROP TABLE `api_has_privileges`;
DROP TABLE `api_history`;
DROP TABLE `api_ips`;
DROP TABLE `api_key`;
DROP TABLE `api_privileges`;
DROP TABLE `api_sessions`;
SET foreign_key_checks = 1;
/* Fin eliminación estructura de control de APIs*/