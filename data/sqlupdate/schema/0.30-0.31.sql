ALTER TABLE `competitions`
ADD COLUMN `background` INT(11) NULL DEFAULT NULL AFTER `banner`,
ADD CONSTRAINT `fk_images_background` FOREIGN KEY (`background`) REFERENCES `images` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;