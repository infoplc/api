ALTER TABLE `users_has_competitions`
ADD COLUMN `accepted_by_users_id` INT NULL AFTER `joined`,
ADD COLUMN `left` DATETIME NULL AFTER `accepted_by_users_id`,
ADD CONSTRAINT `fk_users_has_competitions_accepted_by_users_id` FOREIGN KEY (`accepted_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION;

CREATE TABLE `competition_staff_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `competitions_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `by_users_id` INT(11) NULL DEFAULT NULL,
  `action` ENUM('join','leave') NULL DEFAULT NULL,
  `date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_competition_staff_history_competitions` (`competitions_id`),
  INDEX `FK_competition_staff_history_users_1` (`users_id`),
  INDEX `FK_competition_staff_history_users_2` (`by_users_id`),
  CONSTRAINT `FK_competition_staff_history_competitions` FOREIGN KEY (`competitions_id`) REFERENCES `competitions` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_competition_staff_history_users_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_competition_staff_history_users_2` FOREIGN KEY (`by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `user_notifications`
ALTER `code` DROP DEFAULT;
ALTER TABLE `user_notifications`
CHANGE COLUMN `code` `code` VARCHAR(64) NOT NULL AFTER `author_id`;
