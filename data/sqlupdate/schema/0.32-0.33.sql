ALTER TABLE `teams_has_matches`
CHANGE COLUMN `state` `state` ENUM('win','draw','loss','default_win','default_loss') NULL DEFAULT NULL AFTER `opponent_score`;