SET FOREIGN_KEY_CHECKS=0;

DROP TABLE `private_messages`, `private_message_has_recipients`, `private_message_revisions`;

CREATE TABLE IF NOT EXISTS `private_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) NOT NULL DEFAULT '0',
  `users_id` int(11) NOT NULL,
  `subject` varchar(256) DEFAULT NULL,
  `body` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `revision` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`thread_id`),
  KEY `fk_private_messages_users1_idx` (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `private_message_has_recipients` (
  `private_messages_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `type` enum('main','cc','cco') DEFAULT NULL,
  `last_message_seen_id` int(11) DEFAULT NULL,
  `unread_messages` int(11) NOT NULL,
  `last_private_messages_id` int(11) DEFAULT NULL,
  `replies` int(11) NOT NULL DEFAULT '0',
  `left_at` datetime DEFAULT NULL,
  PRIMARY KEY (`private_messages_id`,`users_id`),
  KEY `fk_private_messages_has_users_users1_idx` (`users_id`),
  KEY `fk_private_messages_has_users_private_messages1_idx` (`private_messages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `private_message_revisions` (
  `private_messages_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text NOT NULL,
  `created_at` datetime NOT NULL,
  KEY `fk_private_message_revisions_private_messages_id` (`private_messages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `private_messages`
  ADD CONSTRAINT `fk_private_messages_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `private_message_has_recipients`
  ADD CONSTRAINT `fk_private_messages_has_users_private_messages1` FOREIGN KEY (`private_messages_id`) REFERENCES `private_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_private_messages_has_users_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `private_message_revisions`
  ADD CONSTRAINT `fk_private_message_revisions_private_messages_id` FOREIGN KEY (`private_messages_id`) REFERENCES `private_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

SET FOREIGN_KEY_CHECKS=1;