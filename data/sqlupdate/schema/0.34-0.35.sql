CREATE TABLE `matches_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NULL,
  `by_users_id` INT NULL,
  `matches_id` INT NOT NULL,
  `teams_id` INT NULL,
  `action` ENUM('add_score','remove_score','accept_score','change_state') NOT NULL,
  `date` DATETIME NULL,
  `own_score` INT NULL,
  `opponent_score` INT NULL,
  `state` ENUM('opened','dated','playing','played','scored','closed') NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_matches_history_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_matches_history_by_users_id` FOREIGN KEY (`by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_matches_history_matches_id` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_matches_history_teams_id` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;
