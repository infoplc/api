ALTER TABLE `teams` ADD `logo` INT NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `images` ADD `created_at` DATETIME NOT NULL AFTER `height`, ADD `client_id` varchar(128) NOT NULL AFTER `created_at`, ADD `ip_address` CHAR(16) NOT NULL AFTER `client_id`;
