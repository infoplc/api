ALTER TABLE `divisions_has_matches`
ADD COLUMN `division_round_id` INT NOT NULL AFTER `matches_id`;
ALTER TABLE `divisions_has_matches`
ADD INDEX `division_round_id` (`division_round_id`);

ALTER TABLE `tournaments`
ADD COLUMN `num_teams_advance_in_each_group` INT(11) NULL DEFAULT NULL AFTER `has_group_phase`;
ALTER TABLE `tournaments`
ADD COLUMN `num_teams_compete_in_each_group` INT(11) NULL DEFAULT NULL AFTER `num_teams_advance_in_each_group`;

ALTER TABLE `group_phases_info`
DROP COLUMN `tournament_branch_id`;
ALTER TABLE `group_phases_info`
ADD COLUMN `next_tree_match_id` INT(11) NOT NULL AFTER `divisions_id`,
ADD CONSTRAINT `fk_group_phases_info_next_tree_match_id` FOREIGN KEY (`next_tree_match_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE `match_games`
DROP FOREIGN KEY `fk_match_games_matches1`;
ALTER TABLE `match_games`
ADD CONSTRAINT `fk_match_games_matches1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `group_phases_info`
DROP FOREIGN KEY `fk_group_phases_info_divisions1`,
DROP FOREIGN KEY `fk_group_phases_info_next_tree_match_id`;
ALTER TABLE `group_phases_info`
ADD CONSTRAINT `fk_group_phases_info_divisions1` FOREIGN KEY (`divisions_id`) REFERENCES `divisions` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
ADD CONSTRAINT `fk_group_phases_info_next_tree_match_id` FOREIGN KEY (`next_tree_match_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `tournaments_has_matches`
DROP FOREIGN KEY `fk_tournaments_has_matches_matches1`;
ALTER TABLE `tournaments_has_matches`
ADD CONSTRAINT `fk_tournaments_has_matches_matches1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `tournaments_has_matches`
DROP FOREIGN KEY `fk_tournaments_has_matches_matches_id_next_loser`,
DROP FOREIGN KEY `fk_tournaments_has_matches_matches_id_next_winner`;
ALTER TABLE `tournaments_has_matches`
ADD CONSTRAINT `fk_tournaments_has_matches_matches_id_next_loser` FOREIGN KEY (`matches_id_next_loser`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
ADD CONSTRAINT `fk_tournaments_has_matches_matches_id_next_winner` FOREIGN KEY (`matches_id_next_winner`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `teams_has_matches`
DROP FOREIGN KEY `fk_teams_has_matches_matches1`;
ALTER TABLE `teams_has_matches`
ADD CONSTRAINT `fk_teams_has_matches_matches1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `teams_has_divisions`
CHANGE COLUMN `score` `score` INT(11) NOT NULL DEFAULT '0' AFTER `divisions_id`;

ALTER TABLE `teams_has_divisions`
DROP FOREIGN KEY `fk_teams_has_divisions_divisions1`;
ALTER TABLE `teams_has_divisions`
ADD CONSTRAINT `fk_teams_has_divisions_divisions1` FOREIGN KEY (`divisions_id`) REFERENCES `divisions` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `divisions_has_matches`
DROP FOREIGN KEY `fk_divisions_has_matches_divisions1`,
DROP FOREIGN KEY `fk_divisions_has_matches_matches1`;
ALTER TABLE `divisions_has_matches`
ADD CONSTRAINT `fk_divisions_has_matches_divisions1` FOREIGN KEY (`divisions_id`) REFERENCES `divisions` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE,
ADD CONSTRAINT `fk_divisions_has_matches_matches1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE `teams_has_divisions`
ADD COLUMN `last_score_state` ENUM('win','draw','loss') NULL AFTER `score`;

ALTER TABLE `teams_has_divisions`
ADD COLUMN `total_num_matches` INT NULL DEFAULT '0' AFTER `last_score_state`,
ADD COLUMN `num_matches_win` INT NULL DEFAULT '0' AFTER `total_num_matches`,
ADD COLUMN `num_matches_draw` INT NULL DEFAULT '0' AFTER `num_matches_win`,
ADD COLUMN `num_matches_loss` INT NULL DEFAULT '0' AFTER `num_matches_draw`;