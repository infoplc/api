ALTER TABLE `competitions`
  ADD COLUMN `lteq_num_users_per_team` INT(3) NULL AFTER `background`,
  ADD COLUMN `gteq_num_users_per_team` INT(3) NULL AFTER `lteq_num_users_per_team`;
ALTER TABLE `teams`
  ADD COLUMN `is_individual_team` TINYINT(1) NOT NULL DEFAULT '0' AFTER `logo`;
ALTER TABLE `teams`
  ADD UNIQUE INDEX `name` (`name`);