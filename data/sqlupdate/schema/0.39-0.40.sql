CREATE TABLE `organizations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `seo` VARCHAR(45) NOT NULL,
  `description` TEXT NULL,
  `creation` DATETIME NOT NULL,
  `is_premium` TINYINT(1) NULL DEFAULT '0',
  `logo` INT NULL DEFAULT NULL,
  `background` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name`),
  UNIQUE INDEX `seo` (`seo`),
  INDEX `fk_organizations_logo` (`logo`),
  INDEX `fk_organizations_background` (`background`),
  INDEX `seo_key` (`seo`),
  CONSTRAINT `fk_organizations_logo` FOREIGN KEY (`logo`) REFERENCES `images` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_organizations_background` FOREIGN KEY (`background`) REFERENCES `images` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `organizations_has_competitions` (
  `organizations_id` INT NOT NULL,
  `competitions_id` INT NOT NULL,
  `assigned_by_users_id` INT NOT NULL,
  `unassigned_by_users_id` INT NULL,
  `creation` DATETIME NOT NULL,
  `active` TINYINT(1) NULL DEFAULT '1',
  PRIMARY KEY (`organizations_id`, `competitions_id`),
  INDEX `fk_organizations_has_competitions_organizations1_idx` (`organizations_id` ASC),
  INDEX `fk_organizations_has_competitions_competitions1_idx` (`competitions_id` ASC),
  INDEX `fk_organizations_has_competitions_assigned_by_users_id1_idx` (`assigned_by_users_id` ASC),
  INDEX `fk_organizations_has_competitions_unassigned_by_users_id1_idx` (`unassigned_by_users_id` ASC),
  CONSTRAINT `fk_organizations_has_competitions_organizations`
  FOREIGN KEY (`organizations_id`)
  REFERENCES `organizations` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_organizations_has_competitions_competitions`
  FOREIGN KEY (`competitions_id`)
  REFERENCES `competitions` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_organizations_has_competitions_assigned_by_users_id`
  FOREIGN KEY (`assigned_by_users_id`)
    REFERENCES `users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT `fk_organizations_has_competitions_unassigned_by_users_id`
  FOREIGN KEY (`unassigned_by_users_id`)
  REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `organization_privileges` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `users_has_organizations` (
  `users_id` INT NOT NULL,
  `organizations_id` INT NOT NULL,
  `organization_privileges_id` INT NOT NULL,
  `joined` DATETIME NOT NULL,
  `accepted_by_users_id` INT NULL DEFAULT NULL,
  `left` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`users_id`, `organizations_id`),
  INDEX `fk_users_has_organizations_users1_idx` (`users_id` ASC),
  INDEX `fk_users_has_organizations_organizations1_idx` (`organizations_id` ASC),
  INDEX `fk_users_has_organizations_organization_privileges1_idx` (`organization_privileges_id` ASC),
  INDEX `fk_users_has_organizations_accepted_by_users_id` (`accepted_by_users_id`),
  CONSTRAINT `fk_users_has_organizations_organizations`
  FOREIGN KEY (`organizations_id`)
  REFERENCES `organizations` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_organizations_users`
  FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_organizations_organization_privileges`
  FOREIGN KEY (`organization_privileges_id`)
    REFERENCES `organization_privileges` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_organizations_accepted_by_users_id`
  FOREIGN KEY (`accepted_by_users_id`)
    REFERENCES `users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `organization_names_history` (
  `organizations_id` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  `users_id` INT(11) NOT NULL,
  `ip` VARCHAR(32) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`organizations_id`, `date`),
  INDEX `FK_organization_names_history_users` (`users_id`),
  INDEX `FK_organization_names_history_organizations` (`organizations_id`),
  CONSTRAINT `FK_organization_names_history_organizations` FOREIGN KEY (`organizations_id`) REFERENCES `organizations` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_organization_names_history_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `organization_users_history` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `organizations_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `by_users_id` INT(11) NULL DEFAULT NULL,
  `action` ENUM('apply','join','leave','kick','unkick') NULL DEFAULT NULL,
  `date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_organization_users_history_organizations` (`organizations_id`),
  INDEX `FK_organization_users_history_users_1` (`users_id`),
  INDEX `FK_organization_users_history_users_2` (`by_users_id`),
  CONSTRAINT `FK_organization_users_history_organizations` FOREIGN KEY (`organizations_id`) REFERENCES `organizations` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_organization_users_history_users_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `FK_organization_users_history_users_2` FOREIGN KEY (`by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `match_comments` DROP FOREIGN KEY `fk_match_comments_has_matches`;
ALTER TABLE `match_comments` ADD CONSTRAINT `fk_match_comments_has_matches` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE CASCADE;



