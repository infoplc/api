ALTER TABLE `matches_history` ALTER `action` DROP DEFAULT;
ALTER TABLE `matches_history` CHANGE COLUMN `action` `action` ENUM('add_score','remove_score','accept_score','change_state','date_round') NOT NULL COLLATE 'utf8_unicode_ci' AFTER `teams_id`;
ALTER TABLE `matches_history` ADD COLUMN `info` TEXT NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `state`;