CREATE TABLE `crons` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_code` VARCHAR(255) NOT NULL,
  `status` VARCHAR(48) NOT NULL DEFAULT 'pending',
  `messages` TEXT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_at` TIMESTAMP NULL DEFAULT NULL,
  `executed_at` TIMESTAMP NULL DEFAULT NULL,
  `finished_at` TIMESTAMP NULL DEFAULT NULL,
  `parameters` TEXT NULL,
  `host` VARCHAR(255) NULL DEFAULT NULL,
  `pid` VARCHAR(255) NULL DEFAULT NULL,
  `progress_message` TEXT NULL,
  `last_seen` TIMESTAMP NULL DEFAULT NULL,
  `kill_request` TIMESTAMP NULL DEFAULT NULL,
  `scheduled_by` INT(10) UNSIGNED NULL DEFAULT NULL,
  `scheduled_reason` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `IDX_CRON_SCHEDULE_JOB_CODE` (`job_code`),
  INDEX `IDX_CRON_SCHEDULE_SCHEDULED_AT_STATUS` (`scheduled_at`, `status`)
)  ENGINE=InnoDB;

CREATE TABLE `cron_jobs` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `job_code` VARCHAR(255) NOT NULL,
  `can_be_scheduled` TINYINT(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `IDX_CRON_SCHEDULE_JOB_CODE` (`job_code`)
)  ENGINE=InnoDB;

CREATE TABLE `elasticsearch_queue` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(255) NOT NULL,
  `model_id` INT NULL,
  `creation` DATETIME NOT NULL,
  `status` ENUM('pending','processing','processed','error') NOT NULL DEFAULT 'pending',
  `processed_date` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `status` (`status`)
) ENGINE=InnoDB;

