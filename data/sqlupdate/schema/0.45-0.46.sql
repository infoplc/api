-- -----------------------------------------------------
-- Table `user_has_stream_channels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `users_has_stream_channels` (
  `users_id` INT NOT NULL,
  `stream_channels_id` INT NOT NULL,
  PRIMARY KEY (`users_id`, `stream_channels_id`),
  INDEX `fk_users_has_stream_channels_stream_channels1_idx` (`stream_channels_id` ASC),
  INDEX `fk_users_has_stream_channels_users1_idx` (`users_id` ASC),
  CONSTRAINT `fk_users_has_stream_channels_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_stream_channels_stream_channels1`
    FOREIGN KEY (`stream_channels_id`)
    REFERENCES `stream_channels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `team_has_stream_channels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teams_has_stream_channels` (
  `teams_id` INT NOT NULL,
  `stream_channels_id` INT NOT NULL,
  PRIMARY KEY (`teams_id`, `stream_channels_id`),
  INDEX `fk_teams_has_stream_channels_stream_channels1_idx` (`stream_channels_id` ASC),
  INDEX `fk_teams_has_stream_channels_teams1_idx` (`teams_id` ASC),
  CONSTRAINT `fk_teams_has_stream_channels_teams1`
    FOREIGN KEY (`teams_id`)
    REFERENCES `teams` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teams_has_stream_channels_stream_channels1`
    FOREIGN KEY (`stream_channels_id`)
    REFERENCES `stream_channels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
