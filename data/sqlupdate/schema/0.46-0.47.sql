ALTER TABLE `teams_has_users`
ADD COLUMN `invitation_date` DATETIME NULL DEFAULT NULL AFTER `apply_date`,
ADD COLUMN `invited_by_users_id` INT(11) NULL DEFAULT NULL AFTER `invitation_date`,
ADD INDEX `FK_teams_has_users_invited_by_users_id` (`invited_by_users_id`),
ADD CONSTRAINT `FK_teams_has_users_invited_by_users_id1` FOREIGN KEY (`invited_by_users_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `team_users_history`
CHANGE COLUMN `action` `action` ENUM('apply','join','leave','kick','unkick','invite','reject_apply','reject_invitation','remove_apply','remove_invitation') NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `by_users_id`;
