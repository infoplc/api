ALTER TABLE `matches_history`
DROP FOREIGN KEY `fk_matches_history_matches_id`;
ALTER TABLE `matches_history`
ADD CONSTRAINT `fk_matches_history_matches_id` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;