-- -----------------------------------------------------
-- Table `organizations_reputation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `organizations_reputation` (
  `users_id` INT NOT NULL COMMENT '',
  `organizations_id` INT NOT NULL COMMENT '',
  `created` DATETIME NULL COMMENT '',
  PRIMARY KEY (`users_id`, `organizations_id`)  COMMENT '',
  INDEX `fk_organizations_reputation_organizations1_idx` (`organizations_id` ASC)  COMMENT '',
  CONSTRAINT `fk_organizations_reputation_users1`
  FOREIGN KEY (`users_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_organizations_reputation_organizations1`
  FOREIGN KEY (`organizations_id`)
  REFERENCES `organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teams_reputation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teams_reputation` (
  `teams_has_matches_teams_id` INT NOT NULL COMMENT '',
  `teams_has_matches_matches_id` INT NOT NULL COMMENT '',
  `users_id` INT NOT NULL COMMENT '',
  `created` DATETIME NULL COMMENT '',
  PRIMARY KEY (`teams_has_matches_teams_id`, `teams_has_matches_matches_id`, `users_id`)  COMMENT '',
  INDEX `fk_teams_reputation_users1_idx` (`users_id` ASC)  COMMENT '',
  INDEX `teams_has_matches_teams_id` (`teams_has_matches_teams_id`),
  CONSTRAINT `fk_teams_reputation_teams_has_matches1`
  FOREIGN KEY (`teams_has_matches_teams_id` , `teams_has_matches_matches_id`)
  REFERENCES `teams_has_matches` (`teams_id` , `matches_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teams_reputation_users1`
  FOREIGN KEY (`users_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;