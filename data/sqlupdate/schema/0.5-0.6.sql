ALTER TABLE `competitions_has_teams`
	ALTER `accepted_by_users_id` DROP DEFAULT;
ALTER TABLE `competitions_has_teams`
	CHANGE COLUMN `accepted_by_users_id` `accepted_by_users_id` INT(11) NULL AFTER `join_date`;