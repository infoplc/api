ALTER TABLE `teams_has_matches`
	CHANGE COLUMN `play_time_accepted` `play_date_proposal` DATETIME NULL DEFAULT NULL AFTER `matches_id`;

ALTER TABLE `competitions_has_teams`
ADD COLUMN `weight` INT(5) NULL DEFAULT '0' AFTER `apply_date`;

CREATE TABLE `match_games` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `matches_id` INT NOT NULL,
  `teams_id` INT NOT NULL,
  `score` INT NULL DEFAULT NULL,
  `duration` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `matches_id` (`matches_id`),
  INDEX `teams_id` (`teams_id`),
  CONSTRAINT `fk_match_games_matches1` FOREIGN KEY (`matches_id`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT `fk_match_games_teams1` FOREIGN KEY (`teams_id`) REFERENCES `teams` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE `competition_default_match_times` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `competitions_id` INT NOT NULL,
  `is_range` TINYINT(1) NULL DEFAULT NULL,
  `start_time` TIME NOT NULL,
  `end_time` TIME NULL,
  PRIMARY KEY (`id`),
  INDEX `competitions_id` (`competitions_id`),
  CONSTRAINT `fk_competitions_default_match_times_competitions1` FOREIGN KEY (`competitions_id`) REFERENCES `competitions` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB;

ALTER TABLE `tournaments_has_matches`
ADD COLUMN `matches_id_right` INT NULL AFTER `tournament_elimination_id`,
ADD CONSTRAINT `fk_tournaments_has_matches_matches_id_right` FOREIGN KEY (`matches_id_right`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
