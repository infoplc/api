ALTER TABLE `tournaments`
	ALTER `num_rounds` DROP DEFAULT;
ALTER TABLE `tournaments`
	CHANGE COLUMN `has_group_phase` `has_group_phase` TINYINT(1) NULL DEFAULT NULL AFTER `competitions_id`,
	CHANGE COLUMN `is_double_elimination` `is_double_elimination` TINYINT(1) NULL DEFAULT NULL AFTER `has_group_phase`,
	CHANGE COLUMN `num_rounds` `num_rounds` INT(11) NULL AFTER `is_double_elimination`,
	ADD COLUMN `has_third_place` TINYINT(1) NULL DEFAULT NULL AFTER `num_group_phases`;
ALTER TABLE `tournaments_has_matches`
	ADD COLUMN `is_third_place_match` TINYINT(1) NULL DEFAULT '0' AFTER `matches_id_right`;