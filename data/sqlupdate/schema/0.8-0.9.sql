ALTER TABLE `tournaments_has_matches`
  DROP FOREIGN KEY `fk_tournaments_has_matches_matches_id_right`;
ALTER TABLE `tournaments_has_matches`
  CHANGE COLUMN `matches_id_right` `matches_id_next_winner` INT(11) NULL DEFAULT NULL AFTER `tournament_elimination_id`,
  ADD COLUMN `matches_id_next_loser` INT(11) NULL DEFAULT NULL AFTER `matches_id_next_winner`,
  DROP INDEX `fk_tournaments_has_matches_matches_id_right`,
  ADD INDEX `fk_tournaments_has_matches_matches_id_next_winner` (`matches_id_next_winner`),
  ADD INDEX `fk_tournaments_has_matches_matches_id_next_loser` (`matches_id_next_loser`),
  ADD CONSTRAINT `fk_tournaments_has_matches_matches_id_next_winner` FOREIGN KEY (`matches_id_next_winner`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_tournaments_has_matches_matches_id_next_loser` FOREIGN KEY (`matches_id_next_loser`) REFERENCES `matches` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE `tournaments_has_matches`
DROP COLUMN `tournament_elimination_id`;
