ALTER TABLE `tournaments_has_matches`
  ADD COLUMN `is_loser_match` TINYINT(1) NULL DEFAULT '0' AFTER `is_third_place_match`;
ALTER TABLE `tournaments_has_matches`
  ADD COLUMN `next_winner_branch_id` INT NULL DEFAULT NULL AFTER `is_loser_match`,
  ADD COLUMN `next_winner_round_id` INT NULL DEFAULT NULL AFTER `next_winner_branch_id`,
  ADD COLUMN `next_loser_branch_id` INT NULL DEFAULT NULL AFTER `next_winner_round_id`,
  ADD COLUMN `next_loser_round_id` INT NULL DEFAULT NULL AFTER `next_loser_branch_id`;
