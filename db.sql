-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-09-2017 a las 02:18:38
-- Versión del servidor: 5.5.55-0+deb8u1
-- Versión de PHP: 5.6.30-0+deb8u1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pfm_api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acls`
--

CREATE TABLE IF NOT EXISTS `acls` (
  `id` int(11) NOT NULL,
  `privilege_name` varchar(32) NOT NULL,
  `resource_name` varchar(32) NOT NULL,
  `accesses_names` varchar(64) NOT NULL,
  `allowed` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acls`
--

INSERT INTO `acls` (`id`, `privilege_name`, `resource_name`, `accesses_names`, `allowed`) VALUES
(1, 'Administrator', 'Device', 'view, insert, delete, edit', 1),
(2, 'Administrator', 'Tag', 'view, insert, delete, edit', 1),
(3, 'Administrator', 'User', 'view, insert, delete, edit', 1),
(1, 'Editor', 'Device', 'view', 1),
(2, 'Editor', 'Tag', 'view, insert, delete, edit', 1),
(3, 'Editor', 'User', 'view', 1),
(1, 'Viewer', 'Device', 'view', 1),
(2, 'Viewer', 'Tag', 'view', 1),
(3, 'Viewer', 'User', 'view', 1),
(4, 'Super Administrator', '*', '*', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL,
  `name` varchar(18) DEFAULT NULL,
  `OPC_path` varchar(128) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices_roles`
--

CREATE TABLE IF NOT EXISTS `devices_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(18) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `devices_roles`
--

INSERT INTO `devices_roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Editor'),
(3, 'Viewer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `device_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `secret` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auto_approve` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `secret`, `name`, `auto_approve`) VALUES
('pfmApp', '***', 'App móvil', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_client_endpoints`
--

CREATE TABLE IF NOT EXISTS `oauth_client_endpoints` (
  `id` int(10) unsigned NOT NULL,
  `client_id` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_client_endpoints`
--

INSERT INTO `oauth_client_endpoints` (`id`, `client_id`, `redirect_uri`) VALUES
(2, 'pfmApp', 'http://localhost/callback');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_scopes` (
  `id` smallint(5) unsigned NOT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_scopes`
--

INSERT INTO `oauth_scopes` (`id`, `scope`, `name`, `description`) VALUES
(1, 'profile', 'Profile', 'Access to basic profile data');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_sessions`
--

CREATE TABLE IF NOT EXISTS `oauth_sessions` (
  `id` int(10) unsigned NOT NULL,
  `client_id` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `owner_type` enum('user','client') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `owner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_access_tokens` (
  `id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `access_token` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=329 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_authcodes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcodes` (
  `id` int(10) unsigned NOT NULL,
  `session_id` int(10) unsigned NOT NULL,
  `auth_code` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `auth_code_expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_authcode_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_authcode_scopes` (
  `oauth_session_authcode_id` int(10) unsigned NOT NULL,
  `scope_id` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_redirects`
--

CREATE TABLE IF NOT EXISTS `oauth_session_redirects` (
  `session_id` int(10) unsigned NOT NULL,
  `redirect_uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_session_redirects`
--

INSERT INTO `oauth_session_redirects` (`session_id`, `redirect_uri`) VALUES
(12, 'http://localhost/callback');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_session_refresh_tokens` (
  `session_access_token_id` int(10) unsigned NOT NULL,
  `refresh_token` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `refresh_token_expires` int(10) unsigned NOT NULL,
  `client_id` char(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_session_token_scopes`
--

CREATE TABLE IF NOT EXISTS `oauth_session_token_scopes` (
  `id` bigint(20) unsigned NOT NULL,
  `session_access_token_id` int(10) unsigned DEFAULT NULL,
  `scope_id` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=329 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `nick` varchar(45) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `suspended` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_email_changes`
--

CREATE TABLE IF NOT EXISTS `user_email_changes` (
  `id` int(11) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(48) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_email_confirmations`
--

CREATE TABLE IF NOT EXISTS `user_email_confirmations` (
  `id` int(10) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `code` char(32) NOT NULL,
  `created_at` datetime NOT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT '0',
  `expired` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_failed_logins`
--

CREATE TABLE IF NOT EXISTS `user_failed_logins` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `ip` char(15) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_device`
--

CREATE TABLE IF NOT EXISTS `user_has_device` (
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `device_role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_has_privileges`
--

CREATE TABLE IF NOT EXISTS `user_has_privileges` (
  `users_id` int(11) NOT NULL,
  `privileges_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_password_changes`
--

CREATE TABLE IF NOT EXISTS `user_password_changes` (
  `id` int(10) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_privileges`
--

CREATE TABLE IF NOT EXISTS `user_privileges` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_remember_tokens`
--

CREATE TABLE IF NOT EXISTS `user_remember_tokens` (
  `id` int(10) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_reset_passwords`
--

CREATE TABLE IF NOT EXISTS `user_reset_passwords` (
  `id` int(10) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `code` varchar(48) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `reset` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_sessions`
--

CREATE TABLE IF NOT EXISTS `user_sessions` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `api_key_id` int(11) NOT NULL,
  `token` varchar(32) DEFAULT NULL,
  `token_expiration` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_success_logins`
--

CREATE TABLE IF NOT EXISTS `user_success_logins` (
  `id` int(10) unsigned NOT NULL,
  `users_id` int(11) NOT NULL,
  `ip` char(15) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `values`
--

CREATE TABLE IF NOT EXISTS `values` (
  `id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acls`
--
ALTER TABLE `acls`
  ADD PRIMARY KEY (`id`,`privilege_name`,`resource_name`,`accesses_names`);

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `devices_roles`
--
ALTER TABLE `devices_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_objetos_dispositivo1_idx` (`device_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_oacl_clse_clid` (`secret`,`id`);

--
-- Indices de la tabla `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i_oaclen_clid` (`client_id`);

--
-- Indices de la tabla `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_oasc_sc` (`scope`);

--
-- Indices de la tabla `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i_uase_clid_owty_owid` (`client_id`,`owner_type`,`owner_id`);

--
-- Indices de la tabla `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_oaseacto_acto_seid` (`access_token`,`session_id`),
  ADD KEY `f_oaseto_seid` (`session_id`);

--
-- Indices de la tabla `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_id` (`session_id`);

--
-- Indices de la tabla `oauth_session_authcode_scopes`
--
ALTER TABLE `oauth_session_authcode_scopes`
  ADD KEY `oauth_session_authcode_id` (`oauth_session_authcode_id`),
  ADD KEY `scope_id` (`scope_id`);

--
-- Indices de la tabla `oauth_session_redirects`
--
ALTER TABLE `oauth_session_redirects`
  ADD PRIMARY KEY (`session_id`);

--
-- Indices de la tabla `oauth_session_refresh_tokens`
--
ALTER TABLE `oauth_session_refresh_tokens`
  ADD PRIMARY KEY (`session_access_token_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indices de la tabla `oauth_session_token_scopes`
--
ALTER TABLE `oauth_session_token_scopes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_setosc_setoid_scid` (`session_access_token_id`,`scope_id`),
  ADD KEY `f_oasetosc_scid` (`scope_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_propiedades_objetos1_idx` (`group_id`),
  ADD KEY `fk_device_id` (`device_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_email_changes`
--
ALTER TABLE `user_email_changes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_email_user` (`users_id`);

--
-- Indices de la tabla `user_email_confirmations`
--
ALTER TABLE `user_email_confirmations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `user_failed_logins`
--
ALTER TABLE `user_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_has_device`
--
ALTER TABLE `user_has_device`
  ADD UNIQUE KEY `user_id` (`user_id`,`device_id`),
  ADD KEY `fk_rlad` (`device_role_id`);

--
-- Indices de la tabla `user_has_privileges`
--
ALTER TABLE `user_has_privileges`
  ADD PRIMARY KEY (`users_id`,`privileges_id`),
  ADD KEY `fk_users_has_privileges_privileges1_idx` (`privileges_id`),
  ADD KEY `fk_users_has_privileges_users1_idx` (`users_id`);

--
-- Indices de la tabla `user_password_changes`
--
ALTER TABLE `user_password_changes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `user_privileges`
--
ALTER TABLE `user_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_remember_tokens`
--
ALTER TABLE `user_remember_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `token` (`token`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `user_reset_passwords`
--
ALTER TABLE `user_reset_passwords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD PRIMARY KEY (`id`,`users_id`,`api_key_id`),
  ADD KEY `fk_user_sessions_users1_idx` (`users_id`),
  ADD KEY `fk_user_sessions_api_key1_idx` (`api_key_id`);

--
-- Indices de la tabla `user_success_logins`
--
ALTER TABLE `user_success_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `values`
--
ALTER TABLE `values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_valores_propiedades_idx` (`tag_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acls`
--
ALTER TABLE `acls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `devices_roles`
--
ALTER TABLE `devices_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=329;
--
-- AUTO_INCREMENT de la tabla `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `oauth_session_token_scopes`
--
ALTER TABLE `oauth_session_token_scopes`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=329;
--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `user_email_changes`
--
ALTER TABLE `user_email_changes`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_email_confirmations`
--
ALTER TABLE `user_email_confirmations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user_failed_logins`
--
ALTER TABLE `user_failed_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `user_password_changes`
--
ALTER TABLE `user_password_changes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `user_privileges`
--
ALTER TABLE `user_privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `user_remember_tokens`
--
ALTER TABLE `user_remember_tokens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_reset_passwords`
--
ALTER TABLE `user_reset_passwords`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `user_sessions`
--
ALTER TABLE `user_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user_success_logins`
--
ALTER TABLE `user_success_logins`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `fk_objetos_dispositivo1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `oauth_client_endpoints`
--
ALTER TABLE `oauth_client_endpoints`
  ADD CONSTRAINT `f_oaclen_clid` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `oauth_sessions`
--
ALTER TABLE `oauth_sessions`
  ADD CONSTRAINT `f_oase_clid` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `oauth_session_access_tokens`
--
ALTER TABLE `oauth_session_access_tokens`
  ADD CONSTRAINT `f_oaseto_seid` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `oauth_session_authcodes`
--
ALTER TABLE `oauth_session_authcodes`
  ADD CONSTRAINT `oauth_session_authcodes_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `oauth_session_authcode_scopes`
--
ALTER TABLE `oauth_session_authcode_scopes`
  ADD CONSTRAINT `oauth_session_authcode_scopes_ibfk_1` FOREIGN KEY (`oauth_session_authcode_id`) REFERENCES `oauth_session_authcodes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `oauth_session_authcode_scopes_ibfk_2` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `oauth_session_redirects`
--
ALTER TABLE `oauth_session_redirects`
  ADD CONSTRAINT `f_oasere_seid` FOREIGN KEY (`session_id`) REFERENCES `oauth_sessions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `oauth_session_refresh_tokens`
--
ALTER TABLE `oauth_session_refresh_tokens`
  ADD CONSTRAINT `f_oasetore_setoid` FOREIGN KEY (`session_access_token_id`) REFERENCES `oauth_session_access_tokens` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `oauth_session_refresh_tokens_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_clients` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `oauth_session_token_scopes`
--
ALTER TABLE `oauth_session_token_scopes`
  ADD CONSTRAINT `f_oasetosc_scid` FOREIGN KEY (`scope_id`) REFERENCES `oauth_scopes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `f_oasetosc_setoid` FOREIGN KEY (`session_access_token_id`) REFERENCES `oauth_session_access_tokens` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `fk_device_id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  ADD CONSTRAINT `fk_propiedades_objetos1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_email_changes`
--
ALTER TABLE `user_email_changes`
  ADD CONSTRAINT `fk_email_user` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_email_confirmations`
--
ALTER TABLE `user_email_confirmations`
  ADD CONSTRAINT `user_email_confirmations_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `user_has_device`
--
ALTER TABLE `user_has_device`
  ADD CONSTRAINT `fk_rlad` FOREIGN KEY (`device_role_id`) REFERENCES `devices_roles` (`id`);

--
-- Filtros para la tabla `user_has_privileges`
--
ALTER TABLE `user_has_privileges`
  ADD CONSTRAINT `fk_users_has_privileges_privileges1` FOREIGN KEY (`privileges_id`) REFERENCES `user_privileges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_has_privileges_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_password_changes`
--
ALTER TABLE `user_password_changes`
  ADD CONSTRAINT `user_password_changes_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `user_remember_tokens`
--
ALTER TABLE `user_remember_tokens`
  ADD CONSTRAINT `user_remember_tokens_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `user_reset_passwords`
--
ALTER TABLE `user_reset_passwords`
  ADD CONSTRAINT `user_reset_passwords_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD CONSTRAINT `fk_user_sessions_api_key1` FOREIGN KEY (`api_key_id`) REFERENCES `api_key` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_sessions_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_success_logins`
--
ALTER TABLE `user_success_logins`
  ADD CONSTRAINT `user_success_logins_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `values`
--
ALTER TABLE `values`
  ADD CONSTRAINT `fk_valores_propiedades` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;