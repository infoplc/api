<?php
namespace pfmAPI\Errors;

class HTTPError {

	public $number;
	public $string;
	public $file;
	public $line;

    /**
     * @param $number
     * @param $string
     * @param $file
     * @param $line
     */
    public function __construct($number, $string, $file, $line){
		$this->number = $number;
		$this->string = $string;
		$this->file = $file;
		$this->line = $line;
	}

	public function send(){
		$di = \Phalcon\DI::getDefault();

		$res = $di->get('response');
		$req = $di->get('request');
		
		//query string, filter, default
		if(!$req->get('suppress_response_codes', null, null)){
			$res->setStatusCode('500', 'Internal Server Error')->sendHeaders();
		} else {
			$res->setStatusCode('200', 'OK')->sendHeaders();
		}
		
		$error = array(
			'errorNumber' => $this->number,
			'errorString' => $this->string,
			'file' => $this->file,
			'line' => $this->line,
		);

		if(!$req->get('type') || $req->get('type') == 'json'){
			$response = new \pfmAPI\Responses\JSONResponse();
			$response->send($error, true);	
			return;
		} else if($req->get('type') == 'csv'){
			$response = new \pfmAPI\Responses\CSVResponse();
			$response->send(array($error));
			return;
		}
		
		error_log(
			'HTTPError: ' . $this->file .
			' at ' . $this->line
		);

		return true;
	}
}