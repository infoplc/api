<?php
namespace pfmAPI\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Select,
    Phalcon\Validation,
    Phalcon\Validation\Validator\Confirmation,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Regex,
    Phalcon\Validation\Validator\Url,

    pfmAPI\Validations\Validators\EmptyUrl,
    pfmAPI\Validations\Validators\EmptyPresenceOf,
    pfmAPI\Validations\Validators\EmptyStringLength,
    pfmAPI\Validations\Validators\EmptyRegex,
    pfmAPI\Validations\Validators\EmptyEmail,
    pfmAPI\Validations\Validators\DateTimeCustom,
    pfmAPI\Validations\Validators\EmptyDateTimeCustom,
    pfmAPI\Exceptions\HTTPException;

class BaseForm extends Form
{
    public $items;
    public $at_least;
    public $request = false;
    private $csrf = false;

    public function getCSRF()
    {
        // esto esta puesto aparte de para cachear, porque si lo llamamos cuando mostramos el html daria error el session
        if ($this->csrf) return $this->csrf;

        if (!$this->session->has('csrf') OR (!$this->csrf = $this->session->get('csrf')))
        {
            $this->csrf = bin2hex(openssl_random_pseudo_bytes(16, $cstrong)); // $this->security->getSessionToken()
            $this->session->set('csrf', $this->csrf);
        }
        return $this->csrf;
    }

    public function addFields()
    {
        if (empty($this->items))
            die("WTF: No se ha especificado en el archivo de validación los campos que contiene el formulario a validar");

        if (!$this->request)
            $this->request = $this->getDI()->get('request');

        // A los campos "at_least" les forzamos la propiedad "opcionabilidad"
        if (!empty($this->at_least))
            foreach ($this->at_least as $items_group)
                foreach ($items_group as $item_field)
                    $this->items[$item_field]['optional'] = true;

        // nos recorremos todos los items del array del formulario, y añadiremos todas las validaciones pertinentes...
        foreach ($this->items as $field => $options)
        {
            // creamos el campo del formulario, luego le meteremos las validaciones
            if (!isset($options['type']))
                $formField = new Text($field);
            else
            {
                switch ($options['type'])
                {
                    case 'text':
                        $formField = new Text($field);
                        break;
                    case 'hidden':
                        $formField = new Hidden($field);
                        break;
                    case 'password':
                        $formField = new Password($field);
                        break;
                    case 'check':
                        $formField = new Check($field, array('value' => isset($options['value']) ? $options['value'] : 'true'));
                        break;
                    case 'textarea':
                        $formField = new Textarea($field);
                        break;
                    case 'submit':
                        $formField = new Submit($field);
                        break;
                    case 'select':
                        $formField = new Select($field, isset($options['options']) ? $options['options'] : array());
                        break;
                }
            }

            // será un campo obligatorio cuando no se ha especificado la opcionalidad, o se ha indicado expresamente que no es opcional
            if (!isset($options['optional']) or !$options['optional'])
            {
                // PresenceOf = Validates that a field’s value isn’t null or empty string.
                if (isset($options['empty']) && $options['empty'])
                    $formField->addValidator(new EmptyPresenceOf(array(
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is required',
                        //'cancelOnFail' => true
                    )));
                else
                    $formField->addValidator(new PresenceOf(array(
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is required',
                        //'cancelOnFail' => true
                    )));
            }

            // si tiene un validador asignado, se lo ponemos...
            if (isset($options['validator']))
            {
                // Si era de tipo email, le añadimos la validación...
                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                    ($options['validator'] == "email"))
                {
                    // Si permitimos que el campo este vacio, usamos un validador especial
                    if (isset($options['empty']) && $options['empty'])
                        $formField->addValidator(new EmptyEmail(array(
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                        )));
                    else
                        $formField->addValidator(new Email(array(
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                        )));
                }

                else

                    if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                        ($options['validator'] == "identical"))
                    {
                        if (isset($options['value']))
                            $formField->addValidator(new Identical(array(
                                'accepted'   => $options['value'],
                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                            )));
                        elseif (isset($options['with']))
                            $formField->addValidator(new Confirmation(array(
                                'with'   => $options['with'],
                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                            )));
                    }

                    else

                        // Si era de tipo numérico, le añadimos la validación...
                        // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                        if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                            ($options['validator'] == "integer"))
                        {
                            // Si permitimos que el campo este vacio, usamos un validador especial
                            if (isset($options['empty']) && $options['empty'])
                                $formField->addValidator(new EmptyRegex(array(
                                    'pattern' => '/^(\d+)*$/',
                                    'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer'
                                )));
                            else
                                $formField->addValidator(new Regex(array(
                                    'pattern' => '/^(\d+)*$/',
                                    'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer'
                                )));
                        }

                        else

                            // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                ($options['validator'] == "multiple_integer"))
                            {
                                // Si permitimos que el campo este vacio, usamos un validador especial
                                if (isset($options['empty']) && $options['empty'])
                                    $formField->addValidator(new EmptyRegex(array(
                                        'pattern' => '/^(\d+(,\d+)*)?$/',
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer (optional: multiple integers with commas)'
                                    )));
                                else
                                    $formField->addValidator(new Regex(array(
                                        'pattern' => '/^(\d+(,\d+)*)?$/',
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer'
                                    )));
                            }

                            else

                            // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                ($options['validator'] == "alphanumeric_extended"))
                            {
                                // Si permitimos que el campo este vacio, usamos un validador especial
                                if (isset($options['empty']) && $options['empty'])
                                    $formField->addValidator(new EmptyRegex(array(
                                        'pattern' => '/^[a-zA-Z0-9_]*$/',
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' can only contain alphanumeric and "_"'
                                    )));
                                else
                                    $formField->addValidator(new Regex(array(
                                        'pattern' => '/^[a-zA-Z0-9_]*$/',
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' can only contain alphanumeric and "_"'
                                    )));
                            }

                            else
                                // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                    ($options['validator'] == "datetime"))
                                {
                                    // Si permitimos que el campo este vacio, usamos un validador especial
                                    if (isset($options['empty']) && $options['empty'])
                                        $formField->addValidator(new EmptyDateTimeCustom(array(
                                            'pattern' => isset($options['pattern']) ? $options['pattern'] : 'y-m-d H:i:s',
                                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid date'
                                        )));
                                    else
                                        $formField->addValidator(new DateTimeCustom(array(
                                            'pattern' => isset($options['pattern']) ? $options['pattern'] : 'y-m-d H:i:s',
                                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid date'
                                        )));
                                }

                                else
                                    // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                                    // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                                    if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                        ($options['validator'] == "url"))
                                    {
                                        // Si permitimos que el campo este vacio, usamos un validador especial
                                        if (isset($options['empty']) && $options['empty'])
                                            $formField->addValidator(new EmptyUrl(array(
                                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid url'
                                            )));
                                        else
                                            $formField->addValidator(new Url(array(
                                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid url'
                                            )));
                                    }

                                else

                                    // comprobamos la opcionalidad del parametro, pues a lo mejor si era de tipo valido pero no se cumplio ninguna de las condiciones anteriores precisamente por esto
                                    if (!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field)))
                                        throw new HTTPException(
                                            "Unable to validate the form at this moment.",
                                            500,
                                            array(
                                                'dev' => "Validator type (".$options['validator'].") not found for '" . (isset($options['name'])?$options['name']:$field). "'",
                                                'internalCode' => '???',
                                            ));

            }

            // Si tenia especificado un valor mínimo, se lo añadimos a la validación...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['minLength']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $formField->addValidator(new EmptyStringLength(array(
                        'messageMinimum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too short',
                        'min' => $options['minLength']
                    )));
                else
                    $formField->addValidator(new StringLength(array(
                        'messageMinimum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too short',
                        'min' => $options['minLength'],
                    )));

            }

            // Si tenia especificado unos valores predefinidos (options['values'])...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['values']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $formField->addValidator(new EmptyRegex(array(
                        'pattern' => '/^'.implode("|^", $options['values']).'$/i',
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' value must be: ' . implode(", ", $options['values'])
                    )));
                else
                    $formField->addValidator(new Regex(array(
                        'pattern' => '/^'.implode("|^", $options['values']).'$/i',
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' value must be: ' . implode(", ", $options['values'])
                    )));

            }

            // Si tenia especificado un valor mínimo, se lo añadimos a la validación...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['maxLength']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $formField->addValidator(new EmptyStringLength(array(
                        'messageMaximum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too long',
                        'max' => $options['maxLength']
                    )));
                else
                    $formField->addValidator(new StringLength(array(
                        'messageMaximum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too long',
                        'max' => $options['maxLength'],
                    )));

            }

            // añadimos el campo al formulario
            $this->add($formField);
        }
    }

    public function setDefaultValues($values)
    {
        if (!is_array($values))
            $values = (array) $values;

        $elements = $this->getElements();
        foreach ($elements as $element)
        {
            if (isset($values[$element->getName()]))
                $element->setDefault($values[$element->getName()]);
        }
    }

    public function niceErrors($field = false)
    {
        $messages = $field ? $this->getMessagesFor($field) : $this->getMessages(false);

        if (count($messages)) {
            //Print each element
            //$html = $field ? '<div class="messages">' : '';
            foreach ($messages as $message) {
                $html = (isset($html) ? $html.".\n" : "") . $message;
            }
            //$html .= $field ? '</div>' : '';
        }

        return isset($html) ? $html : false;
    }

    public function niceRender($field, $options = false, $label = true)
    {
        if ($options === false)
            $options = array();

        //$options['placeholder'] = "test";
        if (isset($this->items[$field]['maxLength']) && $this->items[$field]['maxLength'])
            $options['maxlength'] = $this->items[$field]['maxLength'];

        if (!isset($this->items[$field]['optional']) or !$this->items[$field]['optional'])
            $options['required'] = "required";

        //array('maxlength' => 30, 'placeholder' => 'Type your name'))
        $html = $this->render($field, $options);

        $messages = $this->getMessagesFor($field);
        if (count($messages)) {
            foreach ($messages as $message) {
                $err = (isset($err) ? $err : '' ) . $message;
            }
        }

        if (!is_bool($label))
        {
            $this->items[$field]['label'] = $label;
            $label = true;
        }

        if ($label === true)
        {
            $label = isset($this->items[$field]['label']) ? $this->items[$field]['label'] : $field;

            if (isset($this->items[$field]['type']) && ($this->items[$field]['type'] == "check"))
                $html = '<label '.(isset($err)?'class="error"':'').'>' . $html . " " . $label  .  '</label>';
            else
                $html = '<label '.(isset($err)?'class="error"':'').'>'.$label . $html . '</label>';
        }
        else
            $html = '<label '.(isset($err)?'class="error"':'').'>'. $html . '</label>';

        if (isset($err))
            $html .= '<small class="error">'.$err.'</small>';

        return $html;
    }

    public function getItems() {
        return $this->items;
    }
}