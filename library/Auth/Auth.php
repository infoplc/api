<?php
namespace pfmAPI\Library\Auth;

use Phalcon\Mvc\User\Component,
    pfmAPI\Apps\User\Models\User,
    pfmAPI\Apps\User\Models\RememberTokens;

/**
 * Vokuro\Auth\Auth
 * Manages Authentication/Identity Management in Vokuro
 */
class Auth extends Component
{
    private $user;
    private $cookie_domain = false;

    public function __construct()
    {
        $host_arr = explode(".", $_SERVER['HTTP_HOST']);
        $domain = $host_arr[count($host_arr)-2] . '.' . $host_arr[count($host_arr)-1];
        if(strpos($domain,':') !== false)
            $domain = substr($domain,0,strpos($domain,':'));

        $this->cookie_domain = '.'. $domain;
    }
    /**
     * Checks the user credentials
     */
    public function check($credentials)
    {
        // Check if the user exist
        $user = User::findFirstByEmail($credentials['email']);
        if ($user == false) {
            $this->registerUserThrottling(0);
            throw new Exception('Wrong email/password combination');
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            $this->registerUserThrottling($user->id);
            throw new Exception('Wrong email/password combination');
        }

        // Check if the user was flagged
        $this->checkUserFlags($user);

        // Register the successful login
        $this->saveSuccessLogin($user);

        // Check if the remember me was selected
        if (isset($credentials['remember'])) {
            $this->createRememberEnviroment($user);
        }

        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'nick' => $user->nick
        ));
    }

    public function samesAsCurrentPassword($pwd)
    {
        if (!$this->user && !$this->getUser())
            return false;

        return ($this->security->checkHash($pwd, $this->user->password));
    }


    public function changePassword($pwd)
    {
        if (!$this->user && !$this->getUser())
            return false;

        $this->user->setPassword($pwd);
        return $this->user->save();
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     */
    public function saveSuccessLogin($user)
    {

        // comprobamos si en el cliente existe el usuario, y lo añadimos o actualizamos si hiciera falta
        /*if (!$external_user = User::externalFindFirstById($user->id))
            User::addExternalUser($user->id, $user->nick);
        elseif ($external_user['nick'] != $user->nick)
            User::updateExternalUser($user->id, $user->nick);
*/
        $user->addSuccessLogin($this->request->getClientAddress($this->config->proxy->enabled), $this->request->getUserAgent());
        // $user->addExternalSuccessLogin($this->request->getClientAddress($this->config->proxy->enabled), $this->request->getUserAgent());

        /*
        $attempts = User::getFailedAttemptsByIP($this->request->getClientAddress());

        $successLogin = new SuccessLogins();
        $successLogin->usersId = $user->id;
        $successLogin->ipAddress = $this->request->getClientAddress();
        $successLogin->userAgent = $this->request->getUserAgent();
        if (!$successLogin->save()) {
            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
        */
    }

    /**
     * Implements login throttling
     * Reduces the efectiveness of brute force attacks
     */
    public function registerUserThrottling($userId)
    {
        User::addFailedLogin($userId, $this->request->getClientAddress($this->config->proxy->enabled));
        $attempts = User::getFailedAttemptsByIP($this->request->getClientAddress($this->config->proxy->enabled));

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }

    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     */
    public function createRememberEnviroment($user)
    {
        $token = bin2hex(openssl_random_pseudo_bytes(16, $cstrong));
        // $user->addRememberToken($token, $this->request->getUserAgent());

        $userAgent = $this->request->getUserAgent();
        //$token = md5($user->email . $user->password . $userAgent);

        $remember = new RememberTokens();
        $remember->users_id = $user->id;
        $remember->token = $token;
        $remember->user_agent = $userAgent;

        if ($remember->save() != false) {
            //$user->addExternalRememberToken($token, $userAgent);
            $expire = time() + 86400 * 8;

            // el cookies set de phalcon es UNA PUTA MIERDA
            // $this->cookies->set('RMU', $user->id, $expire);
            // $this->cookies->set('RMT', $token, $expire);

            setcookie("RMU", $this->crypt->encrypt($user->id), $expire, '/', $this->cookie_domain);
            setcookie("RMT", $this->crypt->encrypt($token), $expire, '/', $this->cookie_domain);
            /*
            setcookie("RMU", $user->id, $expire, '/', $this->cookie_domain);
            setcookie("RMT", $token, $expire, '/', $this->cookie_domain);
            */
        }
    }

    /**
     * Check if the session has a remember me cookie
     */
    public function hasRememberMe()
    {
        return isset($_COOKIE['RMU']);
        //return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the coookies
     */
    public function loginWithRememberMe()
    {
        //$userId = $this->cookies->get('RMU')->getValue();
        //$cookieToken = $this->cookies->get('RMT')->getValue();
        $userId = $this->crypt->decrypt($_COOKIE['RMU']);
        $cookieToken = $this->crypt->decrypt($_COOKIE['RMT']);

        $user = User::findFirstById($userId);
        if ($user) {
            /*
            $userAgent = $this->request->getUserAgent();
            $token = md5($user->email . $user->password . $userAgent);
            */

            //if ($cookieToken) {
                $remember = RememberTokens::getRT($user->id, $cookieToken, $this->request->getUserAgent());

                if ($remember) {

                    // Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < strtotime($remember['created_at'])) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity
                        $this->session->set('auth-identity', array(
                            'id' => $user->id,
                            'nick' => $user->nick
                        ));

                        // Register the successful login
                        $this->saveSuccessLogin($user);

                        //return $this->response->redirect('users');
                    }
                }
            //}
        }

        setcookie('RMU', null, -1, '/', $this->cookie_domain);
        setcookie('RMT', null, -1, '/', $this->cookie_domain);
        exit(header("Location: /oauth2/signin"));

        //$this->cookies->get('RMU')->delete();
        //$this->cookies->get('RMT')->delete();

        //return $this->response->redirect('session/login');
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     */
    public function checkUserFlags($user)
    {
        if ($user->active != '1') {
            throw new Exception('The user is inactive');
        }

        if ($user->suspended != '0') {
            throw new Exception('The user is suspended');
        }
    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getNick()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['nick'];
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getId()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['id'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove()
    {
    /*
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $this->cookies->get('RMT')->delete();
        }
    */

        if (isset($_COOKIE['RMU']))
            setcookie('RMU', null, -1, '/', $this->cookie_domain);

        if (isset($_COOKIE['RMT']))
            setcookie('RMT', null, -1, '/', $this->cookie_domain);

        $this->session->remove('auth-identity');
    }

    /**
     * Auths the user by his/her id
     */
    public function authUserById($id)
    {
        $user = User::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', array(
            'id' => $user->id,
            'nick' => $user->nick,
        ));
    }

    /**
     * Get the entity related to user in the active identity
     */
    public function getUser()
    {
        if ($this->user)
            return $this->user;

        $identity = $this->session->get('auth-identity');
        if (isset($identity['id']))
        {
            $this->user = User::findFirstById($identity['id']);
            if ($this->user == false) {
                throw new Exception('The user does not exist');
            }
            return $this->user;
        }

        return false;
    }
}
