<?php
namespace pfmAPI\Library\Mail;

use Phalcon\Mvc\User\Component,
    Swift_Message as Message,
    Swift_SmtpTransport as Smtp,
    Phalcon\Mvc\View;

/**
 * Vokuro\Mail\Mail
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{

    protected $transport;

    protected $amazonSes;

    protected $directSmtp = false;

    /**
     * Send a raw e-mail via AmazonSES
     *
     * @param string $raw
     */
    private function amazonSESSend($raw)
    {
        if ($this->amazonSes == null) {
            $this->amazonSes = new \AmazonSES(array(
                "key" => $this->config->amazon->AWSAccessKeyId,
                "secret" => $this->config->amazon->AWSSecretKey
            ));
            //$this->amazonSes->disable_ssl_verification();
            $this->amazonSes->set_region('email.eu-west-1.amazonaws.com');
        }

        $response = $this->amazonSes->send_raw_email(array(
            'Data' => base64_encode($raw)
        ), array(
            'curlopts' => array(
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0
            )
        ));

        if (!$response->isOK()) {
            throw new Exception('Error sending email from AWS SES: ' . $response->body->asXML());
        }

        return true;
    }

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     */
    public function getTemplate($name, $params)
    {
        $parameters = array_merge(array(
            'publicUrl' => $this->parameters->core->publicURL
        ), $params);


        $view = new \Phalcon\Mvc\View\Simple();
        $view->setViewsDir( BASE_DIR .  $this->parameters->core->viewsDir);

        return $view->render('emailTemplates/' . $name, $parameters);

        return $view->getContent();
    }

    /**
     * Sends e-mails via AmazonSES based on predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     */
    public function send($to, $subject, $name, $params)
    {
        // Settings
        $mailSettings = $this->parameters->mail;

        $template = $this->getTemplate($name, $params);

        // Create the message
        $message = Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom(array(
                $mailSettings->from => $mailSettings->name
            ))
            ->setBody($template, 'text/html');

        $msgId = $message->getHeaders()->get('Message-ID');
        $msgId->setId(time() . '.' . uniqid('thing') . '@staddium.com');

        if ($this->directSmtp) {

            if (!$this->transport) {
                $this->transport = Smtp::newInstance(
                    $this->config->smtp->server,
                    $this->config->smtp->port,
                    $this->config->smtp->security
                )
                ->setUsername($this->config->smtp->username)
                ->setPassword($this->config->smtp->password);
            }

            // Create the Mailer using your created Transport
            $mailer = \Swift_Mailer::newInstance($this->transport);

            return $mailer->send($message);
        } else {
            return $this->amazonSESSend($message->toString());
        }
    }
}
