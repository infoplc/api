<?php
namespace pfmAPI\Library\Utils;

class Log
{
    private $log = array();

    private $dir = false;
    private $silent = false;

    private $selected = false;

    public function __construct($dir, $silent=false)
    {
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        $this->dir = $dir;
        $this->silent = $silent;

        if (!$this->silent)
            $this->header();
    }

    public function write($txt)
    {
        $file = $this->dir . 'update_' . date("d_m_Y") . '.log';
        if (!file_exists($file)) {
            $resource = fopen($file, 'w');
            fclose($resource);
        }
        file_put_contents($file, "\n" . date("d/m/Y H:i:s") . ": " . $txt, FILE_APPEND | LOCK_EX);
    }

    public function lock($action, $seconds) {
        $this->add("Creamos el archivo '".$this->dir . $action . '_' . date("d_m_Y_H_i", $seconds*floor(time()/$seconds)) . '.lock'."'");
        $resource = fopen($this->dir . $action . '_' . date("d_m_Y_H_i", $seconds*floor(time()/$seconds)) . '.lock','w');
        fclose($resource);
    }

    public function unlock($action, $seconds) {
        if ($this->is_locked($action, $seconds))
            unlink($this->dir . $action . '_' . date("d_m_Y_H_i", $seconds*floor(time()/$seconds)) . '.lock');
    }

    public function is_locked($action, $seconds) {
        return (file_exists($this->dir . $action . '_' . date("d_m_Y_H_i", $seconds*floor(time()/$seconds)) . '.lock'));
    }

    public function end($txt) {
        $this->add($txt);
        if (!$this->silent) {
            echo("<script> updateFinished(); </script></body></html>" . "\n");
            @ob_end_flush();
            @ob_flush();
            @flush();
            @ob_start();
        }

    }
    public function add($txt, $status=null) {
        $this->log[] = $txt;
        $this->write($txt);
        if (!$this->silent) {
            if ($status === null) $status = "3";
            else if ($status === false) $status = "1";
            else $status = "2";
            echo("<script> addLog('" . str_replace("'", "\'", $txt) . "', " . $status . "); </script>" . "\n");
            @ob_end_flush();
            @ob_flush();
            @flush();
            @ob_start();
        }
    }

    public function status($txt, $end=false) {
        $this->log[] = $txt;
        $this->write($txt);
        if (!$this->silent) {
            echo("<script> addStatus('" . str_replace("'", "\'", $txt) . "'); </script>" . "\n");
            echo("<script> addLog('" . str_replace("'", "\'", $txt) . "', 3); </script>" . "\n");
            @ob_end_flush();
            @ob_flush();
            @flush();
            @ob_start();

            if ($end)
                exit("<script> updateFinished(); </script></body></html>");
        }
    }

    public function header() {

        if (!$this->silent) {
            $html_head = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="es" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <title>Update</title>
    <script type="text/javascript" src="/js/log/jquery.js"></script>
    <script type="text/javascript" src="/js/log/main.js"></script>
    <style type="text/css" media="all">
        @import "/css/log/main.css";
        @import "/css/font-awesome.min.css";
    </style>
</head>
<body>

<div id="content">

    <div id="header">
        <div id="logo"><h1><span style="color:#444;">Actualizador</span></h1></div>
        <div class="c"></div>
    </div>

    <div id="update_box">
        <div id="status"></div>
        <div id="log"></div>
    </div>

    <p style="padding-top: 15px; text-align: right;"><a href=""><i class="fa fa-refresh"></i></a></p>

    ';

            echo $html_head;
            @ob_end_flush();
            @ob_flush();
            @flush();
            @ob_start();
        }
    }
}