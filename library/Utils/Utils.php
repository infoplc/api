<?php
namespace pfmAPI\Library\Utils;

class Utils
{
    static public function url_exists($url)
    {
        $handle = @fopen($url,'r');
        return ($handle !== false);
    }


    static public function getFile($filename, $use_include_path = false, $referer = false) {
        $timeout = 5;
        if ($referer)
        {
            $hdrs = array('http' => array(
                'method' 	=> 	"GET",
                'header'	=> 	"Accept-language: en\r\n" .
                    "Referer: ".$referer."\r\n",
                'timeout' => $timeout
            )
            );
            $context = stream_context_create($hdrs);
            $file = @fopen($filename, "rb", $use_include_path, $context);
        }
        else
        {
            //$file = @fopen($filename, "rb", $use_include_path);

            $hdrs = array('http' => array(
                'method' 	=> 	"GET",
                'header'	=> 	"Accept-language: en\r\n",
                'timeout' => $timeout
            )
            );

            if (str_replace(" ", "", $filename) != $filename) $filename = str_replace(' ', '%20', $filename);
            $context = stream_context_create($hdrs);
            $file = fopen($filename, "rb", $use_include_path, $context);

            //$data = file_get_contents($filename, false, $context);
        }

        if ($file) {
            //if ($fsize = @filesize($filename)) {
            //	$data = fread($file, $fsize);
            //} else {
            $data = "";
            while (!feof($file)) $data .= fread($file, 1024);
            // }
            fclose($file);

            if ($http_response_header)
            {
                if (in_array("Content-Encoding: gzip", $http_response_header))
                    $data = gzinflate(substr($data,10,-8));
            }
        }
        return isset($data) ? $data : false;
    }



    static public function downloadFile($filename, $url, $dir = '../tmp/', $referer = false, $force_utf8 = false, $force_unzip = false)
    {
        // Primero vamos a asegurarnos de que el archivo existe y es escribible.
        if (!file_exists($dir .$filename) or is_writable($dir . $filename))
        {

            // En nuestro ejemplo estamos abriendo $filename en modo de adición.
            // El puntero al archivo está al final del archivo
            // donde irá $contenido cuando usemos fwrite() sobre él.
            if (!$gestor = @fopen($dir . $filename, 'a'))
                return false;

            // Escribir $contenido a nuestro archivo abierto.
            if ($force_utf8)
                $data = utf8_encode(self::getFile($url, false, $referer));
            else
                $data = self::getFile($url, false, $referer);

            if ($force_unzip)
                $data = gzinflate(substr($data,10,-8));

            //if (!$data) trigger_error("data empty");

            if (fwrite($gestor, $data) === FALSE)
                return false;

            fclose($gestor);
            return true;

        }
        else return false;

    }

    static public function formatBytes($size, $precision = 2)
    {
        $suffixes = array(' bytes', ' kb', ' MB', ' GB', ' TB');
        if ($size==0) return 0 . $suffixes[0];
        $base = log($size, 1024);

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    static public function slugify($str)
    {
        $str = mb_strtolower(trim($str));

        $normalize_chars = array(
            'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
            'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
            'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
            'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
            'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
            'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
            'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', '&'=>'-', '¿'=>'',  '¡'=>'',
            ' '=>'-', '--'=>'-',
        );

        $str = strtr($str, $normalize_chars);
        $str = trim(preg_replace('/[^\w\d_ -]/si', '', $str));

        return $str;
    }
}