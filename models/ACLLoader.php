<?php
namespace pfmAPI\Models;

use Phalcon\Acl,
    Phalcon\Acl\Adapter\Memory,
    Phalcon\Acl\Role,
    Phalcon\Acl\Resource,
    Phalcon\DI,
    Phalcon\Cache\Frontend\Data,
    Phalcon\Cache\Backend\File;

class ACLLoader {
    /** @var  $acl Memory */
    protected $acl;

    /** @var string */
    protected $cacheDir;

    /** @var $aclsTable array */
    protected $aclsTable;

    public function __construct($acl) {
        $this->acl = $acl;
        $this->acl->setDefaultAction(Acl::DENY);

        $this->cacheDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
    }

    public function initAcls() {
        $this->generateCacheFile();
        $this->loadACLs();
        return $this->acl;
    }

    public function getAclsTable() {
        $this->generateCacheFile();
        return $this->aclsTable;
    }

    protected function generateCacheFile() {
        $frontCache = new Data(array(
            "lifetime" => 172800
        ));

        $cache = new File($frontCache, array(
            "cacheDir" => $this->cacheDir
        ));

        $cacheKey = 'acls_table.cache';
        $this->aclsTable = $cache->get($cacheKey);
        if ($this->aclsTable === null) {
            /** @var $connection \Phalcon\Db\AdapterInterface */
            $connection = DI::getDefault()->get('db');
            $aclTableData = 'SELECT * FROM `acls`';
            $result = $connection->query($aclTableData);
            $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
            $resultArray = $result->fetchAll();

            // Store it in the cache
            $cache->save($cacheKey, $resultArray);
            $this->aclsTable = $resultArray;
        }
    }

    protected function loadACLs() {
        foreach($this->aclsTable as $aclRow) {
            if(!empty($aclRow['privilege_name'])) {
                $newRole = new Role($aclRow['privilege_name']);
                $this->acl->addRole($newRole);
                if(!empty($aclRow['resource_name'])) {
                    if($aclRow['resource_name'] == '*') {
                        $newResource = '*';
                    } else {
                        $newResource = new Resource($aclRow['resource_name']);
                    }
                    if(!empty($aclRow['accesses_names'])) {
                        $accessesNames = explode(',',$aclRow['accesses_names']);
                        $accessesNames = array_map(function($elem) { return trim($elem); }, $accessesNames);
                        $hasAllAccessesPrivileges = false;
                        foreach($accessesNames as $accessName) {
                            if($accessName == '*') {
                                $hasAllAccessesPrivileges = true;
                                break;
                            }
                        }
                        if(!$hasAllAccessesPrivileges) {
                            $this->acl->addResource($newResource, $accessesNames);
                        }
                        foreach($accessesNames as $accessName) {
                            if((int)$aclRow['allowed']) {
                                $this->acl->allow($aclRow['privilege_name'],$aclRow['resource_name'],$accessName);
                            } else {
                                $this->acl->deny($aclRow['privilege_name'],$aclRow['resource_name'],$accessName);
                            }
                        }
                    }
                }
            }
        }
    }
}
