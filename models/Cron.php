<?php

namespace pfmAPI\Models;

use pfmAPI\Apps\Misc\Models\Misc;

class Cron extends \Phalcon\Mvc\Model
{
    protected $id;

    protected $job_code;

    protected $cron_expr;

    protected $status;

    protected $messages;

    protected $created_at;

    protected $scheduled_at;

    protected $executed_at;

    protected $finished_at;

    protected $host;

    protected $pid;

    protected $progress_message;

    protected $last_seen;

    protected $scheduled_by;

    protected $scheduled_reason;

    protected $kill_request;

    const STATUS_PENDING = 'pending';
    const STATUS_RUNNING = 'running';
    const STATUS_SUCCESS = 'success';
    const STATUS_MISSED = 'missed';
    const STATUS_ERROR = 'error';

    const STATUS_KILLED = 'killed';
    const STATUS_DISAPPEARED = 'gone';
    const STATUS_DIDNTDOANYTHING = 'nothing';
    const STATUS_SKIP_OTHERJOBRUNNING = 'other_job_running';

    const REASON_RUNNOW_WEB = 'run_now';
    const REASON_GENERATESCHEDULES = 'generate_schedules';

    public function getSource()
    {
        return "crons";
    }

    /**
     * Checks the observer's cron expression against time
     *
     * Supports $this->setCronExpr('* 0-5,10-59/5 2-10,15-25 january-june/2 mon-fri')
     *
     * @param string $time
     * @return boolean
     */
    public function trySchedule($time)
    {
        $e = $this->getCronExpr();
        if (!$e || !$time) {
            return false;
        }
        if (!is_numeric($time)) {
            $time = strtotime($time);
        }

        $d = getdate($time);
        $e = explode(' ',$this->getCronExpr());

        $match = $this->matchCronExpression($e[0], $d['minutes'])
            && $this->matchCronExpression($e[1], $d['hours'])
            && $this->matchCronExpression($e[2], $d['mday'])
            && $this->matchCronExpression($e[3], $d['mon'])
            && $this->matchCronExpression($e[4], $d['wday']);

        if ($match) {
            $this->setCreatedAt(strftime('%Y-%m-%d %H:%M:%S', time()));
            $this->setScheduledAt(strftime('%Y-%m-%d %H:%M', $time));
        }
        return $match;
    }

    public function matchCronExpression($expr, $num)
    {
        // handle ALL match
        if ($expr==='*') {
            return true;
        }

        // handle multiple options
        if (strpos($expr,',')!==false) {
            foreach (explode(',',$expr) as $e) {
                if ($this->matchCronExpression($e, $num)) {
                    return true;
                }
            }
            return false;
        }

        // handle modulus
        if (strpos($expr,'/')!==false) {
            $e = explode('/', $expr);
            if (sizeof($e)!==2) {
                throw new \Exception("Invalid cron expression, expecting 'match/modulus': ".$expr);
            }
            if (!is_numeric($e[1])) {
                throw new \Exception("Invalid cron expression, expecting numeric modulus: ".$expr);
            }
            $expr = $e[0];
            $mod = $e[1];
        } else {
            $mod = 1;
        }

        // handle all match by modulus
        if ($expr==='*') {
            $from = 0;
            $to = 60;
        }
        // handle range
        elseif (strpos($expr,'-')!==false) {
            $e = explode('-', $expr);
            if (sizeof($e)!==2) {
                throw new \Exception("Invalid cron expression, expecting 'from-to' structure: ".$expr);
            }

            $from = $this->getNumeric($e[0]);
            $to = $this->getNumeric($e[1]);
        }
        // handle regular token
        else {
            $from = $this->getNumeric($expr);
            $to = $from;
        }

        if ($from===false || $to===false) {
            throw new \Exception("Invalid cron expression: ".$expr);
        }

        return ($num>=$from) && ($num<=$to) && ($num%$mod===0);
    }

    public function getNumeric($value)
    {
        static $data = array(
            'jan'=>1,
            'feb'=>2,
            'mar'=>3,
            'apr'=>4,
            'may'=>5,
            'jun'=>6,
            'jul'=>7,
            'aug'=>8,
            'sep'=>9,
            'oct'=>10,
            'nov'=>11,
            'dec'=>12,

            'sun'=>0,
            'mon'=>1,
            'tue'=>2,
            'wed'=>3,
            'thu'=>4,
            'fri'=>5,
            'sat'=>6,
        );

        if (is_numeric($value)) {
            return $value;
        }

        if (is_string($value)) {
            $value = strtolower(substr($value,0,3));
            if (isset($data[$value])) {
                return $data[$value];
            }
        }

        return false;
    }

    /**
     * Schedule this task to be executed at a given time
     *
     * @param int $time
     * @return Cron
     */
    public function schedule($time = null)
    {
        if (is_null($time)) {
            $time = time();
        }
        $this->setStatus(self::STATUS_PENDING);
        $this->setCreatedAt(strftime('%Y-%m-%d %H:%M:%S', time()));
        $this->setScheduledAt(strftime('%Y-%m-%d %H:%M:00', $time));
        $this->save();
        return $this;
    }

    /**
     * Run this task now
     *
     * @param string $model
     * @param string $method
     *
     * @throws \Exception
     * @return Cron
     */
    public function runNow($model,$method)
    {
        // if this schedule doesn't exist yet, create it
        if (!$this->getCreatedAt()) {
            $this->schedule();
        }

        // lock job (see below) prevents the exact same schedule from being executed from more than one process (or server)
        // the following check will prevent multiple schedules of the same type to be run in parallel

        if (CronScheduler::isJobCodeRunning($this->getJobCode(), $this->getId())) {
            $this->setStatus(self::STATUS_SKIP_OTHERJOBRUNNING);
            if(!$this->save()) {
                $messages = implode(',', $this->getMessages());
                throw new \Exception(
                    'Could not save a Cron: '.$messages
                );
            }
            Misc::log('The cron "'.$this->getJobCode().'" will not be executed because there is already another process with the same job code running. Skipping.','cron.log');
            return $this;
        }

        try {
            $startTime = time();
            $this->setExecutedAt(strftime('%Y-%m-%d %H:%M:%S', $startTime));
            $this->setLastSeen(strftime('%Y-%m-%d %H:%M:%S', $startTime));
            $this->setStatus(self::STATUS_RUNNING);
            $this->setHost(gethostname());
            $this->setPid(getmypid());
            if(!$this->save()) {
                $messages = implode(',', $this->getMessages());
                throw new \Exception(
                    'Could not save a Cron: '.$messages
                );
            }

            $this->_startBufferToMessages();
            try {
                // this is where the magic happens
                $messages= call_user_func($model.'::'.$method);
                $this->_stopBufferToMessages();

            } catch (\Exception $e) {
                $this->_stopBufferToMessages();
                throw $e;
            }

            if (!empty($messages)) {
                if (is_object($messages)) {
                    $messages = get_class($messages);
                } elseif (!is_scalar($messages)) {
                    $messages = var_export($messages, 1);
                }
                $this->addMessages(PHP_EOL . '---RETURN_VALUE---' . PHP_EOL . $messages);
            }

            // schedules can report an error state by returning a string that starts with "ERROR:"
            if ((is_string($messages) && strtoupper(substr($messages, 0, 6)) == 'ERROR:') || $this->getStatus() === self::STATUS_ERROR) {
                $this->setStatus(self::STATUS_ERROR);
            } elseif ((is_string($messages) && strtoupper(substr($messages, 0, 7)) == 'NOTHING') || $this->getStatus() === self::STATUS_DIDNTDOANYTHING) {
                $this->setStatus(self::STATUS_DIDNTDOANYTHING);
            } else {
                $this->setStatus(self::STATUS_SUCCESS);
            }
        } catch (\Exception $e) {
            $this->setStatus(self::STATUS_ERROR);
            $this->addMessages(PHP_EOL . '---EXCEPTION---' . PHP_EOL . $e->__toString());
        }

        $this->setFinishedAt(strftime('%Y-%m-%d %H:%M:%S', time()));
        if(!$this->save()) {
            $messages = implode(',', $this->getMessages());
            throw new \Exception(
                'Could not save a Cron: '.$messages
            );
        }

        return $this;
    }

    public function isAlive()
    {
        if ($this->getStatus() == self::STATUS_RUNNING) {
            if (time() - strtotime($this->getLastSeen()) < 2 * 60) { // TODO: make this configurable
                return true;
            } elseif ($this->getHost() == gethostname()) {
                if ($this->checkPid()) {
                    $this->setLastSeen(strftime('%Y-%m-%d %H:%M:%S', time()));
                    $this->save();
                    return true;
                } else {
                    $this->markAsDisappeared(sprintf('Process "%s" on host "%s" cannot be found anymore', $this->getPid(), $this->getHost()));
                    return false; // dead
                }
            } else {
                // we don't know because the task is running on a different server
                return null;
            }
        }
        return false;
    }

    /**
     * Mark task as disappeared
     *
     * @param string $message
     * @return void
     */
    public function markAsDisappeared($message = null)
    {
        if (!is_null($message)) {
            $this->setMessages($message);
        }
        $this->setStatus(self::STATUS_DISAPPEARED);
        $this->setFinishedAt($this->getLastSeen());
        $this->save();
    }

    /**
     * Check if process is running (linux only)
     *
     * @return bool
     */
    public function checkPid()
    {
        $pid = intval($this->getPid());
        return $pid && file_exists('/proc/' . $pid);
    }

    /**
     * Redirect all output to the messages field of this Schedule.
     *
     * We use ob_start with `_addBufferToMessages` to redirect the output.
     *
     * @return $this
     */
    protected function _startBufferToMessages()
    {
        $this->addMessages('---START---' . PHP_EOL);

        ob_start(
            array($this, '_addBufferToMessages')
        );
    }

    /**
     * Stop redirecting all output to the messages field of this Schedule.
     *
     * We use ob_end_flush to stop redirecting the output.
     *
     * @return $this
     */
    protected function _stopBufferToMessages()
    {
        ob_end_flush();
        $this->addMessages('---END---' . PHP_EOL);
    }

    /**
     * Used as callback function to redirect the output buffer
     * directly into the messages field of this schedule.
     *
     * @param $buffer
     *
     * @return string
     */
    public function _addBufferToMessages($buffer)
    {
        $this->addMessages($buffer);
        $this->save();

        return $buffer;
    }

    /**
     * Append data to the current messages field.
     *
     * @param $messages
     *
     * @return $this
     */
    public function addMessages($messages)
    {
        $this->setMessages($this->getMessages() . $messages);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getJobCode()
    {
        return $this->job_code;
    }

    /**
     * @param mixed $job_code
     */
    public function setJobCode($job_code)
    {
        $this->job_code = $job_code;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getScheduledAt()
    {
        return $this->scheduled_at;
    }

    /**
     * @param mixed $scheduled_at
     */
    public function setScheduledAt($scheduled_at)
    {
        $this->scheduled_at = $scheduled_at;
    }

    /**
     * @return mixed
     */
    public function getExecutedAt()
    {
        return $this->executed_at;
    }

    /**
     * @param mixed $executed_at
     */
    public function setExecutedAt($executed_at)
    {
        $this->executed_at = $executed_at;
    }

    /**
     * @return mixed
     */
    public function getFinishedAt()
    {
        return $this->finished_at;
    }

    /**
     * @param mixed $finished_at
     */
    public function setFinishedAt($finished_at)
    {
        $this->finished_at = $finished_at;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getProgressMessage()
    {
        return $this->progress_message;
    }

    /**
     * @param mixed $progress_message
     */
    public function setProgressMessage($progress_message)
    {
        $this->progress_message = $progress_message;
    }

    /**
     * @return mixed
     */
    public function getLastSeen()
    {
        return $this->last_seen;
    }

    /**
     * @param mixed $last_seen
     */
    public function setLastSeen($last_seen)
    {
        $this->last_seen = $last_seen;
    }

    /**
     * @return mixed
     */
    public function getScheduledBy()
    {
        return $this->scheduled_by;
    }

    /**
     * @param mixed $scheduled_by
     */
    public function setScheduledBy($scheduled_by)
    {
        $this->scheduled_by = $scheduled_by;
    }

    /**
     * @return mixed
     */
    public function getScheduledReason()
    {
        return $this->scheduled_reason;
    }

    /**
     * @param mixed $scheduled_reason
     */
    public function setScheduledReason($scheduled_reason)
    {
        $this->scheduled_reason = $scheduled_reason;
    }

    /**
     * @return mixed
     */
    public function getKillRequest()
    {
        return $this->kill_request;
    }

    /**
     * @param mixed $kill_request
     */
    public function setKillRequest($kill_request)
    {
        $this->kill_request = $kill_request;
    }

    /**
     * @return mixed
     */
    public function getCronExpr()
    {
        return $this->cron_expr;
    }

    /**
     * @param mixed $cron_expr
     */
    public function setCronExpr($cron_expr)
    {
        $this->cron_expr = $cron_expr;
    }
}
