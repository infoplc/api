<?php

namespace pfmAPI\Models;

class CronJob extends \Phalcon\Mvc\Model
{
    protected $job_code;

    protected $can_be_scheduled;

    public function getSource()
    {
        return "cron_jobs";
    }
    /**
     * @return mixed
     */
    public function getJobCode()
    {
        return $this->job_code;
    }

    /**
     * @param mixed $job_code
     */
    public function setJobCode($job_code)
    {
        $this->job_code = $job_code;
    }

    /**
     * @return mixed
     */
    public function getCanBeScheduled()
    {
        return $this->can_be_scheduled;
    }

    /**
     * @param mixed $can_be_scheduled
     */
    public function setCanBeScheduled($can_be_scheduled)
    {
        $this->can_be_scheduled = $can_be_scheduled;
    }
}
