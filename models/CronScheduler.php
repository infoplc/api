<?php

namespace pfmAPI\Models;

use Phalcon\Config\Adapter\Ini as IniConfig,
    Phalcon\Cache\Frontend\Data,
    Phalcon\Cache\Backend\File,
    pfmAPI\Exceptions\HTTPException,
    pfmAPI\Apps\Misc\Models\Misc;

class CronScheduler extends \Phalcon\Mvc\Model
{
    protected $cronJobs;

    const MAX_AHEAD_SCHEDULE_TIME = 3600;

    public function run() {
        $this->loadCronFiles();

        $hasPendingSchedules = false;
        /** @var Cron $cron */
        foreach ($this->getPendingSchedules() as $cron) {
            if(!$hasPendingSchedules) $hasPendingSchedules = true;
            if(isset($this->cronJobs[$cron->getJobCode()])) {
                $cronInfo = $this->cronJobs[$cron->getJobCode()];
                $pid = pcntl_fork();
                switch($pid) {
                    case -1:
                        throw new \Exception('Could not fork for a new child when executing a cron');
                    case 0:
                        //Children
                        $this->generateNewDbConnections();
                        $cron->runNow($cronInfo['model'],$cronInfo['method']);
                        exit($cron->getId());
                        break;
                    default:
                        //Father
                        $this->generateNewDbConnections();
                        continue;
                }
            }
        }

        while ($hasPendingSchedules && (pcntl_waitpid(0, $status) != -1)) {
            $status = pcntl_wexitstatus($status);
        }
        $this->generateSchedules();
        $this->cleanup();
    }

    /** Generates new database connections. Necessary for multitasking */
    protected function generateNewDbConnections() {
        $config = new IniConfig(__DIR__ . '/../config/config.ini');
        $this->getDI()->remove('db');
        $this->getDI()->set('db', function() use($config) {
            $adapter = $config->database->adapter;
            return new $adapter($config->database->toArray());
        });
        $this->getDI()->set('db_webclient', function() use($config) {
            $adapter = $config->database_webclient->adapter;
            return new $adapter($config->database_webclient->toArray());
        });
    }

    /**
     * Generate cron schedule.
     * Rewrites the original method to remove duplicates afterwards (that exists because of a bug)
     *
     * @return $this
     */
    public function generateSchedules()
    {
        $this->loadCronFiles();
        foreach ($this->cronJobs as $jobCode => $jobParameters) {
            $this->generateSchedulesForJob($jobCode, $jobParameters);
        }

        $this->deleteDuplicates();

        return $this;
    }

    public function loadCronFiles()
    {
        $this->cacheDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
        $frontCache = new Data(array(
            "lifetime" => 172800
        ));

        $cache = new File($frontCache, array(
            "cacheDir" => $this->cacheDir
        ));

        $cacheKey = 'cron_jobs.cache';
        $this->cronJobs = $cache->get($cacheKey);
        if ($this->cronJobs === null) {
            $cronsPath = __DIR__ . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR.'crons';
            if ($handle = opendir($cronsPath)) {
                $this->cronJobs = array();
                while (false !== ($entry = readdir($handle))) {
                    if (pathinfo($cronsPath . DIRECTORY_SEPARATOR . $entry, PATHINFO_EXTENSION) === 'xml') {
                        $xml = simplexml_load_file($cronsPath . DIRECTORY_SEPARATOR . $entry);
                        foreach ($xml as $cron) {
                            if (isset($this->cronJobs[$cron->getName()])) {
                                throw new \Exception("Another cron has the same code: " . $cron->getName());
                            }
                            $cronExpr = explode(' ', (string)$cron->children()->cron_expr);
                            if (count($cronExpr) !== 5) {
                                throw new \Exception("A cron expression has not 5 elements. Job code: " . $cron->getName());
                            }
                            $this->cronJobs[$cron->getName()]['cron_expr'] = $cronExpr;
                            $this->cronJobs[$cron->getName()]['model'] = (string)$cron->children()->model;
                            $this->cronJobs[$cron->getName()]['method'] = (string)$cron->children()->method;
                        }
                    }
                }
                closedir($handle);
            }
            // Store it in the cache
            $cache->save($cacheKey, $this->cronJobs);
        }
    }

    /**
     * Generate jobs for config information
     *
     * @param string $code
     * @param array $parameters
     *
     * @throws HTTPException
     *
     * @return $this
     */
    public function generateSchedulesForJob($code, $parameters)
    {
        $cronJob = CronJob::findFirst(
            array(
                "conditions" => "[job_code] = ?0",
                "bind"       => array(
                    0 => $code,
                )
            ));
        if(!$cronJob) {
            $cronJob = $this->insertCronJob($code);
        }

        if (!$cronJob->getCanBeScheduled()) {
            return $this;
        }

        $exists = array();
        foreach ($this->getAllSchedules($code, time() + self::MAX_AHEAD_SCHEDULE_TIME) as $schedule) {
            /* @var Cron $schedule */
            $exists[$schedule->getJobCode() . '/' . $schedule->getScheduledAt()] = 1;
        }

        $now = time();
        $timeAhead = $now + self::MAX_AHEAD_SCHEDULE_TIME;

        for ($time = $now; $time < $timeAhead; $time += 60) {
            $cronToSave = new Cron();
            $cronToSave->setJobCode($code);
            $cronToSave->setCronExpr(implode(' ',$parameters['cron_expr']));

            $ts = strftime('%Y-%m-%d %H:%M:00', $time);
            if (!empty($exists[$code.'/'.$ts])) {
                // already scheduled
                continue;
            }
            if (!$cronToSave->trySchedule($time)) {
                // time does not match cron expression
                continue;
            }

            $cronToSave->setJobCode($code);
            $cronToSave->setStatus(Cron::STATUS_PENDING);
            $cronToSave->setScheduledReason(Cron::REASON_GENERATESCHEDULES);
            if (!$cronToSave->create()) {
                $messages = implode(',', $this->getMessages());
                throw new HTTPException(
                    "The cron entry could not be inserted right now.",
                    500,
                    array(
                        'dev' => $messages,
                        'internalCode' => 'CronScheduler001',
                    ));
            }
        }

        return $this;
    }

    /**
     * Get pending schedules
     *
     * @param string|null $jobCode
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getPendingSchedules($jobCode = null)
    {
        $extraConditions = '';
        $bindConditions = array(
            0 => Cron::STATUS_PENDING,
            1 => strftime('%Y-%m-%d %H:%M:%S', time()),
        );
        if($jobCode) {
            $extraConditions = ' AND [job_code] = ?2';
            $bindConditions[2] = $jobCode;
        }

        $pendingSchedules = Cron::find(
            array(
                "conditions" => '[status] = ?0 AND [scheduled_at] < ?1'.$extraConditions,
                "bind"       => $bindConditions,
            ));

        return $pendingSchedules;
    }

    /**
     * Get pending schedules
     *
     * @param string|null $jobCode
     *
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getAllSchedules($jobCode = null, $maxScheduledAt = null)
    {
        if(!$maxScheduledAt) {
            $maxScheduledAt = time();
        }
        $extraConditions = '';
        $bindConditions = array(
            1 => strftime('%Y-%m-%d %H:%M:%S', $maxScheduledAt),
        );
        if($jobCode) {
            $extraConditions = ' AND [job_code] = ?2';
            $bindConditions[2] = $jobCode;
        }

        $pendingSchedules = Cron::find(
            array(
                "conditions" => '[scheduled_at] < ?1'.$extraConditions,
                "bind"       => $bindConditions,
            ));

        return $pendingSchedules;
    }

    /**
     * @param $jobCode
     * @return CronJob
     * @throws \Exception
     */
    protected function insertCronJob($jobCode) {
        $newCronJob = new CronJob();
        $newCronJob->setJobCode($jobCode);
        $newCronJob->setCanBeScheduled(true);
        if (!$newCronJob->save()) {
            $messages = implode(',', $newCronJob->getMessages());
            throw new \Exception('The cron job could not be inserted right now: '.$messages);
        }
        return $newCronJob;
    }

    /**
     * Delete duplicate crons
     */
    public function deleteDuplicates()
    {
        $di = \Phalcon\DI::getDefault();
        /** @var \Phalcon\Mvc\Model\Query\BuilderInterface $builder */
        $builder =  $di->get('modelsManager')->createBuilder()
            ->from('\pfmAPI\Models\Cron')
            ->where('[\pfmAPI\Models\Cron].[status] = :value:', ['value' => Cron::STATUS_PENDING]);

        $builder->columns(array(
            'GROUP_CONCAT([\pfmAPI\Models\Cron].id) AS ids',
            'CONCAT([\pfmAPI\Models\Cron].job_code, [\pfmAPI\Models\Cron].scheduled_at) AS jobkey',
            'count(*) AS qty'
        ));
        $builder->groupBy('jobkey');
        $builder->having('qty > 1');
        $results = $builder->getQuery()->execute()->toArray();
        foreach ($results as $row) {
            $ids = explode(',', $row['ids']);
            $removeIds = array_slice($ids, 1);
            foreach ($removeIds as $id) {
                Cron::findFirst($id)->delete();
            }
        }
    }

    /**
     * Check if there's already a job running with the given code
     *
     * @param string $jobCode
     * @param int $ignoreId
     * @return bool
     */
    public static function isJobCodeRunning($jobCode, $ignoreId = null)
    {
        $specialConditions = '';
        $binds = array(
            0 => Cron::STATUS_RUNNING,
            1 => $jobCode,
        );
        if($ignoreId) {
            $specialConditions = ' AND id <> ?2';
            $binds[2] = $ignoreId;
        }
       $pendingSchedules = Cron::find(
            array(
                "conditions" => '[status] = ?0 AND [job_code] = ?1'.$specialConditions,
                "bind"       => $binds
            ));

        /** @var Cron $cron */
        foreach($pendingSchedules as $cron) {
            $alive = $cron->isAlive();
            if ($alive !== false) { // TODO: how do we handle null (= we don't know because might be running on a different server?
                return true;
            }
        }
        return false;
    }

    /**
     * Clean up the history of tasks
     * This override deals with custom states added in Aoe_Scheduler
     *
     * @return CronScheduler
     */
    public function cleanup()
    {
        $schedules = Cron::find(
            array(
                "conditions" => '[status] <> ?0 AND [status] <> ?1',
                "bind"       => array(
                    0 => Cron::STATUS_PENDING,
                    1 => Cron::STATUS_RUNNING
                )
            ));

        $historyLifetimes = array(
            Cron::STATUS_KILLED =>          48*3600, //2 days
            Cron::STATUS_DISAPPEARED =>     168*3600, //7 days
            Cron::STATUS_DIDNTDOANYTHING => 48*3600,
            Cron::STATUS_SUCCESS =>         48*3600,
            Cron::STATUS_MISSED =>          168*3600,
            Cron::STATUS_ERROR =>           168*3600,
        );

        $now = time();
        foreach ($schedules as $cron) { /* @var $cron Cron*/
            if (isset($historyLifetimes[$cron->getStatus()])) {
                if (strtotime($cron->getExecutedAt()) < $now - $historyLifetimes[$cron->getStatus()]) {
                    $cron->delete();
                }
            }
        }

        // delete successful tasks (beyond the configured max number of tasks to keep)
        $maxNo = 100;
        if ($maxNo) {
            $schedules = Cron::find(
                array(
                    "conditions" => '[status] = ?0',
                    "bind"       => array(
                        0 => Cron::STATUS_SUCCESS,
                    )
                ));

            $counter = array();
            foreach ($schedules as $cron) { /* @var $cron Cron*/
                $jobCode = $cron->getJobCode();
                if (!isset($counter[$jobCode])) {
                    $counter[$jobCode] = 0;
                }
                $counter[$jobCode]++;
                if ($counter[$jobCode] > $maxNo) {
                    $cron->delete();
                }
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobCode()
    {
        return $this->job_code;
    }

    /**
     * @param mixed $job_code
     */
    public function setJobCode($job_code)
    {
        $this->job_code = $job_code;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getScheduledAt()
    {
        return $this->scheduled_at;
    }

    /**
     * @param mixed $scheduled_at
     */
    public function setScheduledAt($scheduled_at)
    {
        $this->scheduled_at = $scheduled_at;
    }

    /**
     * @return mixed
     */
    public function getExecutedAt()
    {
        return $this->executed_at;
    }

    /**
     * @param mixed $executed_at
     */
    public function setExecutedAt($executed_at)
    {
        $this->executed_at = $executed_at;
    }

    /**
     * @return mixed
     */
    public function getFinishedAt()
    {
        return $this->finished_at;
    }

    /**
     * @param mixed $finished_at
     */
    public function setFinishedAt($finished_at)
    {
        $this->finished_at = $finished_at;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getProgressMessage()
    {
        return $this->progress_message;
    }

    /**
     * @param mixed $progress_message
     */
    public function setProgressMessage($progress_message)
    {
        $this->progress_message = $progress_message;
    }

    /**
     * @return mixed
     */
    public function getLastSeen()
    {
        return $this->last_seen;
    }

    /**
     * @param mixed $last_seen
     */
    public function setLastSeen($last_seen)
    {
        $this->last_seen = $last_seen;
    }

    /**
     * @return mixed
     */
    public function getScheduledBy()
    {
        return $this->scheduled_by;
    }

    /**
     * @param mixed $scheduled_by
     */
    public function setScheduledBy($scheduled_by)
    {
        $this->scheduled_by = $scheduled_by;
    }

    /**
     * @return mixed
     */
    public function getScheduledReason()
    {
        return $this->scheduled_reason;
    }

    /**
     * @param mixed $scheduled_reason
     */
    public function setScheduledReason($scheduled_reason)
    {
        $this->scheduled_reason = $scheduled_reason;
    }

    /**
     * @return mixed
     */
    public function getKillRequest()
    {
        return $this->kill_request;
    }

    /**
     * @param mixed $kill_request
     */
    public function setKillRequest($kill_request)
    {
        $this->kill_request = $kill_request;
    }

    /**
     * @return mixed
     */
    public function getCronExpr()
    {
        return $this->cron_expr;
    }

    /**
     * @param mixed $cron_expr
     */
    public function setCronExpr($cron_expr)
    {
        $this->cron_expr = $cron_expr;
    }
}
