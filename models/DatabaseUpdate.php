<?php
namespace pfmAPI\Models;

use Phalcon\DI,
    Phalcon\Exception,
    pfmAPI\Apps\Misc\Models\Misc;

/**
 * Class DatabaseUpdate
 * @package PhalconRest\Models
 *
 * Checks the current version of the database and updates the database if necessary.
 *
 * For performance reasons, if a 'current_database_version.lock' file exists in the 'cache' directory, it will not check any
 * update. To force the check and update actions, just remove the 'current_database_version.lock' file, and when it has finished,
 * it will create it again.
 *
 * Put the database schema changes in the 'schema' directory, and put the data changes in the 'data' directory. The
 * schema updates will be applied first.
 *
 * The sql update files must be sorted alphabetically, and the versions must be correlative between files. For example:
 * 0.0-0.1.sql
 * 0.2-0.3.sql
 * 0.3-0.3.1.sql
 *
 */
class DatabaseUpdate {
    /** @var  \Phalcon\DiInterface */
    protected $di;

    /** @var \Phalcon\Db\AdapterInterface */
    protected $connection;

    /** @var string */
    protected $updatesBaseDir;

    /** @var string */
    protected $cacheDir;

    /** @var  string */
    protected $currentDatabaseVersion;

    /** @var  array */
    protected $files;

    /** @var string */
    protected $dataFilePattern = '/([-+]?[0-9]*\.?[0-9]+)\[([-+]?[0-9]*\.?[0-9]+)]/';

    public function __construct() {
        $this->updatesBaseDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'sqlupdate' . DIRECTORY_SEPARATOR;
        $this->cacheDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR;
    }

    public function checkDatabaseUpdates() {
        if(!$this->checkLockFile()) {
            Misc::createMaintenanceFile();
            $this->setupDatabaseUpdating();
            $this->updateVersions();
            $this->writeLockFile();
            Misc::removeMaintenanceFile();
        }
    }

    protected function setupDatabaseUpdating() {
        $this->di = DI::getDefault();
        $this->connection = $this->di->get('db');
    }

    /**
     * If file 'current_version.lock' does not exist, it will be created again and will trigger a database update
     *
     * @return bool False if 'current_version.lock' file does not exist, true if it exists.
     */
    protected function checkLockFile() {
        if(file_exists($this->cacheDir.'current_database_version.lock'))
            return true;
        return false;
    }

    /**
     * It checks the current database version and applies the necessary updates found in 'schema' and 'data' directories.
     */
    protected function updateVersions() {
        $currentVersions = $this->checkCurrentDatabaseVersion();
        $this->updateDatabase($currentVersions['schema_version'], $currentVersions['data_version']);
    }

    protected function updateDatabase($currentSchemaVersion, $currentDataVersion) {
        // forzamos la actualización de data (aunque sea la misma versión, pueden haber nuevas revisiones)
        if ($currentSchemaVersion)
            $currentDataVersion = $this->applyUpdates('data', $currentDataVersion, $currentSchemaVersion);

        while (($nextSchemaFileVersion = $this->checkNextFileVersion($currentSchemaVersion, 'schema')) && (version_compare($nextSchemaFileVersion, $currentSchemaVersion, "!=")))
        {
            $currentSchemaVersion = $this->applyUpdates('schema', $currentSchemaVersion, $nextSchemaFileVersion);
            $currentDataVersion = $this->applyUpdates('data', $currentDataVersion, $nextSchemaFileVersion);
        }
    }

    /**
     * Returns an array with the current 'schema_version' and 'data_version' of the database.
     *
     * @return array
     * @throws \Exception
     */
    protected function checkCurrentDatabaseVersion() {
        try {
            $searchSchemaSql = 'SELECT `schema_version` FROM `sql_updates` WHERE `schema_version` IS NOT NULL ORDER BY `id` DESC LIMIT 1';
            $result = $this->connection->query($searchSchemaSql);
            $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
            $resultArray = $result->fetch();
            $returnArray['schema_version'] = $resultArray['schema_version'];

            $searchDataSql = 'SELECT `data_version` FROM `sql_updates` WHERE `data_version` IS NOT NULL ORDER BY `id` DESC LIMIT 1';
            $result = $this->connection->query($searchDataSql);
            $result->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
            $resultArray = $result->fetch();
            $returnArray['data_version'] = $resultArray['data_version'];
        } catch (\Exception $e) {
            if($e->getCode() == '42S02') {
               return array('schema_version' => null, 'data_version' => null);
            } else {
                throw $e;
            }
        }

        return $returnArray;
    }

    /**
     * Checks the last file version for the directory given. The last file ordered alphabetically will be the last update.
     *
     * @param $dir
     * @return null
     * @throws \Exception
     */
    protected function checkLastFileVersion($dir) {
        $this->getDirFiles($dir);
        if(!$this->checkAllFilesAreSql($this->files[$dir])) {
            throw new \Exception('All the files in '.$this->updatesBaseDir.$dir.' must be .sql files');
        }
        $lastFileName = basename(array_reverse($this->files[$dir])[0], '.sql');
        if(strcmp($lastFileName,'.') === 0 || strcmp($lastFileName,'..') === 0)
            return null;

        if ($dir == 'data')
        {
            preg_match($this->dataFilePattern, $lastFileName, $versions);
            if(count($versions) !== 3) {
                throw new \Exception('Data files must be like "0.1[0].sql" (version[revision]).sql');
            }
            return $versions[1] . "[".$versions[2]."]";
        }
        else
        {
            $versions = explode('-',$lastFileName);
            if(count($versions) !== 2) {
                throw new \Exception('The file must have in its name the versions dash-separated (i.e. "0.1-0-2.sql"');
            }
            return $versions[1];
        }
    }


    /**
     * Checks the next file version for the directory given.
     *
     * @param $dir
     * @param $currentVersion
     * @return null
     * @throws \Exception
     */
    protected function checkNextFileVersion($currentVersion, $dir) {
        $this->getDirFiles($dir);
        if(!$this->checkAllFilesAreSql($this->files[$dir])) {
            throw new \Exception('All the files in '.$this->updatesBaseDir.$dir.' must be .sql files');
        }


        foreach ($this->files[$dir] as $file)
        {
            if ($dir == 'data')
            {
                preg_match($this->dataFilePattern, basename($file, '.sql'), $versions);
                if(count($versions) !== 3) {
                    throw new \Exception('Data files must be like "0.1[0].sql" (version[revision]).sql');
                }
                if (version_compare($versions[1], $currentVersion, ">"))
                    return $versions[1] . "[".$versions[2]."]";
            }
            else
            {
                $versions = explode('-', basename($file, '.sql'));
                if(count($versions) !== 2) {
                    throw new \Exception('The file must have in its name the versions dash-separated (i.e. "0.1-0-2.sql"');
                }
                if (version_compare($versions[1], $currentVersion, ">"))
                    return $versions[1];
            }
        }
        return null;
    }


    protected function getDirFiles($dir) {
        $this->files[$dir] = scandir($this->updatesBaseDir . $dir);

        //Unset '.' and '..' dirs
        $this->files[$dir] = array_diff($this->files[$dir], array('.', '..'));

        if ($dir == 'schema')
            usort($this->files[$dir],
                function($a, $b)
                {
                    // $a = 0.10-0.11.sql
                    // $b = 0.2-0.3.sql
                    $v1 = explode("-", $a); // v1 = 0.10
                    $v2 = explode("-", $b); // v2 = 0.2

                    if (version_compare($v1[0], $v2[0], '='))
                        return 0;
                    elseif (version_compare($v1[0], $v2[0], '<'))
                        return -1;
                    else
                        return 1;
                });
        else
            usort($this->files[$dir],
                function($a, $b)
                {
                    // $a = 0.2[0].sql
                    // $b = 0.12[2].sql
                    preg_match($this->dataFilePattern, $a, $v1);
                    preg_match($this->dataFilePattern, $b, $v2);

                    $r1 = $v1[2];
                    $r2 = $v2[2];

                    if (version_compare($v1[1], $v2[1], '='))
                    {
                        if ($r1 == $r2)
                            return 0;
                        else
                            return ((int)$r1 < (int)$r2) ? -1 : 1;
                    }
                    elseif (version_compare($v1[1], $v2[1], '<'))
                        return -1;
                    else
                        return 1;
                });

    }

    protected function checkAllFilesAreSql($fileNames) {
        foreach($fileNames as $fileName) {
            if(strcmp($fileName,'.') === 0 || strcmp($fileName,'..') === 0)
                continue;
            if(strpos($fileName,'.sql') === false)
                return false;
        }
        return true;
    }

    /**
     * If a $fromVersion is given, it will apply the update files from the $fromVersion to the $toVersion in the
     * given $filesDir.
     * If is not given any $fromVersion, it will apply all the update files from the given $filesDir.
     * It checks that the files between $fromVersion and $toVersion are correlative (i.e. 0.0-0.1.sql, 0.2-0.3.sql,
     * 0.3-0.4.sql, etc.). If the files are not correlative (i.e. 0.0-0.1.sql, 0.3-0.4.sql), will abort all the updates.
     *
     * @param string $filesDir
     * @param string |null $fromVersion
     * @param string $toVersion
     * @throws \Exception
     * @return string
     */
    protected function applyUpdates($filesDir, $fromVersion, $toVersion) {
        if (!isset($this->files[$filesDir]))
            $this->getDirFiles($filesDir);

        $sqlFiles = $this->files[$filesDir];

        if(!$fromVersion)
            $fromVersion = ($filesDir == 'data') ? '0.0[-1]' : '0.0';   // el -1 es para forzar la 0.0[0], de lo contrario, la 0.0[0] no se ejecutaria

        if ($filesDir == 'data')
        {
            // sacamos la versión y revisión en caso de 'data' ($fromVersion = 0.0[0])
            preg_match($this->dataFilePattern, $fromVersion, $versions);
            $fromVersion = $versions[1];
            $fromRevision = $versions[2];
        }

        $filesToExecute = array();
        $fromVersionFound = false;
        $includeFile = false;

        foreach($sqlFiles as $sqlFile) {
            if(strcmp($sqlFile,'.') === 0 || strcmp($sqlFile,'..') === 0)
                continue;

            if ($filesDir=='data')
                preg_match($this->dataFilePattern, basename($sqlFile, '.sql'), $versions);
            else
                $versions = explode('-',basename($sqlFile, '.sql'));

            /*
             * damos por supuesto que la lista que recibimos de ficheros esta ordenada correctamente
             * si es de tipo 'data', seleccionaremos todos los ficheros que hayan a partir de la primera aparición (no incluida) hasta la versión objectivo (incluida)
             */
            if ((($filesDir=='data') && (((version_compare($versions[1], $fromVersion, ">=") && ($versions[2] >= $fromRevision))) or ($fromVersionFound)))
                or (version_compare($versions[0], $fromVersion, "==")))
            {
                if($fromVersionFound && ($filesDir=='schema'))  {
                    throw new \Exception('There is more than one file with the same "from" version (and in case of data, with same revision too): '.$fromVersion);
                }
                $includeFile = (($filesDir=='schema') or (($filesDir=='data') && ($fromVersionFound or (($versions[1]!=$fromVersion) or ($versions[2] > $fromRevision)))));
                $fromVersionFound = true;

                if ($filesDir=='schema')
                {
                    $nextVersionToFind = $versions[1];
                }
                else
                {
                    if (version_compare($versions[1], $toVersion, ">")) { // si estamos en una versión superior a la objetivo (las revisiones no cuentan), cortamos
                        break;
                    }
                    $lastRevisionFound = $versions[2];
                    $lastVersionFound = $versions[1];
                }
            }
            if($includeFile) {
                $includeFile = false;
                if(isset($nextVersionToFind) && sizeof($filesToExecute) && $nextVersionToFind) {
                    $lastVersionsChecked = explode('-',basename($filesToExecute[sizeof($filesToExecute)-1], '.sql'));
                    if(strcmp($lastVersionsChecked[1],$versions[0]) !== 0) {
                        throw new \Exception('Could not find a link between the file '.$filesToExecute[sizeof($filesToExecute)-1].' and '.$sqlFile);
                    }
                    $nextVersionToFind = $versions[1];
                }
                if (isset($nextVersionToFind))
                {
                    $fromVersion = $nextVersionToFind;
                    $fromVersionFound=false;
                }

                $filesToExecute[] = $sqlFile;
                if(($filesDir=='schema') && ($versions[1] == $toVersion))
                    break;
            }
        }

        foreach($filesToExecute as $file) {
            $versions = explode('-',basename($file, '.sql'));
            try {
                $resource = fopen($this->updatesBaseDir.$filesDir.DIRECTORY_SEPARATOR.$file,'r');
                $fileSize = filesize($this->updatesBaseDir.$filesDir.DIRECTORY_SEPARATOR.$file);
                if(!$fileSize) {
                    throw new \Exception('The file '.$file.' is empty');
                }
                $sqlSentences = fread($resource, $fileSize);
                fclose($resource);

                $result = $this->connection->execute($sqlSentences);
                if(!$result) {
                    throw new \Exception('Could not execute the database update sql sentence');
                }
            } catch (\Exception $e) {
                if($filesDir=='data') {
                    $this->saveVersionToDatabase(null, $versions[0]);
                } else {
                    $this->saveVersionToDatabase($versions[0]);
                }
                throw $e;
            }
        }

        if($filesDir=='data') {
            $this->saveVersionToDatabase(null, (isset($lastVersionFound)?$lastVersionFound:$fromVersion)."[".(isset($lastRevisionFound)?$lastRevisionFound:$fromRevision)."]");
            return (isset($lastVersionFound)?$lastVersionFound:$fromVersion)."[".(isset($lastRevisionFound)?$lastRevisionFound:$fromRevision)."]";
        } else {
            $this->saveVersionToDatabase($toVersion);
            return $toVersion;
        }
    }

    protected function saveVersionToDatabase($schemaVersion=null, $dataVersion=null) {
        $insertSql = 'INSERT INTO `sql_updates` (`schema_version`, `data_version`, `update_time`)
                      VALUES (:schema_version, :data_version, :update_time)';
        $result = $this->connection->execute($insertSql,
            array(
                'schema_version'    => $schemaVersion,
                'data_version'      => $dataVersion,
                'update_time'       => date('Y/m/d H:i:s', time()),
            )
        );
        if(!$result) {
            throw new \Exception('Error while updating database version. Tried to insert schema version: "' . $schemaVersion. '", data version: "'.$dataVersion.', update time: "' . date('Y/m/d H:i:s', time()) .'"');
        }
    }

    protected function writeLockFile() {
        $currentVersions = $this->checkCurrentDatabaseVersion();
        Misc::createCacheDirIfNotExists();
        $resource = fopen($this->cacheDir . 'current_database_version.lock','w');
        fwrite($resource, 'Schema Version: '.$currentVersions['schema_version']."\n");
        fwrite($resource, 'Data Version: '.$currentVersions['data_version']."\n");
        fclose($resource);
    }
}