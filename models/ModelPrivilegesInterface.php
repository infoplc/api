<?php
namespace pfmAPI\Models;

interface ModelPrivilegesInterface {
    public static function getUserPrivilege($objectId, $userId);

    public static function getIsUserActive($objectId, $userId);

    public static function getPrivilegesIds();
}