<?php
namespace pfmAPI\Models;

interface ModelRolesInterface {
    public static function getUserPrivilege($objectId, $userId);

    public static function getIsUserActive($objectId, $userId);

    public static function getPrivilegesIds();
}