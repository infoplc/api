<?php
namespace pfmAPI\Preconditions;

use Phalcon\Acl\RoleInterface;
use pfmAPI\Exceptions\HTTPException;
use pfmAPI\Models\ModelRolesInterface;
use pfmAPI\Validations\BaseValidation;
use Phalcon\Acl\Adapter\Memory;
use Phalcon\Di;

class BasePreconditions
{
    public $params;
    public $params_at_least;
    public $token = array("owner" => false, "scopes" => false);
    public $permissions;

    protected $request;
    protected $is_token_validated = false;

    public function __construct()
    {
        $this->request = Di::getDefault()->get('request');
        if ($this->request->has('access_token') || isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $this->checkToken(true);
        }
        $this->setPreconditions();
    }

    public function doCheck()
    {
        $this->checkParameters();
        $this->checkToken();
        $this->checkPermissions();
    }

    public function setPreconditions()
    { /* ... */
    }

    public function checkPermissions()
    {
        if (isset($this->permissions)) {
            if (!isset($this->token['owner']) || $this->token['owner'] != 'user') {
                throw new HTTPException(
                    'In order to check permissions you must provide a token with a USER owner',
                    401,
                    array(
                        'internalCode' => 'Permissions001',
                    ));
            }
            if (!isset($this->permissions['class']) || !isset($this->permissions['resource']) || !isset($this->permissions['access']) || !isset($this->permissions['object_id'])) {
                throw new HTTPException(
                    'The permissions array must have all the following keys: class, resource, access, object_id',
                    401,
                    array(
                        'internalCode' => 'Permissions002',
                    ));
            }

            $class = $this->permissions['class'];

            if (!is_subclass_of($class, 'pfmAPI\Models\ModelRolesInterface')) {
                throw new HTTPException(
                    'The class ' . $class . ' must implement ModelRolesInterface',
                    401,
                    array(
                        'internalCode' => 'Permissions003',
                    ));
            }
            $access = $this->permissions['access'];
            $resource = $this->permissions['resource'];
            $objectId = $this->permissions['object_id'];
            $ownerId = Di::getDefault()->get('oauth2_resource')->getOwnerId();

            /** @var $class ModelRolesInterface $userRole */
            $userRole = $class::getUserRole($objectId, $ownerId);
            /** @var Memory $acl */
            $acl = Di::getDefault()->get('acl');
            if (!$acl->isAllowed($userRole, $resource, $access) || !$class::getIsUserActive($objectId, $ownerId)) {
                throw new HTTPException(
                    'The user with ID "' . $ownerId . '" can not "' . $access . '" a "' . $resource . '" resource in an object of class "' . $class . '" with ID "' . $objectId,
                    403,
                    array(
                        'internalCode' => 'Permissions004',
                    ));
            }
        }
    }

    public function checkToken($notCatchOauthException = false)
    {
        if ($this->is_token_validated) {
            return;
        }
        if (!isset($this->token['owner'])) {
            $this->token['owner'] = false;
        }

        if (!isset($this->token['scopes'])) {
            $this->token['scopes'] = false;
        }

        $di = Di::getDefault();
        $server = $di->getShared('oauth2_resource');
        try {
            // Check that an access token is present and is valid
            $server->isValid();
        } catch (\League\OAuth2\Server\Exception\OAuth2Exception $e) {
            if ($notCatchOauthException) {
                return;
            }
            throw new HTTPException(
                $e->getMessage(),
                401,
                array(
                    'dev' => $e->getMessage(),
                    'internalCode' => 'OAUTH2_INVALID_TOKEN',
                )
            );
        } catch (\Exception $e) {
            $response = $di->get('response');
            $response->setStatusCode(500, "Access denied");
            $response->setContent(json_encode(['status_code' => 500, 'message' => $e->getMessage()]));
            exit($response->send());
        }

        if (($this->token['owner'] !== false) && ($this->token['owner'] != $server->getOwnerType())) {
            throw new HTTPException(
                "Invalid token",
                401,
                array(
                    'dev' => "The action required a token type \"" . $this->token['owner'] . "\", but the token provided was \"" . $server->getOwnerType() . "\"",
                    'internalCode' => 'Oauth003',
                ));
        }

        if ($this->token['scopes'] !== false) {
            $missingScopes = array();
            if (is_string($this->token['scopes']) && !in_array($this->token['scopes'], $server->getScopes())) {
                $missingScopes[] = $this->token['scopes'];
            } else {
                foreach ($this->token['scopes'] as $scope) {
                    if (!in_array($scope, $server->getScopes())) {
                        $missingScopes[] = $scope;
                    }
                }
            }

            if (count($missingScopes) > 0) {
                throw new HTTPException(
                    "Invalid token",
                    401,
                    array(
                        'dev' => "The following scopes were missing: " . implode(", ", $missingScopes),
                        'internalCode' => 'Oauth004',
                    ));
            }
        }
        if (!$notCatchOauthException) {
            $this->is_token_validated = true;
        }
    }

    public function checkParameters()
    {
        if ($this->params != null) {
            $di = Di::getDefault();
            $validation = new BaseValidation();
            $validation->setItems($this->params);

            if ($this->params_at_least !== null) {
                $validation->setItemsAtLeast($this->params_at_least);
            }

            $validation->addValidations();
            $validation->doValidation($di->get('request')->isPut()?$di->get('request')->getPut():$di->get('request')->get());
        }
    }

    /**
     * If we use a token with a 'user' owner type, we can check the roles of the user's token.
     *
     * Needs an array with keys:
     * - class:     The class that we'll use to check which permission the user has.
     * - object_id: The object ID of the class previously defined.
     * - resource:  The resource that the user wants to access.
     * - access:    The access name.
     *
     * @param $permissions array
     */
    public function setPermissions($permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @param $token array
     * @param $doValidate bool Does the check for the token
     */
    public function setToken($token, $doValidate = false)
    {
        $this->token = $token;
        if ($this->request->has('access_token') && $doValidate) {
            $this->checkToken();
        }
    }

    /**
     * @param $parameters array
     */
    public function setParameters($parameters)
    {
        $this->params = $parameters;
    }

    protected function getParam($param)
    {
        $params = $this->getParams();
        if (isset($params[$param])) {
            return $params[$param];
        }
        return null;
    }

    protected function getParams()
    {
        return Di::getDefault()->get('router')->getParams();
    }
}