CREATE TABLE IF NOT EXISTS `api_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` varchar(64) DEFAULT NULL,
  `command` varchar(256) NOT NULL,
  `response_time` float NOT NULL,
  `requested_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;