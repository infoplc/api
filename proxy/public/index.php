<?php

$config = array(
    "url" => "http://api.staddium.web",
    "cookie_domain" => ".staddium.web",
    "database" => array(
            "host" => "localhost",
            "username" => "root",
            "password" => "",
            "database" => "staddium-proxy"
        ),
);
$ignore = array("bower_components", "css", "fonts", "images", "js", "scss", "robots.txt", "humans.txt");
$ip = $_SERVER['REMOTE_ADDR'];

/* **************************************** */

if (substr($_SERVER['REQUEST_URI'], 0, strlen('/debug')) == "/debug")
{
  
  if (isset($_GET['id']))
    {
      echo "<a href='/debug'>atr&aacute;s</a><br /><pre>";
      include "../".$_GET['id'].".query";
      echo "</pre>";
    }
    else
    {
?><html>
<body>
<?php
    
      $mysqli = new mysqli($config['database']['host'], $config['database']['username'], $config['database']['password'], $config['database']['database']);
      if (!$mysqli->connect_errno) {
        //(`client`, `command`, `response_time`, `requested_at`)
          $query = $mysqli->query("SELECT * FROM `api_calls` order by `id` DESC limit 1000");
          while ($obj = $query->fetch_object())
          {
            echo $obj->requested_at . " - <a href='/debug?id=".$obj->id."'>" . $obj->command . "</a> (".$obj->response_time."ms)<br />";
          }
      }
?>
</body>
</html>
<?php
}
die();
}


$url = $config['url'] . $_SERVER['REQUEST_URI'];
$data = $_POST+$_GET;

switch ($_SERVER['REQUEST_METHOD'])
{
    case "GET":
        $data = $_GET;
        $url = $url . ((strpos($url,'?') !== false)?'&':'?') . http_build_query($data);
        //echo("<br />".$url);
        $curl = curl_init($url);
        break;
    case "POST":
        $data = $_POST;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
    case "PUT":
        $data = $_POST;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        //echo http_build_query($data);
        break;
    case "DELETE":
        $data = $_POST;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        break;
}

if (isset($_SERVER['HTTP_USER_AGENT']))
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_HEADER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "x-forwarded-for: $ip"));

foreach($_COOKIE as $k=>$v){
    if(is_array($v)){
        $v = serialize($v);
    }
    $unique_cookies_string = (isset($unique_cookies_string)?$unique_cookies_string.';':'') . "$k=$v";
}
if (isset($unique_cookies_string))
    curl_setopt($curl, CURLOPT_COOKIE, $unique_cookies_string."; domain=.".$config['cookie_domain']." ; path=/");

$response = curl_exec($curl);

$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$response_header = substr($response, 0, $header_size);
$response_body = substr($response, $header_size);

http_response_code(curl_getinfo($curl, CURLINFO_HTTP_CODE));

if ($response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    echo "<pre>";
    exit('error occurred during curl exec. Additional info: ' . var_export($info));
}
$curl_info = curl_getinfo($curl);
curl_close($curl);

$headers = explode("\n", $response_header);

$ck_keys = array("expires", "max-age", "path", "domain");
foreach ($headers as $hd)
{
    if (substr($hd, 0, strlen('Content-Type')) == 'Content-Type')
        header($hd);
    elseif (substr($hd, 0, strlen('Location')) == "Location")
        exit(header($hd));
    elseif (substr($hd, 0, strlen('Set-Cookie')) == 'Set-Cookie')
    {
        $ck_content = explode(";", substr($hd, strlen('Set-Cookie')+1));
        $ck = array("domain" => $config['cookie_domain'], "path" => "/");
        foreach ($ck_content as $params)
        {
            $param = explode("=", trim($params));
            if (!in_array(strtolower($param[0]), $ck_keys))
            {
                $ck['field'] = $param[0];
                $ck['value'] = $param[1];
            }
            else
                $ck[$param[0]] = $param[1];
        }
        setcookie($ck['field'], $ck['value'], (isset($ck['expires']) ? strtotime($ck['expires']) : false), $ck['path'], $ck['domain']);
    }
}

/* **************************************** */
if (!in_array(explode("/", $_SERVER['REQUEST_URI'])[1], $ignore))
{
    $mysqli = new mysqli($config['database']['host'], $config['database']['username'], $config['database']['password'], $config['database']['database']);
    if (!$mysqli->connect_errno) {
        if (isset($_GET['client_id']) OR isset($_POST['client_id']))
            $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : $_POST['client_id'];
        $mysqli->query("INSERT INTO `api_calls` (`client`, `command`, `response_time`, `requested_at`) VALUES (".(isset($client_id)?"'".$mysqli->real_escape_string($client_id)."'":'NULL') . ", '".$mysqli->real_escape_string(explode('?', $_SERVER['REQUEST_URI'], 2)[0])."', '".$curl_info['total_time']."', CURRENT_TIMESTAMP);");
        
        $filename = $mysqli->insert_id . ".query";
        file_put_contents('../'.$filename, json_encode($_POST+$_GET)."\r\n\r\n\r\n" . $response_header . "\r\n\r\n\r\n\r\n" . $response_body);
    }
}

/* **************************************** */

echo $response_body;