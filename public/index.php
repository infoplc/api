<?php
header("Access-Control-Allow-Origin: *");
 
use pfmAPI\Exceptions\HTTPException,
    pfmAPI\Apps\Misc\Models\Misc,
    pfmAPI\Errors\HTTPError;

require_once(__DIR__.DIRECTORY_SEPARATOR.'init.php');

$app->before(function() use ($app, $di) {
    if (is_array($app->getActiveHandler()))
    {
        if (explode("/", $app->router->getMatchedRoute()->getPattern())[1] == "oauth2")
            $controllerName = "oauth2";
        else
            $controllerName = explode("/", $app->router->getMatchedRoute()->getPattern())[2];
        $actionName = $app->getActiveHandler()[1];
        if (file_exists(BASE_DIR . '/apps/' . $controllerName . '/preconditions/'. ucfirst($actionName) . 'Preconditions.php' ))
            $preconditionClassName = "pfmAPI\Apps\\" . ucfirst($controllerName) . "\\Preconditions\\" . ucfirst($actionName) . "Preconditions";
        else
            $preconditionClassName = "pfmAPI\Preconditions\BasePreconditions";
        $precondition = new $preconditionClassName();
        $precondition->doCheck();
    }
});
/*
$app->before(function() use ($app, $di) {
	// Browser requests, user was stored in session on login, replace into DI
	if ($di->getShared('session')->get('user') != false) {
		$di->setShared('user', function() use ($di){
			return $di->getShared('session')->get('user');
		});
		return true;
	}
	// Basic auth, for programmatic responses
	if($app->request->getServer('PHP_AUTH_USER')){
		$user = new \pfmAPI\Controllers\UsersController();
		$user->login(
			$app->request->getServer('PHP_AUTH_USER'),
			$app->request->getServer('PHP_AUTH_PW')
		);
		return true;
	}
	// All options requests get a 200, then die
	if($app->__get('request')->getMethod() == 'OPTIONS'){
		$app->response->setStatusCode(200, 'OK')->sendHeaders();
		exit;
	}
	// Exempted routes, such as login, or public info.  Let the route handler
	// pick it up.
	switch($app->getRouter()->getRewriteUri()){
		case '/users/login':
			return true;
			break;
		case '/example/route':
			return true;
			break;
	}
	// If we made it this far, we have no valid auth method, throw a 401.
	throw new \pfmAPI\Exceptions\HTTPException(
		'Must login or provide credentials.',
		401,
		array(
			'dev' => 'Please provide credentials by either passing in a session token via cookie, or providing password and username via BASIC authentication.',
			'internalCode' => 'Unauth:1'
		)
	);
	return false;
});*/

/**
 * Mount all of the collections, which makes the routes active.
 */
foreach($di->get('collections') as $collection){
    $app->mount($collection);
}
/**
 * The base route return the list of defined routes for the application.
 * This is not strictly REST compliant, but it helps to base API documentation off of.
 * By calling this, you can quickly see a list of all routes and their methods.
 */
$app->get('/', function() use ($app){
    foreach($app->di->get('collections') as $collection){
        $handlers = $collection->getHandlers();
        foreach ($handlers as $handler)
        {
            if ($collection->getPrefix() == "/oauth2")
                continue;
            $namespaces = explode('\\',$collection->getHandler());
            $file = BASE_DIR . DIRECTORY_SEPARATOR . 'apps' . DIRECTORY_SEPARATOR . lcfirst($namespaces[3]) . DIRECTORY_SEPARATOR . 'preconditions' . DIRECTORY_SEPARATOR . ucfirst($handler[2]) . 'Preconditions.php';
            if (file_exists($file))
            {
                $namespace = implode('\\',array($namespaces[1],$namespaces[2],$namespaces[3])).'\\Preconditions\\'.ucfirst($handler[2]) . 'Preconditions';
                $aux = new $namespace;
                $params = $aux->params;
                $paramsAtLeast = $aux->params_at_least;
                $token = $aux->token;
            }
            else
            {
                throw new Exception('File not found when generating index.php: '.$file);
            }
            $routeDefinitions[$collection->getPrefix()][$handler[1]] = array("name" => $handler[2], "method" => $handler[0], "params" => $params, "paramsAtLeast" => $paramsAtLeast, "token" => $token);
        }
    }
    /*
	$routeDefinitions = array('GET'=>array(), 'POST'=>array(), 'PUT'=>array(), 'PATCH'=>array(), 'DELETE'=>array(), 'HEAD'=>array(), 'OPTIONS'=>array());
	foreach($routes as $route){
		$method = $route->getHttpMethods();
        print_r($route-> getConverters  ()); die();
        $routeDefinitions[$method][] = array("url" => $route->getPattern(), "parameters" => array());
	}
    */
    return $routeDefinitions;
});
$app->get("/img/{width:[0-9]+}x{height:[0-9]+}{crop:(f|)}/{type:(users|teams|competitions|organizations)}/{id:[0-9]+}/{filename}", function($width, $height, $crop, $type, $id, $filename) use ($app) {
    $hash = md5($width.'/'.$height.'/'.$type.'/'.$id.'/'.(($crop=='f')?"f_":"").$filename);
    $toDir = IMAGES_DIR.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR;
    if (file_exists($toDir.$hash.'.png'))
        exit(header('Location: /images/upload/cache/'.$type.'/'.$id.'/'. $hash.'.png'));
    $fromPath = realpath('./images/upload/'.$type.'/'.$id.'/') . DIRECTORY_SEPARATOR . $filename;
    if (!is_dir($toDir))
        mkdir($toDir,0777,true);
    $toPath = $toDir.(($crop=='f')?"f_":"").$hash.'.png';
    $pathInfo = pathinfo(basename($filename));
    \pfmAPI\Models\Image::resizeImage($fromPath,$width,$height,mb_strtolower($pathInfo['extension']), $toPath, ($crop=='f'));
    // Imprimir la imagen
    header('Content-Type: image/png');
    exit(header('Location: /images/upload/cache/'.$type.'/'.$id.'/'. (($crop=='f')?"f_":"") . $hash.'.png'));
});
/**
 * After a route is run, usually when its Controller returns a final value,
 * the application runs the following function which actually sends the response to the client.
 *
 * The default behavior is to send the Controller's returned value to the client as JSON.
 * However, by parsing the request querystring's 'type' parameter, it is easy to install
 * different response type handlers.  Below is an alternate csv handler.
 */
$app->after(function() use ($app, $parameters) {
    if (($app->getReturnedValue() !== false) && ($app->getReturnedValue() !== NULL))
    {
        // OPTIONS have no body, send the headers, exit
        if ($app->request->getMethod() == 'OPTIONS') {
            $app->response->setStatusCode('200', 'OK');
            $app->response->send();
            return;
        }
        // Respond by default as JSON
        if(!$app->request->get('type') || $app->request->get('type') == 'json'){
            // Results returned from the route's controller.  All Controllers should return an array
            $records = $app->getReturnedValue();
            $response = new \pfmAPI\Responses\JSONResponse();
            $response->useEnvelope($parameters->rest->envelope) //this is default behavior
            ->convertSnakeCase($parameters->rest->snake) //this is also default behavior
            ->send($records);
            return;
        }
        else if($app->request->get('type') == 'csv'){
            $records = $app->getReturnedValue();
            $response = new \pfmAPI\Responses\CSVResponse();
            $response->useHeaderRow(true)->send($records);
            return;
        }
        else {
            throw new HTTPException(
                'Could not return results in specified format',
                403,
                array(
                    'dev' => 'Could not understand type specified by type paramter in query string.',
                    'internalCode' => 'NF1000',
                    'more' => 'Type may not be implemented. Choose either "csv" or "json"'
                )
            );
        }
    }
});
/**
 * The notFound service is the default handler function that runs when no route was matched.
 * We set a 404 here unless there's a suppress error codes.
 */
$app->notFound(function () use ($app) {
    throw new HTTPException(
        'Not Found.',
        404,
        array(
            'dev' => 'That route was not found on the server: '.$_SERVER['REQUEST_URI'],
            'internalCode' => 'NF1000',
            'more' => 'Check route for misspellings.'
        )
    );
});
/**
 * Sends a json response when an error occurs
 */
set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (!(error_reporting() & $errno)) {
        return true;
    }
    $HTTPError = new HTTPError($errno, $errstr, $errfile, $errline);
    $HTTPError->send();
    exit(1);
});
/**
 * If the application throws an HTTPException, send it on to the client as json.
 * Else wise, just log it.
 * TODO:  Improve this.
 */
set_exception_handler(function($exception) use ($app){
    //HTTPException's send method provides the correct response headers and body
    if(is_a($exception, 'pfmAPI\\Exceptions\\HTTPException')){
        /** @var pfmAPI\Exceptions\HTTPException $exception */
        $exception->send();
    } else {
        die($exception);
        Misc::logException($exception);
        $exception = new HTTPException(
            'Uncaught exception',
            500,
            array(
                'dev' => 'An exception occurred and the application could not handle it. Check the route and the params.',
                'internalCode' => 'NF2000',
                'more' => 'A report of this error has been saved. Please, contact the administrators if the problem persists.')
        );
        $exception->send();
    }
});
if(file_exists('maintenance.flag')) {
    throw new HTTPException(
        'Under maintenance',
        503,
        array(
            'dev' => 'The application is under maintenance. Please, wait, we are working on it.',
            'internalCode' => 'MAINTENANCE',
        )
    );
}
$app->handle();