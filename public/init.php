<?php
use Phalcon\DI\FactoryDefault as DefaultDI,
    Phalcon\Mvc\Micro\Collection,
    Phalcon\Config\Adapter\Ini as IniConfig,
    Phalcon\Loader,
    \Phalcon\Db\Adapter\Pdo,
    Phalcon\Flash\Direct as Flash,
    Phalcon\Session\Adapter\Files as SessionAdapter,
    Phalcon\Crypt,
    Phalcon\Acl\Adapter\Memory,
    pfmAPI\Exceptions\HTTPException,
    pfmAPI\Errors\HTTPError,
    pfmAPI\Library\Auth\Auth,
    pfmAPI\Library\Acl\Acl,
    pfmAPI\Library\Mail\Mail,
    pfmAPI\Models\ACLLoader,
    pfmAPI\Apps\Misc\Models\Misc;
/**
 * Define some useful constants
 */
define('BASE_DIR', dirname(__DIR__));
define('APP_DIR', BASE_DIR . '/apps');
define('IMAGES_DIR', BASE_DIR . DIRECTORY_SEPARATOR . 'public'.DIRECTORY_SEPARATOR.'images');
// Use composer autoloader to load vendor classes
require_once __DIR__ . '/../vendor/autoload.php';
/**
 * Read the configuration
 */
$config = new IniConfig(__DIR__ . '/../config/config.ini');
$parameters = new IniConfig(__DIR__ . '/../config/parameters.ini');
// force ssl if available
if (isset($_SERVER["REQUEST_URI"])&&$parameters->core->forceHttps && !((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443))
    exit(header("Location: https://" . $_SERVER['SERVER_NAME'] . $_SERVER["REQUEST_URI"]));
/**
 * By default, namespaces are assumed to be the same as the path.
 * This function allows us to assign namespaces to alternative folders.
 * It also puts the classes into the PSR-0 autoLoader.
 */
$loader = new Loader();
// Dinamic namespaces
$dinamicNamespaces = array();
$appsCollection = scandir(BASE_DIR . '/apps');
foreach ($appsCollection as $app) {
    if (($app != ".") && ($app != "..")) {
        $dinamicNamespaces['pfmAPI\Apps\\'.ucfirst($app).'\Preconditions'] = BASE_DIR . '/apps/'.$app.'/preconditions';
        $dinamicNamespaces['pfmAPI\Apps\\'.ucfirst($app).'\Controllers'] = BASE_DIR . '/apps/'.$app.'/controllers';
        $dinamicNamespaces['pfmAPI\Apps\\'.ucfirst($app).'\Models'] = BASE_DIR . '/apps/'.$app.'/models';
        $dinamicNamespaces['pfmAPI\Apps\\'.ucfirst($app).'\Forms'] = BASE_DIR . '/apps/'.$app.'/forms';
        $dinamicNamespaces['pfmAPI\Apps\\'.ucfirst($app).'\Translations'] = BASE_DIR . '/apps/'.$app.'/translations';
    }
}
$loader->registerNamespaces($dinamicNamespaces + array(
        'pfmAPI\Models'                    => __DIR__ . '/..' . $parameters->core->modelsDir,
        'pfmAPI\Controllers'               => __DIR__ . '/..' . $parameters->core->controllersDir,
        'pfmAPI\Exceptions'                => __DIR__ . '/..' . $parameters->core->exceptionsDir,
        'pfmAPI\Errors'                    => __DIR__ . '/..' . $parameters->core->errorsDir,
        'pfmAPI\Responses'                 => __DIR__ . '/..' . $parameters->core->responsesDir,
        'pfmAPI\Validations'               => __DIR__ . '/..' . $parameters->core->validationsDir,
        'pfmAPI\Validations\Validators'    => __DIR__ . '/..' . $parameters->core->validatorsDir,
        'pfmAPI\Validations\Teams'         => __DIR__ . '/..' . $parameters->core->validationsDir . 'Teams',
        'pfmAPI\Validations\Competitions'  => __DIR__ . '/..' . $parameters->core->validationsDir . 'Competitions',
        'pfmAPI\Translations'              => __DIR__ . '/..' . $parameters->core->translationsDir,
        'pfmAPI\Forms'                     => __DIR__ . '/..' . $parameters->core->formsDir,
        'pfmAPI\Library'                   => __DIR__ . '/..' . $parameters->core->libraryDir,
        'pfmAPI\Preconditions'             => __DIR__ . '/..' . $parameters->core->preconditionsDir,
        'Sum\Oauth2\Server'                     => __DIR__ . '/..' . '/vendor/Sum/Oauth2/Server',
        'League\OAuth2\Server'                  => __DIR__ . '/..' . '/vendor/league/oauth2-server/src/League/OAuth2/Server',
    ))->register();
/**
 * The DI is our direct injector.  It will store pointers to all of our services
 * and we will insert it into all of our controllers.
 * @var DefaultDI
 */
$di = new DefaultDI();
/**
 * Crypt service
 */
$di->setShared('crypt', function () use ($config) {
    $crypt = new Crypt();
    $crypt->setKey($config->app->cryptSalt);
    return $crypt;
});
$di->set('cookies', function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
});
/**
 * Flash service with custom CSS classes
 */
$di->set('flash', function () {
    return new Flash(array(
        'error'     => 'alert alert-error',
        'success'   => 'alert alert-success',
        'notice'    => 'alert alert-info'
    ));
});
/**
 * Return array of the Collections, which define a group of routes, from
 * routes/collections.  These will be mounted into the app itself later.
 */
$di->set('collections', function(){
    return include(__DIR__ . '/..' . '/routes/routeLoader.php');
});
/**
 * $di's setShared method provides a singleton instance.
 * If the second parameter is a function, then the service is lazy-loaded
 * on its first instantiation.
 */
$di->setShared('config', function() use ($config) {
    return $config;
});
$di->setShared('parameters', function() use ($parameters) {
    return $parameters;
});
/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function(){
    $host_arr = explode(".", $_SERVER['HTTP_HOST']);
    $domain = $host_arr[count($host_arr)-2] . '.' . $host_arr[count($host_arr)-1];
    if(strpos($domain,':') !== false)
        $domain = substr($domain,0,strpos($domain,':'));
    session_set_cookie_params(0, '/', '.'. $domain);
    $session = new SessionAdapter();
    $session->start();
    return $session;
});
$di->set('modelsCache', function() {
    //Cache data for one day by default
    $frontCache = new \Phalcon\Cache\Frontend\Data(array(
        'lifetime' => 3600
    ));
    //File cache settings
    $cache = new \Phalcon\Cache\Backend\File($frontCache, array(
        'cacheDir' => __DIR__ . '/../cache/'
    ));
    return $cache;
});
/**
 * Database setup
 */
$di->set('db', function() use($config) {
    $adapter = $config->database->adapter;
    return new $adapter($config->database->toArray());
});
/**
 * Database setup
 */
$di->set('db_webclient', function() use($config) {
    $adapter = $config->database_webclient->adapter;
    return new $adapter($config->database_webclient->toArray());
});
/**
 * If our request contains a body, it has to be valid JSON.  This parses the
 * body into a standard Object and makes that vailable from the DI.  If this service
 * is called from a function, and the request body is nto valid JSON or is empty,
 * the program will throw an Exception.
 */
$di->setShared('requestBody', function() {
    $in = file_get_contents('php://input');
    $in = json_decode($in, FALSE);
    // JSON body could not be parsed, throw exception
    if($in === null){
        throw new HTTPException(
            'There was a problem understanding the data sent to the server by the application.',
            409,
            array(
                'dev' => 'The JSON body sent to the server was unable to be parsed.',
                'internalCode' => 'REQ1000',
                'more' => ''
            )
        );
    }
    return $in;
});
$di->set('modelsManager', function() {
    return new Phalcon\Mvc\Model\Manager();
});
/**
 * Out application is a Micro application, so we mush explicitly define all the routes.
 * For APIs, this is ideal.  This is as opposed to the more robust MVC Application
 * @var $app
 */
$app = new Phalcon\Mvc\Micro();
$app->setDI($di);
/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new Auth();
});
/**
 * Mail service uses AmazonSES
 */
$di->set('mail', function () {
    return new Mail();
});
/**
 * Access Control List
 */
$di->set('acl', function () {
    return new Acl();
});
# set oauth2 authorization service
/*
 * Authorization Server: An intermidiary token generated when a user authorizes a client to access protected resources on their behalf. The client receives this token and exchanges it for an access token.
 */
$di->setShared('oauth2_authorization', function() use ($config) {
    //$oauthdb = new Phalcon\Db\Adapter\Pdo\Mysql($config->database->oauth->toArray());
    $di = \Phalcon\DI::getDefault();
    $server = new \League\OAuth2\Server\Authorization(
        new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Client($di->get('db')),
        new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Session($di->get('db')),
        new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Scope($di->get('db'))
    );
    # Not required as it called directly from original code
    # $request = new \League\OAuth2\Server\Util\Request();
    # add these 2 lines code if you want to use my own Request otherwise comment it
    #$request = new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Request();
    #$server->setRequest($request);
    $server->setAccessTokenTTL(86400);
    $server->addGrantType(new League\OAuth2\Server\Grant\ClientCredentials());
    $server->addGrantType(new League\OAuth2\Server\Grant\AuthCode());
    $server->addGrantType(new League\OAuth2\Server\Grant\RefreshToken());
    return $server;
});
# set oauth2 as resource server
/*
 * Resource Server: A server which sits in front of protected resources (for example �tweets�, users� photos, or personal data) and is capable of accepting and responsing to protected resource requests using access tokens.
 */
$di->setShared('oauth2_resource', function() use ($config) {
    //$oauthdb = new Phalcon\Db\Adapter\Pdo\Mysql($config->database->oauth->toArray());
    $di = \Phalcon\DI::getDefault();
    $sessionStorage = new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Session($di->get('db'));
    $accessTokenStorage = false;
    $clientStorage = new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Client($di->get('db'));
    $scopeStorage = new \Sum\Oauth2\Server\Storage\Pdo\Mysql\Scope($di->get('db'));
    $server = new League\OAuth2\Server\Resource(
        $sessionStorage,
        $accessTokenStorage,
        $clientStorage,
        $scopeStorage
    );
    return $server;
});
$databaseUpdate = new \pfmAPI\Models\DatabaseUpdate();
$databaseUpdate->checkDatabaseUpdates();
/**
 * Before every request, make sure user is authenticated.
 * Returning true in this function resumes normal routing.
 * Returning false stops any route from executing.
 */
/*
This will require changes to fit your application structure.
It supports Basic Auth, Session auth, and Exempted routes.
It also allows all Options requests, as those tend to not come with
cookies or basic auth credentials and Preflight is not implemented the
same in every browser.
*/
$di->setShared('aclLoader', function () {
    $acl = new Memory();
    return new ACLLoader($acl);
});
$di->setShared('acl', function () use ($di) {
    return $di->getShared('aclLoader')->initAcls();
});