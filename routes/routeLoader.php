<?php

/***
 * routeLoader loads a set of Phalcon Mvc\Micro\Collections from
 * the collections directory.
 *
 * php files in the collections directory must return Collection objects only.
 */

return call_user_func(function() {
	$collections = array();
	$appsCollection = scandir(BASE_DIR . '/apps');
	foreach ($appsCollection as $app) {
		if (($app != ".") && ($app != "..") && file_exists(BASE_DIR . '/apps/' . $app .'/routes')) {
			$collectionFiles = scandir(BASE_DIR . '/apps/' . $app .'/routes');
			foreach ($collectionFiles as $collectionFile) {
				$pathinfo = pathinfo($collectionFile);
				//Only include php files
				if ($pathinfo['extension'] === 'php') {
					// The collection files return their collection objects, so mount
					// them directly into the router.
					$collections[] = include(BASE_DIR . '/apps/' . $app .'/routes/' . $collectionFile);
				}
			}
		}
	}
	return $collections;
});