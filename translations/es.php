<?php

use pfmAPI\Apps\Notification\Models\Notifications;

    $messages = array(
        'GRANT_CLIENT_ACCESS'               => '<b>%username%</b>, el cliente <b>%client%</b> quiere tener acceso a',
        "GRANT_CLIENT_ACCESS_AP"            => '<b>%username%</b>, estás a punto de iniciar sesión en <b>%client%</b>.',
        'CONTINUE'                          => 'Continuar',
        'DENY'                              => 'Denegar',
        'APPROVE'                           => 'Conceder',
        'NICK'                              => 'Nombre de usuario',
        'EMAIL'                             => 'Correo electrónico',
        'PASSWORD'                          => 'Clave de acceso',
        'DO_LOGIN'                          => 'Iniciar sesión',
        'REMEMBER_ME'                       => 'Recordarme',
        'CONFIRM_PASSWORD'                  => 'Confirmar clave de acceso',
        'ACCEPT_TERMS'                      => 'Acepto las <a href="%url%" target="_blank">condiciones</a>',
        'DO_SIGNUP'                         => 'Crear cuenta',
        'ACCOUNT_CREATED_SUCCESSFULLY'      => 'Cuenta creada. Sigue las instrucciones del correo electrónico que acabamos de enviarte para poder activar tu cuenta',
        'SENT_RESET_PASSWORD_SUCCESSFULLY'  => 'Se han enviado las instrucciones a tu correo electrónico',
        'DO_SEND_RESET_PASSWORD'            => 'Enviar instrucciones',
        'WAIT_MORE_FOR_RESET_PASSWORD'      => 'Hace menos de cinco minutos que se han enviado las instrucciones a esta cuenta... Para volverlas a enviar deberás dejar algo de tiempo',
        'NO_EMAIL_ACCOUNT_ASSOCIATED'       => 'No hay ninguna cuenta asociada a este correo electrónico',
        'INVALID_USERNAME_OR_PASSWORD'      => 'Nombre de usuario y/o clave incorrectos',
        'INVALID_CSRF'                      => 'CSRF inválido, inténtalo de nuevo',
        'YES'                               => 'Si',
        'NO'                                => 'No',
        'CHANGE_PASSWORD'                   => 'Cambiar clave de acceso',
        'CHANGE_EMAIL'                      => 'Cambiar correo electrónico',
        'CREATED_AT'                        => 'Fecha de registro',
        'UNIQUE_ID'                         => 'Identificador',
        'BANNED'                            => 'Expulsado',
        "EMAIL_VERIFIED_SUCCESSFULLY_AGAIN" => '¿Otra vez aquí? Tu cuenta de correo electrónico ya fue verificada y tu cuenta activada',
        "INVALID_CONFIRMATION_EMAIL_CODE"   => 'Oops! Vaya, parece que algo fue mal. Comprueba que la dirección URL coincida con la del email de verificación',
        "DO_CHANGE_PASSWORD"                => 'Cambiar clave',
        "INVALID_RESET_PASSWORD_CODE"       => 'Oops! Vaya, parece que algo fue mal. Comprueba que la dirección URL coincida con la del email',
        "ALREADY_USED_RESET_PASSWORD_CODE"  => 'Ya has utilizado este enlace para cambiar tu clave con anterioridad. Pide otro nuevo si lo necesitas.',
        "PASSWORD_SUCCESSFULLY_CHANGED"     => 'Tu clave se ha actualizado',

        "CURRENT_PASSWORD"                  => 'Clave de acceso actual',
        "NEW_PASSWORD"                      => 'Nueva clave de acceso',
        "CONFIRM_NEW_PASSWORD"              => 'Confirmar nueva clave de acceso',
        "INVALID_CURRENT_PASSWORD"          => 'Clave actual incorrecta',
        "ERROR_SAVING_CHANGES"              => 'Error al guardar los cambios, inténtalo de nuevo',

        "NEW_EMAIL"                         => 'Nueva cuenta de correo electrónico',
        "DO_CHANGE_EMAIL"                   => 'Enviar instrucciones al nuevo correo electrónico',
        "CHANGE_EMAIL_FORM_INFO"            => 'Se enviará un correo de confirmación a la nueva dirección de correo electrónico. Una vez confirmado se hará efectivo el cambio.',

        "SENT_CONFIRMATION_EMAIL_SUCCESSFULLY"  => 'Se ha enviado una notificación a la nueva cuenta de correo electrónica especificada. Cuando la confirmes se hará efectivo el cambio.',
        "EMAIL_VERIFIED_SUCCESSFULLY"            => 'Cuenta de correo electrónica verificada.',
        "WAIT_MORE_FOR_CHANGE_EMAIL"        => 'Has pedido un cambio de correo electrónico recientemente, por favor, espera un rato para poder pedir otro.',

        "EMAIL_ALREADY_TAKEN"              => "La cuenta de correo electrónico que estás intentando verificar ya está registrada a nombre del usuario %nickname%",
        "NEW_EMAIL_EQUAL_CURRENT_EMAIL"     => "La nueva cuenta de correo electrónico es la misma que ya tienes asignada en tu perfil.",
        "EMAIL_VERIFICATION_EXPIRED"       => "La verificación es correcta, pero ha caducado debido a la petición de nuevos cambios de correo electrónico.",

      

        'AUTHORIZE' => 'Autorizar',
        'EDIT_PROFILE' => 'Editar perfil',
        'PREFERENCES' => 'Preferencias API',
        'LOGOUT' => 'Cerrar sesión',
        'SIGNIN' => 'Iniciar sesión',
        'SIGNUP' => 'Nueva cuenta',
        'FORGOT_PASSWORD' => 'Recordar clave',


    );