<?php
namespace pfmAPI\Validations;

/*
 * Ayuda:
 *
 *  $items = array( campo => opciones); *
 *  |-> Campos del formulario
 *
 *  $at_least = array( array("field_1", "field_2"), array("field_3", field_4"));
 *  |-> Conjunto de campos, de los que ÚNICAMENTE UNO deberá existir, sino no pasará la validación. Todos los campos especificados
 *      obtendrán el valor "optional=true".
 *
 *  Opciones (opcional):
 *      - optional (bool)       = (true = puede no existir | false = es obligatorio que exista) (por defecto = false)
 *      - empty (bool)          = (true = se admite como valido que este vacio | false = no puede estar vacio) (por defecto = false)
 *      - values (array)        = array ("valor1", "valor2", ...)  - El valor solo podrá ser alguno de los especificados en este array
 *      - validator (string)    = [ integer | multiple_integer | email ]
 *      - minLength (int)       = longitud mínima
 *      - maxLength (int)       = longitud máxima
 */

use Phalcon\Validation,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Regex,

    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\Url,

    pfmAPI\Validations\Validators\EmptyUrl,
    pfmAPI\Validations\Validators\EmptyPresenceOf,
    pfmAPI\Validations\Validators\EmptyStringLength,
    pfmAPI\Validations\Validators\EmptyRegex,
    pfmAPI\Validations\Validators\EmptyEmail,
    pfmAPI\Validations\Validators\DateTimeCustom,
    pfmAPI\Validations\Validators\EmptyDateTimeCustom,
    pfmAPI\Validations\Validators\Integer,
    pfmAPI\Validations\Validators\EmptyInteger;
use pfmAPI\Exceptions\HTTPException;

class BaseValidation extends Validation
{
    public $items;
    public $at_least;
    public $request = false;

    public function addValidations()
    {
        if (empty($this->items))
            die("WTF: No se ha especificado en el archivo de validación los campos que contiene el formulario a validar");

        if (!$this->request)
            $this->request = $this->getDI()->get('request');

        // A los campos "at_least" les forzamos la propiedad "opcionabilidad"
        if (!empty($this->at_least))
            foreach ($this->at_least as $items_group)
                foreach ($items_group as $item_field)
                    $this->items[$item_field]['optional'] = true;

        // nos recorremos todos los items del array del formulario, y añadiremos todas las validaciones pertinentes...
        foreach ($this->items as $field => $options)
        {
            // será un campo obligatorio cuando no se ha especificado la opcionalidad, o se ha indicado expresamente que no es opcional
            if (!isset($options['optional']) or !$options['optional'])
            {
                // PresenceOf = Validates that a field’s value isn’t null or empty string.
                if (isset($options['empty']) && $options['empty'])
                    $this->add($field, new EmptyPresenceOf(array(
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is required',
                        //'cancelOnFail' => true
                    )));
                else
                    $this->add($field, new PresenceOf(array(
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is required',
                        //'cancelOnFail' => true
                    )));
            }

            // si tiene un validador asignado, se lo ponemos...
            if (isset($options['validator']))
            {
                // Si era de tipo email, le añadimos la validación...
                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                    ($options['validator'] == "email"))
                {
                    // Si permitimos que el campo este vacio, usamos un validador especial
                    if (isset($options['empty']) && $options['empty'])
                        $this->add($field, new EmptyEmail(array(
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                        )));
                    else
                        $this->add($field, new Email(array(
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                        )));
                }

                else

                // Si era de tipo numérico, le añadimos la validación...
                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                    ($options['validator'] == "integer"))
                {
                    // Si permitimos que el campo este vacio, usamos un validador especial
                    if (isset($options['empty']) && $options['empty'])
                        $this->add($field,  new EmptyInteger(array(
                            'min' => isset($options['min']) ? $options['min'] : null,
                            'max' => isset($options['max']) ? $options['max'] : null,
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be an integer'
                        )));
                    else
                        $this->add($field,  new Integer(array(
                            'min' => isset($options['min']) ? $options['min'] : null,
                            'max' => isset($options['max']) ? $options['max'] : null,
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be an integer'
                        )));
                }

                else

                // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                    ($options['validator'] == "multiple_integer"))
                {
                    // Si permitimos que el campo este vacio, usamos un validador especial
                    if (isset($options['empty']) && $options['empty'])
                        $this->add($field,  new EmptyRegex(array(
                            'pattern' => '/^(\d+(,\d+)*)?$/',
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer (optional: multiple integers with commas)'
                        )));
                    else
                        $this->add($field,  new Regex(array(
                            'pattern' => '/^(\d+(,\d+)*)?$/',
                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a integer'
                        )));
                }

                else

                    // Si era de tipo numérico, le añadimos la validación...
                    // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                    if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                        ($options['validator'] == "datetime"))
                    {
                        // Si permitimos que el campo este vacio, usamos un validador especial
                        if (isset($options['empty']) && $options['empty'])
                            $this->add($field,  new EmptyDateTimeCustom(array(
                                'pattern' => isset($options['pattern']) ? $options['pattern'] : 'Y-m-d H:i:s',
                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid date'
                            )));
                        else
                            $this->add($field,  new DateTimeCustom(array(
                                'pattern' => isset($options['pattern']) ? $options['pattern'] : 'Y-m-d H:i:s',
                                'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid date'
                            )));
                    }

                    else

                        // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                        // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                        if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                            ($options['validator'] == "decimal"))
                        {
                            // Si permitimos que el campo este vacio, usamos un validador especial
                            if (isset($options['empty']) && $options['empty'])
                                $this->add($field,  new EmptyRegex(array(
                                    'pattern' => '/^\d+(\.\d{1,2})?$/',
                                    'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a decimal (dot-separated, maximum 2 decimals)'
                                )));
                            else
                                $this->add($field,  new Regex(array(
                                    'pattern' => '/^\d+(\.\d{1,2})??$/',
                                    'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a decimal (dot-separated, maximum 2 decimals)'
                                )));
                        }

                        else

                            // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                ($options['validator'] == "identical"))
                            {
                                // Si permitimos que el campo este vacio, usamos un validador especial
                                if (isset($options['empty']) && $options['empty'])
                                    $this->add($field,  new Identical(array(
                                        'value'   => $options['value'],
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                                    )));
                                elseif (isset($options['with']))
                                    $this->add($field,  new Identical(array(
                                        'with'   => $options['with'],
                                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' is not valid'
                                    )));
                            }

                            else

                                // Si era de tipo numérico (MULTIPLE = separado por comas), le añadimos la validación...
                                // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
                                if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                                    ($options['validator'] == "url"))
                                {
                                    // Si permitimos que el campo este vacio, usamos un validador especial
                                    if (isset($options['empty']) && $options['empty'])
                                        $this->add($field,  new EmptyUrl(array(
                                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid url'
                                        )));
                                    else
                                        $this->add($field,  new Url(array(
                                            'pattern' => '/^\d+(\.\d{1,2})??$/',
                                            'message' => 'The '.(isset($options['name'])?$options['name']:$field).' must be a valid url'
                                        )));
                                }
                else

                // comprobamos la opcionalidad del parametro, pues a lo mejor si era de tipo valido pero no se cumplio ninguna de las condiciones anteriores precisamente por esto
                if (!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field)))
                throw new HTTPException(
                    "Unable to validate the form at this moment.",
                    500,
                    array(
                        'dev' => "Validator type (".$options['validator'].") not found for '" . (isset($options['name'])?$options['name']:$field). "'",
                        'internalCode' => '???',
                    ));

            }

            // Si tenia especificado un valor mínimo, se lo añadimos a la validación...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['minLength']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $this->add($field, new EmptyStringLength(array(
                        'messageMinimum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too short',
                        'min' => $options['minLength']
                    )));
                else
                    $this->add($field, new StringLength(array(
                        'messageMinimum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too short',
                        'min' => $options['minLength'],
                    )));

            }

            // Si tenia especificado unos valores predefinidos (options['values'])...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['values']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $this->add($field,  new EmptyRegex(array(
                        'pattern' => '/^'.implode("|^", $options['values']).'$/i',
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' value must be: ' . implode(", ", $options['values'])
                    )));
                else
                    $this->add($field,  new Regex(array(
                        'pattern' => '/^'.implode("|^", $options['values']).'$/i',
                        'message' => 'The '.(isset($options['name'])?$options['name']:$field).' value must be: ' . implode(", ", $options['values'])
                    )));

            }

            // Si tenia especificado un valor mínimo, se lo añadimos a la validación...
            // ... pero solo si se trata de un parametro obligatorio, o es opcional y el usuario nos lo esta mandando (si es opcional y no recibimos nada, no hay que validarlo)
            if ((!isset($options['optional']) or !$options['optional'] or ($options['optional'] && $this->request->has($field))) &&
                isset($options['maxLength']))
            {
                // Si permitimos que el campo este vacio, usamos un validador especial
                if (isset($options['empty']) && $options['empty'])
                    $this->add($field, new EmptyStringLength(array(
                        'messageMaximum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too long',
                        'max' => $options['maxLength']
                    )));
                else
                    $this->add($field, new StringLength(array(
                        'messageMaximum' => 'The '.(isset($options['name'])?$options['name']:$field).' is too long',
                        'max' => $options['maxLength'],
                    )));

            }
        }
    }

    public function doValidation($values)
    {
        // Si el formulario solo tenia campos opcionales y el usuario no nos ha mandado ninguno, en este caso no habrá ningun validador activo
        if (!$this->getValidators())
            return true;

        $messages = $this->validate($values);
        if (count($messages)) {
            foreach ($this->getMessages() as $message) {
                $errs[] = $message->getMessage();
            }
        }

        if (!empty($this->at_least))
        {
            foreach ($this->at_least as $items_group)
            {
                $counter = 0;
                foreach ($items_group as $item_field)
                {
                    if ($this->request->has($item_field))
                         $counter++;
                }

                if ($counter != 1)
                    $errs[] = "You".(($counter>1)?" only":"")." need to specify ONE of the following parameters: " . implode(", ", $items_group);
            }
        }

        if (isset($errs))
            throw new HTTPException(
                "Could not complete the operation. One or more parameter values are not valid",
                401,
                array(
                    'internalCode' => 'Params001',
                    'dev' => $errs,
                ));

        return true;
    }

    public function getItems() {
        return $this->items;
    }

    public function setItems($items) {
        $this->items = $items;
    }

    public function setItemsAtLeast($items) {
        $this->at_least = $items;
    }
}