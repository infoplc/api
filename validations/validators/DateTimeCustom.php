<?php
namespace pfmAPI\Validations\Validators;

use Phalcon\Validation\Validator,
    Phalcon\Validation\ValidatorInterface,
    Phalcon\Validation\Message;

class DateTimeCustom extends Validator implements ValidatorInterface
{
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $d = \DateTime::createFromFormat($this->getOption('pattern'), $validator->getValue($attribute));
        if ($d && $d->format($this->getOption('pattern')) == $validator->getValue($attribute))
        {
            return true;
        }
        else
        {
            $validator->appendMessage(new Message($this->getOption('message'), $attribute, 'type'));
            return false;
        }
    }
}