<?php
namespace pfmAPI\Validations\Validators;
use Phalcon\Validation\Validator,
    Phalcon\Validation\ValidatorInterface,
    Phalcon\Validation\Message;
class EmptyInteger extends Validator implements ValidatorInterface
{

    /**
     * Executes the validation
     *
     * @param \Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        if($value == '') return true;
        else {
            if (is_numeric($value) && (int)$value >= 0) {
                return true;
            }
            $validator->appendMessage(new Message($this->getOption('message'), $attribute, 'type'));
            return false;
        }
    }
}