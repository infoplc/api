<?php
namespace pfmAPI\Validations\Validators;

class EmptyUrl extends \Phalcon\Validation\Validator\Url
{

    /**
     * Executes the validation
     *
     * @param Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        //if ($value === null or $value === '') {
        if ($value === '') {
            return true;
        }
        return parent::validate($validator, $attribute);
    }

}