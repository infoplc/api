<?php
namespace pfmAPI\Validations\Validators;
use Phalcon\Validation\Validator,
    Phalcon\Validation\ValidatorInterface,
    Phalcon\Validation\Message;
class Integer extends Validator implements ValidatorInterface
{

    /**
     * Executes the validation
     *
     * @param \Phalcon\Validation $validator
     * @param string $attribute
     * @return \Phalcon\Validation\Message\Group
     */
    public function validate(\Phalcon\Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $max = $this->getOption('max');
        $min = $this->getOption('min');

        if (is_numeric($value)) {
            if(($max && $value > (int)$max) || ($min && $value < (int)$min)) {
                $validator->appendMessage(new Message('The integer is not in the minimum or maximum range of numbers accepted.', $attribute, 'type'));
                return false;
            }
            return true;
        }
        $validator->appendMessage(new Message($this->getOption('message'), $attribute, 'type'));
        return false;
    }
}